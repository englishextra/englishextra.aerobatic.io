/*!
 * externalcounters.js
 * track visitors
 * gist.github.com/englishextra/12dec2c7a796ab9ec5e9ed84b134c055
 */
!/localhost/.test(self.location.host)&&(function(b,a,c,d){if(c&&d){var e=encodeURIComponent(a.referrer||"");b=encodeURIComponent(b.location.href||"");a=encodeURIComponent(("undefined"!==typeof earlyDocumentTitle?earlyDocumentTitle:(a.title||"")).replace("\x27","&#39;").replace("\x28","&#40;").replace("\x29","&#41;"));c.setAttribute("style","position:absolute;left:-9999px;width:1px;height:1px;border:0;background:transparent url("+d+"?dmn="+b+"&rfrr="+e+"&ttl="+a+"&encoding=utf-8) top left no-repeat;")}})(window,document,document.getElementById("externalcounters")||"",/localhost/.test(self.location.host)?"http://localhost/externalcounters/": "//shimansky.biz/externalcounters/");
(function(d){var g=/localhost/.test(self.location.host)?"http://localhost/externalcounters/":"//shimansky.biz/externalcounters/",c=d.getElementsByTagName("a")||"",a=self.location.protocol+"//"+self.location.host+"/"||"",h=self.location.host+"/"||"",k=encodeURIComponent(d.location.href||""),l=encodeURIComponent((d.title||"").replace("\x27","&#39;").replace("\x28","&#40;").replace("\x29","&#41;"));if(c&&a&&h)for(a=0;a<c.length;a+=1)if(b=c[a],(e=b.getAttribute("href")||"")&&(e.match(/^\/scripts\//)||/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)|(localhost)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(e))&& !b.getAttribute("rel"))c[a].onclick=function(){var a=this.getAttribute("href"),c=getChildrenByTag.one("body").firstChild,f=d.createElement("div");f.setAttribute("style","position:absolute;left:-9999px;width:1px;height:1px;border:0;background:transparent url("+g+"?dmn="+encodeURIComponent(a)+"&rfrr="+k+"&ttl="+l+"&encoding=utf-8) top left no-repeat;");c.parentNode.insertBefore(f,c)}})(document);
(function(k){k.addEventListener("blur",function(){(function(d,w){var a=/localhost/.test(self.location.host)?"http://localhost/externalcounters/":"//shimansky.biz/externalcounters/",b=encodeURIComponent(d.referrer||""),c=encodeURIComponent(w.location.href||""),e=encodeURIComponent((d.title||"").replace("\x27","&#39;").replace("\x28","&#40;").replace("\x29","&#41;")+" - \u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442 \u043d\u0435 \u0430\u043a\u0442\u0438\u0432\u0435\u043d"),f=d.createElement("div"),g=getChildrenByTag.one("body").firstChild;f.setAttribute("style", "position:absolute;left:-9999px;width:1px;height:1px;border:0;background:transparent url("+a+"?dmn="+c+"\x26rfrr="+b+"\x26ttl="+e+"\x26encoding=utf-8) top left no-repeat;");g.parentNode.insertBefore(f,g);}(document,window));},!1);}(window));
/*!
 * safe way to handle console.log():
 * sitepoint.com/safe-console-log/
 */
"undefined"===typeof console&&(console={log:function(){}});
/*!
 * detect Electron and NW.js
 */
var isElectron="undefined"!==typeof window&&window.process&&"renderer"===window.process.type||"",
isNwjs="";try{"undefined"!==typeof require("nw.gui")&&(isNwjs=!0)}catch(a){isNwjs=!1};
var isOldOpera=!!window.opera||!1;
/*!
 * Temporal hiding of module
 * github.com/electron/electron/pull/3497
 * gist.github.com/englishextra/1bf72395f77c8cf25ddd900aa69a0d68
 */
var hideWindowModule=function(){"object"===typeof module&&(window.module=module,module=void 0);};
/*!
 * Temporal reveal of module
 * github.com/electron/electron/pull/3497
 * gist.github.com/englishextra/1bf72395f77c8cf25ddd900aa69a0d68
 */
var revealWindowModule=function(){window.module&&(module=window.module);};
/*!
 * modified for babel ToProgress v0.1.1
 * http://github.com/djyde/ToProgress
 * arguments.callee changed to ToProgress
 * wrapped in curly brackets:
 * else{document.body.appendChild(this.progressBar);}
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 */
var ToProgress=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}function t(){var t,s=document.createElement("fakeelement"),i={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in i)if(void 0!==s.style[t])return i[t]}function s(t,s){if(this.progress=0,this.options={id:"top-progress-bar",color:"#F44336",height:"2px",duration:.2},t&&"object"==typeof t)for(var i in t)this.options[i]=t[i];if(this.options.opacityDuration=3*this.options.duration,this.progressBar=document.createElement("div"),this.progressBar.id=this.options.id,this.progressBar.setCSS=function(t){for(var s in t)this.style[s]=t[s]},this.progressBar.setCSS({position:s?"relative":"fixed",top:"0",left:"0",right:"0","background-color":this.options.color,height:this.options.height,width:"0%",transition:"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-moz-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-webkit-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s"}),s){var o=document.querySelector(s);o&&(o.hasChildNodes()?o.insertBefore(this.progressBar,o.firstChild):o.appendChild(this.progressBar))}else document.body.appendChild(this.progressBar)}var i=t();return s.prototype.transit=function(){this.progressBar.style.width=this.progress+"%"},s.prototype.getProgress=function(){return this.progress},s.prototype.setProgress=function(t,s){this.show(),this.progress=t>100?100:0>t?0:t,this.transit(),s&&s()},s.prototype.increase=function(t,s){this.show(),this.setProgress(this.progress+t,s)},s.prototype.decrease=function(t,s){this.show(),this.setProgress(this.progress-t,s)},s.prototype.finish=function(t){var s=this;this.setProgress(100,t),this.hide(),i&&this.progressBar.addEventListener(i,function(t){s.reset(),s.progressBar.removeEventListener(t.type,ToProgress)})},s.prototype.reset=function(t){this.progress=0,this.transit(),t&&t()},s.prototype.hide=function(){this.progressBar.style.opacity="0"},s.prototype.show=function(){this.progressBar.style.opacity="1"},s;}());
/*!
 * init ToProgress
 */
var toprogress_options = {
	id : "top-progress-bar",
	color : "#FC6054",
	height : "3px",
	duration : .2
}, progressBar = new ToProgress(toprogress_options),
progressBarAvailable = "undefined" !== typeof window && window.progressBar ? !0 : !1,
startProgressBar = function (v) {
	v = v || 50;
	progressBarAvailable && progressBar.increase(v);
},
finishProgressBar = function () {
	progressBarAvailable && (progressBar.finish(), progressBar.hide());
};
progressBarAvailable && progressBar.increase(20);
/*!
 * Polyfill for IE8 Javascript Event
 * gist.github.com/englishextra/871776e0aabb70bb5d13
 * based on gist.github.com/chriswrightdesign/7573368
 */
(function(){if(!Event.prototype.preventDefault){Event.prototype.preventDefault=function(){this.returnValue=false;};}if(!Event.prototype.stopPropagation){Event.prototype.stopPropagation=function(){this.cancelBubble=true;};}if(!Element.prototype.addEventListener){var eventListeners=[];var addEventListener=function(type,listener){var self=this;var wrapper=function(e){e.target=e.srcElement;e.currentTarget=self;if(listener.handleEvent){listener.handleEvent(e);}else{listener.call(self,e);}};if(type=="DOMContentLoaded"){var wrapper2=function(e){if(document.readyState=="complete"){wrapper(e);}};document.attachEvent("onreadystatechange",wrapper2);eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper2});if(document.readyState=="complete"){var e=new Event();e.srcElement=window;wrapper2(e);}}else{this.attachEvent("on"+type,wrapper);eventListeners.push({object:this,type:type,listener:listener,wrapper:wrapper});}};var removeEventListener=function(type,listener){var counter=0;while(counter<eventListeners.length){var eventListener=eventListeners[counter];if(eventListener.object==this&&eventListener.type==type&&eventListener.listener==listener){if(type=="DOMContentLoaded"){this.detachEvent("onreadystatechange",eventListener.wrapper);}else{this.detachEvent("on"+type,eventListener.wrapper);}break;}++counter;}};Element.prototype.addEventListener=addEventListener;Element.prototype.removeEventListener=removeEventListener;if(HTMLDocument){HTMLDocument.prototype.addEventListener=addEventListener;HTMLDocument.prototype.removeEventListener=removeEventListener;}if(Window){Window.prototype.addEventListener=addEventListener;Window.prototype.removeEventListener=removeEventListener;}}})();
/*!
 * paulirish.com/2011/requestanimationframe-for-smart-animating/
 * my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 * requestAnimationFrame polyfill by Erik Moller. fixes from Paul Irish and Tino Zijdel
 * MIT license
*/
(function(){var lastTime=0;var vendors=["ms","moz","webkit","o"];for(var x=0;x<vendors.length&&!window.requestAnimationFrame;++x){window.requestAnimationFrame=window[vendors[x]+"RequestAnimationFrame"];window.cancelAnimationFrame=window[vendors[x]+"CancelAnimationFrame"]||window[vendors[x]+"CancelRequestAnimationFrame"];}if(!window.requestAnimationFrame)window.requestAnimationFrame=function(callback,element){var currTime=new Date().getTime();var timeToCall=Math.max(0,16-(currTime-lastTime));var id=window.setTimeout(function(){callback(currTime+timeToCall);},timeToCall);lastTime=currTime+timeToCall;return id;};if(!window.cancelAnimationFrame)window.cancelAnimationFrame=function(id){clearTimeout(id);};}());
/*!
 * weakmap-polyfill v2.0.0 - ECMAScript6 WeakMap polyfill
 * https://github.com/polygonplanet/weakmap-polyfill
 * Copyright (c) 2015-2016 polygon planet <polygon.planet.aqua@gmail.com>
 * @license MIT
 */
(function(e){"use strict";if(e.WeakMap){return}var t=Object.prototype.hasOwnProperty;var r=function(e,t,r){if(Object.defineProperty){Object.defineProperty(e,t,{configurable:true,writable:true,value:r})}else{e[t]=r}};e.WeakMap=function(){function WeakMap(){if(this===void 0){throw new TypeError("Constructor WeakMap requires 'new'")}r(this,"_id",genId("_WeakMap"));if(arguments.length>0){throw new TypeError("WeakMap iterable is not supported")}}r(WeakMap.prototype,"delete",function(e){checkInstance(this,"delete");if(!isObject(e)){return false}var t=e[this._id];if(t&&t[0]===e){delete e[this._id];return true}return false});r(WeakMap.prototype,"get",function(e){checkInstance(this,"get");if(!isObject(e)){return void 0}var t=e[this._id];if(t&&t[0]===e){return t[1]}return void 0});r(WeakMap.prototype,"has",function(e){checkInstance(this,"has");if(!isObject(e)){return false}var t=e[this._id];if(t&&t[0]===e){return true}return false});r(WeakMap.prototype,"set",function(e,t){checkInstance(this,"set");if(!isObject(e)){throw new TypeError("Invalid value used as weak map key")}var n=e[this._id];if(n&&n[0]===e){n[1]=t;return this}r(e,this._id,[e,t]);return this});function checkInstance(e,r){if(!isObject(e)||!t.call(e,"_id")){throw new TypeError(r+" method called on incompatible receiver "+typeof e)}}function genId(e){return e+"_"+rand()+"."+rand()}function rand(){return Math.random().toString().substring(2)}r(WeakMap,"_polyfill",true);return WeakMap}();function isObject(e){return Object(e)===e}})(typeof self!=="undefined"?self:typeof window!=="undefined"?window:typeof global!=="undefined"?global:this);
/*!
 * github.com/webcomponents/webcomponentsjs/blob/master/src/MutationObserver/MutationObserver.js
 * closure-compiler.appspot.com/code/jsc665493f158bf71252a410cf61ec2ee93/default.js
 * v0.7.22
 * @license
 * Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */
(function(k){function v(){p=!1;var a=q;q=[];a.sort(function(b,a){return b.uid_-a.uid_});var b=!1;a.forEach(function(a){var d=a.takeRecords();A(a);d.length&&(a.callback_(d,a),b=!0)});b&&v()}function A(a){a.nodes_.forEach(function(b){(b=f.get(b))&&b.forEach(function(b){b.observer===a&&b.removeTransientObservers()})})}function r(a,b){for(var c=a;c;c=c.parentNode){var d=f.get(c);if(d)for(var e=0;e<d.length;e++){var g=d[e],h=g.options;(c===a||h.subtree)&&(h=b(h))&&g.enqueue(h)}}}function l(a){this.callback_=a;this.nodes_=[];this.records_=[];this.uid_=++B}function w(a,b){this.type=a;this.target=b;this.addedNodes=[];this.removedNodes=[];this.oldValue=this.attributeNamespace=this.attributeName=this.nextSibling=this.previousSibling=null}function t(a,b){return m=new w(a,b)}function x(a){if(g)return g;var b=m,c=new w(b.type,b.target);c.addedNodes=b.addedNodes.slice();c.removedNodes=b.removedNodes.slice();c.previousSibling=b.previousSibling;c.nextSibling=b.nextSibling;c.attributeName=b.attributeName;c.attributeNamespace=b.attributeNamespace;c.oldValue=b.oldValue;g=c;g.oldValue=a;return g}function y(a,b,c){this.observer=a;this.target=b;this.options=c;this.transientObservedNodes=[]}if(!k.JsMutationObserver){var f=new WeakMap,n;if(/Trident|Edge/.test(navigator.userAgent))n=setTimeout;else if(window.setImmediate)n=window.setImmediate;else{var u=[],z=String(Math.random());window.addEventListener("message",function(a){a.data===z&&(a=u,u=[],a.forEach(function(a){a()}))});n=function(a){u.push(a);window.postMessage(z,"*")}}var p=!1,q=[],B=0;l.prototype={observe:function(a,b){var c=a;a=window.ShadowDOMPolyfill&&window.ShadowDOMPolyfill.wrapIfNeeded(c)||c;if(!b.childList&&!b.attributes&&!b.characterData||b.attributeOldValue&&!b.attributes||b.attributeFilter&&b.attributeFilter.length&&!b.attributes||b.characterDataOldValue&&!b.characterData)throw new SyntaxError;(c=f.get(a))||f.set(a,c=[]);for(var d,e=0;e<c.length;e++)if(c[e].observer===this){d=c[e];d.removeListeners();d.options=b;break}d||(d=new y(this,a,b),c.push(d),this.nodes_.push(a));d.addListeners()},disconnect:function(){this.nodes_.forEach(function(a){a=f.get(a);for(var b=0;b<a.length;b++){var c=a[b];if(c.observer===this){c.removeListeners();a.splice(b,1);break}}},this);this.records_=[]},takeRecords:function(){var a=this.records_;this.records_=[];return a}};var m,g;y.prototype={enqueue:function(a){var b=this.observer.records_,c=b.length;if(0<b.length){var d=b[c-1];d=d===a?d:!g||d!==g&&d!==m?null:g;if(d){b[c-1]=d;return}}else q.push(this.observer),p||(p=!0,n(v));b[c]=a},addListeners:function(){this.addListeners_(this.target)},addListeners_:function(a){var b=this.options;b.attributes&&a.addEventListener("DOMAttrModified",this,!0);b.characterData&&a.addEventListener("DOMCharacterDataModified",this,!0);b.childList&&a.addEventListener("DOMNodeInserted",this,!0);(b.childList||b.subtree)&&a.addEventListener("DOMNodeRemoved",this,!0)},removeListeners:function(){this.removeListeners_(this.target)},removeListeners_:function(a){var b=this.options;b.attributes&&a.removeEventListener("DOMAttrModified",this,!0);b.characterData&&a.removeEventListener("DOMCharacterDataModified",this,!0);b.childList&&a.removeEventListener("DOMNodeInserted",this,!0);(b.childList||b.subtree)&&a.removeEventListener("DOMNodeRemoved",this,!0)},addTransientObserver:function(a){if(a!==this.target){this.addListeners_(a);this.transientObservedNodes.push(a);var b=f.get(a);b||f.set(a,b=[]);b.push(this)}},removeTransientObservers:function(){var a=this.transientObservedNodes;this.transientObservedNodes=[];a.forEach(function(a){this.removeListeners_(a);a=f.get(a);for(var c=0;c<a.length;c++)if(a[c]===this){a.splice(c,1);break}},this)},handleEvent:function(a){a.stopImmediatePropagation();switch(a.type){case"DOMAttrModified":var b=a.attrName,c=a.relatedNode.namespaceURI,d=a.target,e=new t("attributes",d);e.attributeName=b;e.attributeNamespace=c;var f=a.attrChange===MutationEvent.ADDITION?null:a.prevValue;r(d,function(a){if(a.attributes&&(!a.attributeFilter||!a.attributeFilter.length||-1!==a.attributeFilter.indexOf(b)||-1!==a.attributeFilter.indexOf(c)))return a.attributeOldValue?x(f):e});break;case"DOMCharacterDataModified":d=a.target;e=t("characterData",d);f=a.prevValue;r(d,function(a){if(a.characterData)return a.characterDataOldValue?x(f):e});break;case"DOMNodeRemoved":this.addTransientObserver(a.target);case"DOMNodeInserted":var h=a.target,k;"DOMNodeInserted"===a.type?(d=[h],k=[]):(d=[],k=[h]);var l=h.previousSibling,h=h.nextSibling,e=t("childList",a.target.parentNode);e.addedNodes=d;e.removedNodes=k;e.previousSibling=l;e.nextSibling=h;r(a.relatedNode,function(a){if(a.childList)return e})}m=g=void 0}};k.JsMutationObserver=l;k.MutationObserver||(k.MutationObserver=l,l._isPolyfilled=!0)}})(self);
/*!
 * matchMedia() polyfill - Test a CSS media type/query in JS.
 * github.com/paulirish/matchMedia.js
 * Authors & copyright (c) 2012:
 * Scott Jehl, Paul Irish, Nicholas Zakas, David Knight.
 * Dual MIT/BSD license
 */
window.matchMedia||(window.matchMedia=function(){"use strict";var styleMedia=(window.styleMedia||window.media);if(!styleMedia){var style=document.createElement('style'),script=document.getElementsByTagName('script')[0],info=null;style.type='text/css';style.id='matchmediajs-test';script.parentNode.insertBefore(style,script);info=('getComputedStyle'in window)&&window.getComputedStyle(style,null)||style.currentStyle;styleMedia={matchMedium:function(media){var text='@media '+media+'{ #matchmediajs-test { width: 1px; } }';if(style.styleSheet){style.styleSheet.cssText=text;}else{style.textContent=text;}return info.width==='1px';}};}return function(media){return{matches:styleMedia.matchMedium(media||'all'),media:media||'all'};};}());
/*!
 * modified MediaHack - (c) 2013 Pomke Nohkan MIT LICENCED.
 * gist.github.com/englishextra/ff8c9dde94abe32a9d7c4a65e0f2ccac
 * removed className fallback and additionally
 * returns earlyDeviceOrientation,earlyDeviceSize
 * Add media query classes to DOM nodes
 * github.com/pomke/mediahack/blob/master/mediahack.js
 */
var earlyDeviceOrientation="",earlyDeviceSize="";(function(d){function n(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.add(i);}}}function l(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.remove(i);}}}var i={landscape:"all and (orientation:landscape)",portrait:"all and (orientation:portrait)"};var j={small:"all and (max-width:768px)",medium:"all and (min-width:768px) and (max-width:991px)",large:"all and (min-width:992px)"};for(var e in i){var o=window.matchMedia(i[e]);!function(i,e){var o=function(i){i.matches?(n(e),(earlyDeviceOrientation=e)):l(e);};o(i),i.addListener(o);}(o,e);}for(var e in j){var s=window.matchMedia(j[e]);!function(j,e){var s=function(j){j.matches?(n(e),(earlyDeviceSize=e)):l(e);};s(j),j.addListener(s);}(s,e);}}(document.documentElement.classList||""));
/*!
 * add mobile or desktop class
 * using Detect Mobile Browsers | Open source mobile phone detection
 * Regex updated: 1 August 2014
 * detectmobilebrowsers.com
 */
var earlyDeviceType;(function(a,b,c,n){var c=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(n)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0,4))?b:c;a&&c&&(a.className+=" "+c),(earlyDeviceType=c)}(document.getElementsByTagName("html")[0]||"","mobile","desktop",navigator.userAgent||navigator.vendor||window.opera));
/*!
 * add svg support class
 */
var earlySvgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/2000/svg","1.1")?b:"no-"+b;(earlySvgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svg"));
/*!
 * add svgasimg support class
 */
var earlySvgasimgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")?b:"no-"+b;(earlySvgasimgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svgasimg"));
/*!
 * add touch support class
 */
var earlyHasTouch;(function(a,b){var c="ontouchstart" in document.documentElement?b:"no-"+b;(earlyHasTouch=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","touch"));
/*!
 * return date in YYYY-MM-DD format
 */
var earlyFnGetYyyymmdd=function(){"use strict";var a=new Date,b=a.getDate(),c=a.getMonth(),c=c+1,d=a.getFullYear();10>b&&(b="0"+b);10>c&&(c="0"+c);return d+"-"+c+"-"+b;};
/*!
 * A minimal html entities decoder/encoder using DOM.
 * github.com/jussi-kalliokoski/htmlentities.js
 * htmlentities.encode('<&>'); returns '&lt;&amp;&gt;';
 * htmlentities.decode('&lt;&amp;&gt;'); returns '<&>';
 * htmlentities is a shorthand for htmlentities.encode
 */
var htmlentities=(function(a){function b(c){var d=a.createElement("div");d.appendChild(a.createTextNode(c));c=d.innerHTML;d=null;return c}b.decode=function(c){var d=a.createElement("div");d.innerHTML=c;c=d.innerText||d.textContent;d=null;return c};return(b.encode=b)}(document));
/*!
 * escape string for HTML
 * gist.github.com/englishextra/214582c0f3ba64be19c655f57f11241d
 */
var escapeHtml=function(s){return s=s.replace(/[<!="'\/>&]/g,function(s){return{"<":"&lt;","!":"&#033;","=":"&#061;",'"':"&quot;","'":"&#39;","/":"&#047;",">":"&gt;","&":"&amp;"}[s]})||"";};
/*!
 * Escape strings for use as JavaScript string literals
 * gist.github.com/englishextra/3053a4dc18c2de3c80ce7d26207681e0
 * modified github.com/joliss/js-string-escape
 */
var jsStringEscape=function(s){return(""+s).replace(/["'\\\n\r\u2028\u2029]/g,function(a){switch(a){case '"':case "'":case "\\":return"\\"+a;case "\n":return"\\n";case "\r":return"\\r";case "\u2028":return"\\u2028";case "\u2029":return"\\u2029"}})};
/*!
 * append details to title
 */
var initialDocumentTitle=document.title||"",
userBrowsingDetails=" ["+(earlyFnGetYyyymmdd()?earlyFnGetYyyymmdd():"")+(earlyDeviceType?" "+earlyDeviceType:"")+(earlyDeviceSize?" "+earlyDeviceSize:"")+(earlyDeviceOrientation?" "+earlyDeviceOrientation:"")+(earlySvgSupport?" "+earlySvgSupport:"")+(earlySvgasimgSupport?" "+earlySvgasimgSupport:"")+(earlyHasTouch?" "+earlyHasTouch:"")+"]";
document.title&&(document.title=jsStringEscape(document.title+userBrowsingDetails));
/*!
 * stackoverflow.com/questions/16053357/what-does-foreach-call-do-in-javascript
 * Instead of using [].forEach.call() or Array.prototype.forEach.call() every time
 * you do this, make a simple function out of it:
 */
var forEach=function(a,b){if(Array.prototype.forEach)Array.prototype.forEach.call(a,b);else return!1};
/*!
 * modified JavaScript Sync/Async forEach - v0.1.2 - 1/10/2012
 * github.com/cowboy/javascript-sync-async-foreach
 * Copyright (c) 2012 "Cowboy" Ben Alman; Licensed MIT
 * removed Node.js / browser support wrapper function
 * forEach(a,function(e){console.log("eachCallback: "+e);},!1});
 * forEach(a,function(e){console.log("eachCallback: "+e);},function(){console.log("doneCallback");});
 */
var forEach=function(a,b,c){var d=-1,e=a.length>>>0;(function f(g){var h,j=g===!1;do++d;while(!(d in a)&&d!==e);if(j||d===e){c&&c(!j,a);return}g=b.call({async:function(){return h=!0,f}},a[d],d,a),h||f(g)})()};
/*!
 * A function for elements selection
 * 0.1.9
 * github.com/finom/bala
 * global $ renamed to BALA, prepended var
 * a = BALA.one("#someid");
 * a = BALA.one(".someclass");
 * a = BALA(".someclass");
 */
var BALA=function(e,f,g){function c(a,b,d){d=Object.create(c.fn);a&&d.push.apply(d,a[f]?[a]:""+a===a?/</.test(a)?((b=e.createElement(b||f)).innerHTML=a,b.children):b?(b=c(b)[0])?b[g](a):d:e[g](a):"function"==typeof a?e.readyState[7]?a():e[f]("DOMContentLoaded",a):a);return d}c.fn=[];c.one=function(a,b){return c(a,b)[0]||null};return c}(document,"addEventListener","querySelectorAll");
/*!
 * requestAnimationFrame() shim by Paul Irish
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * paulirish.com/2011/requestanimationframe-for-smart-animating/
 */
window.requestAnimFrame=(function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(callback,element){window.setTimeout(callback,1000/60);};})();
/*!
 * Behaves the same as setTimeout except uses requestAnimationFrame() where possible for better performance
 * modified gist.github.com/joelambert/1002116
 * the fallback function requestAnimFrame is incorporated
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {function} fn The callback function
 * @param {int} delay The delay in milliseconds
 */
window.requestTimeout=function(fn,delay){if(!window.requestAnimationFrame&&!window.webkitRequestAnimationFrame&&!(window.mozRequestAnimationFrame&&window.mozCancelRequestAnimationFrame)&&!window.oRequestAnimationFrame&&!window.msRequestAnimationFrame){return window.setTimeout(fn,delay);};var requestAnimFrame=function(callback,element){window.setTimeout(callback,1000/60);},start=new Date().getTime(),handle=new Object();function loop(){var current=new Date().getTime(),delta=current-start;delta>=delay?fn.call():handle.value=requestAnimFrame(loop);};handle.value=requestAnimFrame(loop);return handle;};
/*!
 * Behaves the same as clearTimeout except uses cancelRequestAnimationFrame() where possible for better performance
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {int|object} fn The callback function
 */
window.clearRequestTimeout=function(handle){window.cancelAnimationFrame?window.cancelAnimationFrame(handle.value):window.webkitCancelAnimationFrame?window.webkitCancelAnimationFrame(handle.value):window.webkitCancelRequestAnimationFrame?window.webkitCancelRequestAnimationFrame(handle.value):window.mozCancelRequestAnimationFrame?window.mozCancelRequestAnimationFrame(handle.value):window.oCancelRequestAnimationFrame?window.oCancelRequestAnimationFrame(handle.value):window.msCancelRequestAnimationFrame?window.msCancelRequestAnimationFrame(handle.value):clearTimeout(handle);};
/*!
 * set and clear timeout
 * based on requestTimeout and clearRequestTimeout
 */
var setAndClearTimeout=function(fn,delay){delay=delay||500;if(!!fn&&"function"===typeof fn){var sct=requestTimeout(function(){clearRequestTimeout(sct);fn();},delay);}};
/*!
 * Behaves the same as setInterval except uses requestAnimationFrame() where possible for better performance
 * modified gist.github.com/joelambert/1002116
 * the fallback function requestAnimFrame is incorporated
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {function} fn The callback function
 * @param {int} delay The delay in milliseconds
 */
window.requestInterval=function(fn,delay){if(!window.requestAnimationFrame&&!window.webkitRequestAnimationFrame&&!(window.mozRequestAnimationFrame&&window.mozCancelRequestAnimationFrame)&&!window.oRequestAnimationFrame&&!window.msRequestAnimationFrame){return window.setInterval(fn,delay);};var requestAnimFrame=function(callback,element){window.setTimeout(callback,1000/60);},start=new Date().getTime(),handle=new Object();function loop(){var current=new Date().getTime(),delta=current-start;if(delta>=delay){fn.call();start=new Date().getTime();};handle.value=requestAnimFrame(loop);};handle.value=requestAnimFrame(loop);return handle;};
/*!
 * Behaves the same as clearInterval except uses cancelRequestAnimationFrame() where possible for better performance
 * gist.github.com/joelambert/1002116
 * gist.github.com/englishextra/873c8f78bfda7cafc905f48a963df07b
 * @param {int|object} fn The callback function
 */
window.clearRequestInterval=function(handle){window.cancelAnimationFrame?window.cancelAnimationFrame(handle.value):window.webkitCancelAnimationFrame?window.webkitCancelAnimationFrame(handle.value):window.webkitCancelRequestAnimationFrame?window.webkitCancelRequestAnimationFrame(handle.value):window.mozCancelRequestAnimationFrame?window.mozCancelRequestAnimationFrame(handle.value):window.oCancelRequestAnimationFrame?window.oCancelRequestAnimationFrame(handle.value):window.msCancelRequestAnimationFrame?window.msCancelRequestAnimationFrame(handle.value):clearInterval(handle);}
/*!
 * Accurate Javascript setInterval replacement
 * gist.github.com/manast/1185904
 * gist.github.com/englishextra/f721a0c4d12aa30f74c2e089370e09eb
 * minified with closure-compiler.appspot.com/home
 * var si = new interval(50, function(){ if(1===1){si.stop(), si = 0;}}); si.run();
 * The handle will be a number that isn't equal to 0; therefore, 0 makes a handy flag value for "no timer set".
 * stackoverflow.com/questions/5978519/setinterval-and-how-to-use-clearinterval
 */
function interval(d,f){this.baseline=void 0;this.run=function(){void 0===this.baseline&&(this.baseline=(new Date).getTime());f();var c=(new Date).getTime();this.baseline+=d;var b=d-(c-this.baseline);0>b&&(b=0);(function(d){d.timer=setTimeout(function(){d.run(c)},b)}(this))};this.stop=function(){clearTimeout(this.timer)}};
/*!
 * modified for babel crel - a small, simple, and fast DOM creation utility
 * github.com/KoryNunn/crel
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * crel(tagName/dom element [, attributes, child1, child2, childN...])
 * var element=crel('div',crel('h1','Crello World!'),crel('p','This is crel'),crel('input',{type:'number'}));
 */
var crel=(function(){function e(){var o,a=arguments,p=a[0],m=a[1],x=2,v=a.length,b=e[f];if(p=e[c](p)?p:d.createElement(p),1===v)return p;if((!l(m,t)||e[u](m)||s(m))&&(--x,m=null),v-x===1&&l(a[x],"string")&&void 0!==p[r])p[r]=a[x];else for(;v>x;++x)if(o=a[x],null!=o)if(s(o))for(var g=0;g<o.length;++g)y(p,o[g]);else y(p,o);for(var h in m)if(b[h]){var N=b[h];typeof N===n?N(p,m[h]):p[i](N,m[h])}else p[i](h,m[h]);return p}var n="function",t="object",o="nodeType",r="textContent",i="setAttribute",f="attrMap",u="isNode",c="isElement",d=typeof document===t?document:{},l=function(e,n){return typeof e===n},a=typeof Node===n?function(e){return e instanceof Node}:function(e){return e&&l(e,t)&&o in e&&l(e.ownerDocument,t)},p=function(n){return e[u](n)&&1===n[o]},s=function(e){return e instanceof Array},y=function(n,t){e[u](t)||(t=d.createTextNode(t)),n.appendChild(t)};return e[f]={},e[c]=p,e[u]=a,"undefined"!=typeof Proxy&&(e.proxy=new Proxy(e,{get:function(n,t){return!(t in e)&&(e[t]=e.bind(null,t)),e[t]}})),e}());
/*!
 * implementing fadeIn and fadeOut without jQuery
 * gist.github.com/englishextra/baaa687f6ae9c7733d560d3ec74815cd
 * modified jsfiddle.net/LzX4s/
 * changed options.complete(); to:
 * function"==typeof options.complete && options.complete();
 * usage:
 * FX.fadeIn(document.getElementById('test'), {
 * 	duration: 2000,
 * 	complete: function() {
 * 		alert('Complete');
 * 	}
 * });
 */
(function(){var FX={easing:{linear:function(progress){return progress;},quadratic:function(progress){return Math.pow(progress,2);},swing:function(progress){return 0.5-Math.cos(progress*Math.PI)/2;},circ:function(progress){return 1-Math.sin(Math.acos(progress));},back:function(progress,x){return Math.pow(progress,2)*((x+1)*progress-x);},bounce:function(progress){for(var a=0,b=1,result;1;a+=b,b/=2){if(progress>=(7-4*a)/11){return-Math.pow((11-6*a-11*progress)/4,2)+Math.pow(b,2);}}},elastic:function(progress,x){return Math.pow(2,10*(progress-1))*Math.cos(20*Math.PI*x/3*progress);}},animate:function(options){var start=new Date;var id=setInterval(function(){var timePassed=new Date-start;var progress=timePassed/options.duration;if(progress>1){progress=1;};options.progress=progress;var delta=options.delta(progress);options.step(delta);if(progress==1){clearInterval(id);"function"==typeof options.complete&&options.complete();}},options.delay||10);},fadeOut:function(element,options){var to=1;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to-delta;}});},fadeIn:function(element,options){var to=0;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to+delta;}});}};window.FX=FX;}());
/*!
 * Fade In
 * gist.github.com/englishextra/cbf5137a95ed5e02927cd3e19e271bae
 * youmightnotneedjquery.com
 * fadeIn(el);
 * fadeIn(el, "inline-block");
 */
var fadeIn=function(el){el.style.opacity=0;var last=+new Date();var tick=function(){el.style.opacity=+el.style.opacity+(new Date()-last)/400;last=+new Date();if(+el.style.opacity<1){(window.requestAnimationFrame&&requestAnimationFrame(tick))||setTimeout(tick,160);}};tick();};
/*!
 * Fade Out
 * gist.github.com/englishextra/818ba5b558f9c15f9776684b8e8feaf9
 * youmightnotneedjquery.com
 * chrisbuttery.com/articles/fade-in-fade-out-with-javascript/
 * fadeOut(el);
 */
var fadeOut=function(el){el.style.opacity=1;(function fade(){if((el.style.opacity-=.1)<0){el.style.display="none";}else{requestAnimationFrame(fade);}})();};
/*!
 * modified for babel Zenscroll 3.2.2
 * github.com/zengabor/zenscroll
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * Copyright 2015�2016 Gabor Lenard
 * minified with closure-compiler.appspot.com/home
 * github.com/zengabor/zenscroll/blob/dist/zenscroll.js
 */
var zenscroll=(function(){"use strict";if("undefined"==typeof window||!("document"in window)){return{};}var t=function(t,e,n){e=e||999,n||0===n||(n=9);var o,i=document.documentElement,r=function(){return"getComputedStyle"in window&&"smooth"===window.getComputedStyle(t?t:document.body)["scroll-behavior"]},c=function(){return t?t.scrollTop:window.scrollY||i.scrollTop},u=function(){return t?Math.min(t.offsetHeight,window.innerHeight):window.innerHeight||i.clientHeight},f=function(e){return t?e.offsetTop:e.getBoundingClientRect().top+c()-i.offsetTop},l=function(){clearTimeout(o),o=0},a=function(n,f,a){if(l(),r())(t||window).scrollTo(0,n),a&&a();else{var d=c(),s=Math.max(n,0)-d;f=f||Math.min(Math.abs(s),e);var m=(new Date).getTime();!function e(){o=setTimeout(function(){var n=Math.min(((new Date).getTime()-m)/f,1),o=Math.max(Math.floor(d+s*(n<.5?2*n*n:n*(4-2*n)-1)),0);t?t.scrollTop=o:window.scrollTo(0,o),n<1&&u()+o<(t||i).scrollHeight?e():(setTimeout(l,99),a&&a())},9)}()}},d=function(t,e,o){a(f(t)-n,e,o)},s=function(t,e,o){var i=t.getBoundingClientRect().height,r=f(t),l=r+i,s=u(),m=c(),h=m+s;r-n<m||i+n>s?d(t,e,o):l+n>h?a(l-s+n,e,o):o&&o()},m=function(t,e,n,o){a(Math.max(f(t)-u()/2+(n||t.getBoundingClientRect().height/2),0),e,o)},h=function(t,o){t&&(e=t),(0===o||o)&&(n=o)};return{setup:h,to:d,toY:a,intoView:s,center:m,stop:l,moving:function(){return!!o}}},e=t();if("addEventListener"in window&&"smooth"!==document.body.style.scrollBehavior&&!window.noZensmooth){var n=function(t){try{history.replaceState({},"",window.location.href.split("#")[0]+t)}catch(t){}};window.addEventListener("click",function(t){for(var o=t.target;o&&"A"!==o.tagName;)o=o.parentNode;if(!(!o||1!==t.which||t.shiftKey||t.metaKey||t.ctrlKey||t.altKey)){var i=o.getAttribute("href")||"";if(0===i.indexOf("#"))if("#"===i)t.preventDefault(),e.toY(0),n("");else{var r=o.hash.substring(1),c=document.getElementById(r);c&&(t.preventDefault(),e.to(c),n("#"+r))}}},!1)}return{createScroller:t,setup:e.setup,to:e.to,toY:e.toY,intoView:e.intoView,center:e.center,stop:e.stop,moving:e.moving};}());
/*!
 * Scroll to top with Zenscroll and fallback
 */
var scrollToTop=function(){var w=window,g=function(){w.zenscroll?zenscroll.toY(0):w.scrollTo(0,0);};w.setImmediate?setImmediate(function(){g()}):setTimeout(function(){g();});};
/*!
 * enquire.js v2.1.2 - Awesome Media Queries in JavaScript
 * Copyright (c) 2014 Nick Williams - http://wicky.nillia.ms/enquire.js
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */
!function(a,b,c){var d=window.matchMedia;"undefined"!=typeof module&&module.exports?module.exports=c(d):"function"==typeof define&&define.amd?define(function(){return b[a]=c(d)}):b[a]=c(d)}("enquire",this,function(a){"use strict";function b(a,b){var c,d=0,e=a.length;for(d;e>d&&(c=b(a[d],d),c!==!1);d++);}function c(a){return"[object Array]"===Object.prototype.toString.apply(a)}function d(a){return"function"==typeof a}function e(a){this.options=a,!a.deferSetup&&this.setup()}function f(b,c){this.query=b,this.isUnconditional=c,this.handlers=[],this.mql=a(b);var d=this;this.listener=function(a){d.mql=a,d.assess()},this.mql.addListener(this.listener)}function g(){if(!a)throw new Error("matchMedia not present, legacy browsers require a polyfill");this.queries={},this.browserIsIncapable=!a("only all").matches}return e.prototype={setup:function(){this.options.setup&&this.options.setup(),this.initialised=!0},on:function(){!this.initialised&&this.setup(),this.options.match&&this.options.match()},off:function(){this.options.unmatch&&this.options.unmatch()},destroy:function(){this.options.destroy?this.options.destroy():this.off()},equals:function(a){return this.options===a||this.options.match===a}},f.prototype={addHandler:function(a){var b=new e(a);this.handlers.push(b),this.matches()&&b.on()},removeHandler:function(a){var c=this.handlers;b(c,function(b,d){return b.equals(a)?(b.destroy(),!c.splice(d,1)):void 0})},matches:function(){return this.mql.matches||this.isUnconditional},clear:function(){b(this.handlers,function(a){a.destroy()}),this.mql.removeListener(this.listener),this.handlers.length=0},assess:function(){var a=this.matches()?"on":"off";b(this.handlers,function(b){b[a]()})}},g.prototype={register:function(a,e,g){var h=this.queries,i=g&&this.browserIsIncapable;return h[a]||(h[a]=new f(a,i)),d(e)&&(e={match:e}),c(e)||(e=[e]),b(e,function(b){d(b)&&(b={match:b}),h[a].addHandler(b)}),this},unregister:function(a,b){var c=this.queries[a];return c&&(b?c.removeHandler(b):(c.clear(),delete this.queries[a])),this}},new g});
/*!
 * Plain javascript replacement for jQuery's .ready()
 * so code can be scheduled to run when the document is ready
 * github.com/jfriend00/docReady
 * docReady(function(){});
 * simple substitute by Christoph at stackoverflow.com/questions/8100576/how-to-check-if-dom-is-ready-without-a-framework
 * (function(){var a=document.readyState;"interactive"===a||"complete"===a?(function(){}()):setTimeout(arguments.callee,100)})();
 */
(function(funcName,baseObj){"use strict";funcName=funcName||"docReady";baseObj=baseObj||window;var readyList=[];var readyFired=false;var readyEventHandlersInstalled=false;function ready(){if(!readyFired){readyFired=true;for(var i=0;i<readyList.length;i++){readyList[i].fn.call(window,readyList[i].ctx);}readyList=[];}}function readyStateChange(){if(document.readyState==="complete"){ready();}}baseObj[funcName]=function(callback,context){if(readyFired){setTimeout(function(){callback(context);},1);return;}else{readyList.push({fn:callback,ctx:context});}if(document.readyState==="complete"||(!document.attachEvent&&document.readyState==="interactive")){setTimeout(ready,1);}else if(!readyEventHandlersInstalled){if(document.addEventListener){document.addEventListener("DOMContentLoaded",ready,false);window.addEventListener("load",ready,false);}else{document.attachEvent("onreadystatechange",readyStateChange);window.attachEvent("onload",ready);}readyEventHandlersInstalled=true;}}})("docReady",window);
/*!
 * modified for babel Evento - v1.0.0
 * by Erik Royall <erikroyalL@hotmail.com> (http://erikroyall.github.io)
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 * Dual licensed under MIT and GPL
 * Array.prototype.indexOf shim
 * developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
 * gist.github.com/erikroyall/6618740
 * gist.github.com/englishextra/3a959e4da0fcc268b140
 * evento.add(window,"load",function(){});
 */
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement){'use strict';if(this==null){throw new TypeError();}var n,k,t=Object(this),len=t.length>>>0;if(len===0){return-1;}n=0;if(arguments.length>1){n=Number(arguments[1]);if(n!=n){n=0;}else if(n!=0&&n!=Infinity&&n!=-Infinity){n=(n>0||-1)*Math.floor(Math.abs(n));}}if(n>=len){return-1;}for(k=n>=0?n:Math.max(len-Math.abs(n),0);k<len;k++){if(k in t&&t[k]===searchElement){return k;}}return-1;};}var evento=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}var win=window,doc=win.document,_handlers={},addEvent,removeEvent,triggerEvent;addEvent=(function(){if(typeof doc.addEventListener==="function"){return function(el,evt,fn){el.addEventListener(evt,fn,false);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else if(typeof doc.attachEvent==="function"){return function(el,evt,fn){el.attachEvent(evt,fn);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else{return function(el,evt,fn){el["on"+evt]=fn;_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}}());removeEvent=(function(){if(typeof doc.removeEventListener==="function"){return function(el,evt,fn){el.removeEventListener(evt,fn,false);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else if(typeof doc.detachEvent==="function"){return function(el,evt,fn){el.detachEvent(evt,fn);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else{return function(el,evt,fn){el["on"+evt]=undefined;Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}}());triggerEvent=function(el,evt){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];for(var _i=0,_l=_handlers[el][evt].length;_i<_l;_i+=1){_handlers[el][evt][_i]();}};return{add:addEvent,remove:removeEvent,trigger:triggerEvent,_handlers:_handlers};}());
/*!
 * Waypoints - 4.0.0
 * Copyright � 2011-2015 Caleb Troughton
 * Licensed under the MIT license.
 * github.com/imakewebthings/waypoints/blog/master/licenses.txt
 */
!function(){"use strict";function t(n){if(!n)throw new Error("No options passed to Waypoint constructor");if(!n.element)throw new Error("No element option passed to Waypoint constructor");if(!n.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,n),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=n.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var n in i)e.push(i[n]);for(var o=0,r=e.length;r>o;o++)e[o][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.invokeAll("enable")},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=o.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,n[t.waypointContextKey]=this,i+=1,this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,n={},o=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical);t&&e&&(this.adapter.off(".waypoints"),delete n[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,o.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||o.isTouch)&&(e.didScroll=!0,o.requestAnimationFrame(t))})},e.prototype.handleResize=function(){o.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var n=e[i],o=n.newScroll>n.oldScroll,r=o?n.forward:n.backward;for(var s in this.waypoints[i]){var l=this.waypoints[i][s],a=n.oldScroll<l.triggerPoint,h=n.newScroll>=l.triggerPoint,p=a&&h,u=!a&&!h;(p||u)&&(l.queueTrigger(r),t[l.group.id]=l.group)}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?o.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?o.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var n=0,o=t.length;o>n;n++)t[n].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),n={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var l in this.waypoints[r]){var a,h,p,u,c,f=this.waypoints[r][l],d=f.options.offset,y=f.triggerPoint,g=0,w=null==y;f.element!==f.element.window&&(g=f.adapter.offset()[s.offsetProp]),"function"==typeof d?d=d.apply(f):"string"==typeof d&&(d=parseFloat(d),f.options.offset.indexOf("%")>-1&&(d=Math.ceil(s.contextDimension*d/100))),a=s.contextScroll-s.contextOffset,f.triggerPoint=g+a-d,h=y<s.oldScroll,p=f.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!w&&u?(f.queueTrigger(s.backward),n[f.group.id]=f.group):!w&&c?(f.queueTrigger(s.forward),n[f.group.id]=f.group):w&&s.oldScroll>=f.triggerPoint&&(f.queueTrigger(s.forward),n[f.group.id]=f.group)}}return o.requestAnimationFrame(function(){for(var t in n)n[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in n)n[t].refresh()},e.findByElement=function(t){return n[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},o.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},o.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),n[this.axis][this.name]=this}var n={vertical:{},horizontal:{}},o=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var n=this.triggerQueues[i],o="up"===i||"left"===i;n.sort(o?e:t);for(var r=0,s=n.length;s>r;r+=1){var l=n[r];(l.options.continuous||r===n.length-1)&&l.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints),n=i===this.waypoints.length-1;return n?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=o.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return n[t.axis][t.name]||new i(t)},o.Group=i}(),function(){"use strict";function t(t){return t===t.window}function e(e){return t(e)?e:e.defaultView}function i(t){this.element=t,this.handlers={}}var n=window.Waypoint;i.prototype.innerHeight=function(){var e=t(this.element);return e?this.element.innerHeight:this.element.clientHeight},i.prototype.innerWidth=function(){var e=t(this.element);return e?this.element.innerWidth:this.element.clientWidth},i.prototype.off=function(t,e){function i(t,e,i){for(var n=0,o=e.length-1;o>n;n++){var r=e[n];i&&i!==r||t.removeEventListener(r)}}var n=t.split("."),o=n[0],r=n[1],s=this.element;if(r&&this.handlers[r]&&o)i(s,this.handlers[r][o],e),this.handlers[r][o]=[];else if(o)for(var l in this.handlers)i(s,this.handlers[l][o]||[],e),this.handlers[l][o]=[];else if(r&&this.handlers[r]){for(var a in this.handlers[r])i(s,this.handlers[r][a],e);this.handlers[r]={}}},i.prototype.offset=function(){if(!this.element.ownerDocument)return null;var t=this.element.ownerDocument.documentElement,i=e(this.element.ownerDocument),n={top:0,left:0};return this.element.getBoundingClientRect&&(n=this.element.getBoundingClientRect()),{top:n.top+i.pageYOffset-t.clientTop,left:n.left+i.pageXOffset-t.clientLeft}},i.prototype.on=function(t,e){var i=t.split("."),n=i[0],o=i[1]||"__default",r=this.handlers[o]=this.handlers[o]||{},s=r[n]=r[n]||[];s.push(e),this.element.addEventListener(n,e)},i.prototype.outerHeight=function(e){var i,n=this.innerHeight();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginTop,10),n+=parseInt(i.marginBottom,10)),n},i.prototype.outerWidth=function(e){var i,n=this.innerWidth();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginLeft,10),n+=parseInt(i.marginRight,10)),n},i.prototype.scrollLeft=function(){var t=e(this.element);return t?t.pageXOffset:this.element.scrollLeft},i.prototype.scrollTop=function(){var t=e(this.element);return t?t.pageYOffset:this.element.scrollTop},i.extend=function(){function t(t,e){if("object"==typeof t&&"object"==typeof e)for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t}for(var e=Array.prototype.slice.call(arguments),i=1,n=e.length;n>i;i++)t(e[0],e[i]);return e[0]},i.inArray=function(t,e,i){return null==e?-1:e.indexOf(t,i)},i.isEmptyObject=function(t){for(var e in t)return!1;return!0},n.adapters.push({name:"noframework",Adapter:i}),n.Adapter=i}();
/*!
 * modified for babel Reqwest! A general purpose XHR connection manager
 * license MIT (c) Dustin Diaz 2015
 * github.com/ded/reqwest
 * this changed to self
 */
;(function(e,t,n){t[e]=n()}("reqwest",self,function(){function succeed(e){var t=protocolRe.exec(e.url);return t=t&&t[1]||context.location.protocol,httpsRe.test(t)?twoHundo.test(e.request.status):!!e.request.response}function handleReadyState(e,t,n){return function(){if(e._aborted)return n(e.request);if(e._timedOut)return n(e.request,"Request is aborted: timeout");e.request&&e.request[readyState]==4&&(e.request.onreadystatechange=noop,succeed(e)?t(e.request):n(e.request))}}function setHeaders(e,t){var n=t.headers||{},r;n.Accept=n.Accept||defaultHeaders.accept[t.type]||defaultHeaders.accept["*"];var i=typeof FormData!="undefined"&&t.data instanceof FormData;!t.crossOrigin&&!n[requestedWith]&&(n[requestedWith]=defaultHeaders.requestedWith),!n[contentType]&&!i&&(n[contentType]=t.contentType||defaultHeaders.contentType);for(r in n)n.hasOwnProperty(r)&&"setRequestHeader"in e&&e.setRequestHeader(r,n[r])}function setCredentials(e,t){typeof t.withCredentials!="undefined"&&typeof e.withCredentials!="undefined"&&(e.withCredentials=!!t.withCredentials)}function generalCallback(e){lastValue=e}function urlappend(e,t){return e+(/\?/.test(e)?"&":"?")+t}function handleJsonp(e,t,n,r){var i=uniqid++,s=e.jsonpCallback||"callback",o=e.jsonpCallbackName||reqwest.getcallbackPrefix(i),u=new RegExp("((^|\\?|&)"+s+")=([^&]+)"),a=r.match(u),f=doc.createElement("script"),l=0,c=navigator.userAgent.indexOf("MSIE 10.0")!==-1;return a?a[3]==="?"?r=r.replace(u,"$1="+o):o=a[3]:r=urlappend(r,s+"="+o),context[o]=generalCallback,f.type="text/javascript",f.src=r,f.async=!0,typeof f.onreadystatechange!="undefined"&&!c&&(f.htmlFor=f.id="_reqwest_"+i),f.onload=f.onreadystatechange=function(){if(f[readyState]&&f[readyState]!=="complete"&&f[readyState]!=="loaded"||l)return!1;f.onload=f.onreadystatechange=null,f.onclick&&f.onclick(),t(lastValue),lastValue=undefined,head.removeChild(f),l=1},head.appendChild(f),{abort:function(){f.onload=f.onreadystatechange=null,n({},"Request is aborted: timeout",{}),lastValue=undefined,head.removeChild(f),l=1}}}function getRequest(e,t){var n=this.o,r=(n.method||"GET").toUpperCase(),i=typeof n=="string"?n:n.url,s=n.processData!==!1&&n.data&&typeof n.data!="string"?reqwest.toQueryString(n.data):n.data||null,o,u=!1;return(n["type"]=="jsonp"||r=="GET")&&s&&(i=urlappend(i,s),s=null),n["type"]=="jsonp"?handleJsonp(n,e,t,i):(o=n.xhr&&n.xhr(n)||xhr(n),o.open(r,i,n.async===!1?!1:!0),setHeaders(o,n),setCredentials(o,n),context[xDomainRequest]&&o instanceof context[xDomainRequest]?(o.onload=e,o.onerror=t,o.onprogress=function(){},u=!0):o.onreadystatechange=handleReadyState(this,e,t),n.before&&n.before(o),u?setTimeout(function(){o.send(s)},200):o.send(s),o)}function Reqwest(e,t){this.o=e,this.fn=t,init.apply(this,arguments)}function setType(e){if(e===null)return undefined;if(e.match("json"))return"json";if(e.match("javascript"))return"js";if(e.match("text"))return"html";if(e.match("xml"))return"xml"}function init(o,fn){function complete(e){o.timeout&&clearTimeout(self.timeout),self.timeout=null;while(self._completeHandlers.length>0)self._completeHandlers.shift()(e)}function success(resp){var type=o.type||resp&&setType(resp.getResponseHeader("Content-Type"));resp=type!=="jsonp"?self.request:resp;var filteredResponse=globalSetupOptions.dataFilter(resp.responseText,type),r=filteredResponse;try{resp.responseText=r}catch(e){}if(r)switch(type){case"json":try{resp=context.JSON?context.JSON.parse(r):eval("("+r+")")}catch(err){return error(resp,"Could not parse JSON in response",err)}break;case"js":resp=eval(r);break;case"html":resp=r;break;case"xml":resp=resp.responseXML&&resp.responseXML.parseError&&resp.responseXML.parseError.errorCode&&resp.responseXML.parseError.reason?null:resp.responseXML}self._responseArgs.resp=resp,self._fulfilled=!0,fn(resp),self._successHandler(resp);while(self._fulfillmentHandlers.length>0)resp=self._fulfillmentHandlers.shift()(resp);complete(resp)}function timedOut(){self._timedOut=!0,self.request.abort()}function error(e,t,n){e=self.request,self._responseArgs.resp=e,self._responseArgs.msg=t,self._responseArgs.t=n,self._erred=!0;while(self._errorHandlers.length>0)self._errorHandlers.shift()(e,t,n);complete(e)}this.url=typeof o=="string"?o:o.url,this.timeout=null,this._fulfilled=!1,this._successHandler=function(){},this._fulfillmentHandlers=[],this._errorHandlers=[],this._completeHandlers=[],this._erred=!1,this._responseArgs={};var self=this;fn=fn||function(){},o.timeout&&(this.timeout=setTimeout(function(){timedOut()},o.timeout)),o.success&&(this._successHandler=function(){o.success.apply(o,arguments)}),o.error&&this._errorHandlers.push(function(){o.error.apply(o,arguments)}),o.complete&&this._completeHandlers.push(function(){o.complete.apply(o,arguments)}),this.request=getRequest.call(this,success,error)}function reqwest(e,t){return new Reqwest(e,t)}function normalize(e){return e?e.replace(/\r?\n/g,"\r\n"):""}function serial(e,t){var n=e.name,r=e.tagName.toLowerCase(),i=function(e){e&&!e.disabled&&t(n,normalize(e.attributes.value&&e.attributes.value.specified?e.value:e.text))},s,o,u,a;if(e.disabled||!n)return;switch(r){case"input":/reset|button|image|file/i.test(e.type)||(s=/checkbox/i.test(e.type),o=/radio/i.test(e.type),u=e.value,(!s&&!o||e.checked)&&t(n,normalize(s&&u===""?"on":u)));break;case"textarea":t(n,normalize(e.value));break;case"select":if(e.type.toLowerCase()==="select-one")i(e.selectedIndex>=0?e.options[e.selectedIndex]:null);else for(a=0;e.length&&a<e.length;a++)e.options[a].selected&&i(e.options[a])}}function eachFormElement(){var e=this,t,n,r=function(t,n){var r,i,s;for(r=0;r<n.length;r++){s=t[byTag](n[r]);for(i=0;i<s.length;i++)serial(s[i],e)}};for(n=0;n<arguments.length;n++)t=arguments[n],/input|select|textarea/i.test(t.tagName)&&serial(t,e),r(t,["input","select","textarea"])}function serializeQueryString(){return reqwest.toQueryString(reqwest.serializeArray.apply(null,arguments))}function serializeHash(){var e={};return eachFormElement.apply(function(t,n){t in e?(e[t]&&!isArray(e[t])&&(e[t]=[e[t]]),e[t].push(n)):e[t]=n},arguments),e}function buildParams(e,t,n,r){var i,s,o,u=/\[\]$/;if(isArray(t))for(s=0;t&&s<t.length;s++)o=t[s],n||u.test(e)?r(e,o):buildParams(e+"["+(typeof o=="object"?s:"")+"]",o,n,r);else if(t&&t.toString()==="[object Object]")for(i in t)buildParams(e+"["+i+"]",t[i],n,r);else r(e,t)}var context=this;if("window"in context)var doc=document,byTag="getElementsByTagName",head=doc[byTag]("head")[0];else{var XHR2;try{XHR2=require("xhr2")}catch(ex){throw new Error("Peer dependency `xhr2` required! Please npm install xhr2")}}var httpsRe=/^http/,protocolRe=/(^\w+):\/\//,twoHundo=/^(20\d|1223)$/,readyState="readyState",contentType="Content-Type",requestedWith="X-Requested-With",uniqid=0,callbackPrefix="reqwest_"+ +(new Date),lastValue,xmlHttpRequest="XMLHttpRequest",xDomainRequest="XDomainRequest",noop=function(){},isArray=typeof Array.isArray=="function"?Array.isArray:function(e){return e instanceof Array},defaultHeaders={contentType:"application/x-www-form-urlencoded",requestedWith:xmlHttpRequest,accept:{"*":"text/javascript, text/html, application/xml, text/xml, */*",xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript",js:"application/javascript, text/javascript"}},xhr=function(e){if(e.crossOrigin===!0){var t=context[xmlHttpRequest]?new XMLHttpRequest:null;if(t&&"withCredentials"in t)return t;if(context[xDomainRequest])return new XDomainRequest;throw new Error("Browser does not support cross-origin requests")}return context[xmlHttpRequest]?new XMLHttpRequest:XHR2?new XHR2:new ActiveXObject("Microsoft.XMLHTTP")},globalSetupOptions={dataFilter:function(e){return e}};return Reqwest.prototype={abort:function(){this._aborted=!0,this.request.abort()},retry:function(){init.call(this,this.o,this.fn)},then:function(e,t){return e=e||function(){},t=t||function(){},this._fulfilled?this._responseArgs.resp=e(this._responseArgs.resp):this._erred?t(this._responseArgs.resp,this._responseArgs.msg,this._responseArgs.t):(this._fulfillmentHandlers.push(e),this._errorHandlers.push(t)),this},always:function(e){return this._fulfilled||this._erred?e(this._responseArgs.resp):this._completeHandlers.push(e),this},fail:function(e){return this._erred?e(this._responseArgs.resp,this._responseArgs.msg,this._responseArgs.t):this._errorHandlers.push(e),this},"catch":function(e){return this.fail(e)}},reqwest.serializeArray=function(){var e=[];return eachFormElement.apply(function(t,n){e.push({name:t,value:n})},arguments),e},reqwest.serialize=function(){if(arguments.length===0)return"";var e,t,n=Array.prototype.slice.call(arguments,0);return e=n.pop(),e&&e.nodeType&&n.push(e)&&(e=null),e&&(e=e.type),e=="map"?t=serializeHash:e=="array"?t=reqwest.serializeArray:t=serializeQueryString,t.apply(null,n)},reqwest.toQueryString=function(e,t){var n,r,i=t||!1,s=[],o=encodeURIComponent,u=function(e,t){t="function"==typeof t?t():t==null?"":t,s[s.length]=o(e)+"="+o(t)};if(isArray(e))for(r=0;e&&r<e.length;r++)u(e[r].name,e[r].value);else for(n in e)e.hasOwnProperty(n)&&buildParams(n,e[n],i,u);return s.join("&").replace(/%20/g,"+")},reqwest.getcallbackPrefix=function(){return callbackPrefix},reqwest.compat=function(e,t){return e&&(e.type&&(e.method=e.type)&&delete e.type,e.dataType&&(e.type=e.dataType),e.jsonpCallback&&(e.jsonpCallbackName=e.jsonpCallback)&&delete e.jsonpCallback,e.jsonp&&(e.jsonpCallback=e.jsonp)),new Reqwest(e,t)},reqwest.ajaxSetup=function(e){e=e||{};for(var t in e)globalSetupOptions[t]=e[t]},reqwest}));
/*!
 * @overview es6-promise - a tiny implementation of Promises/A+.
 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
 * @license   Licensed under MIT license
 *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
 * @version   3.2.2+39aa2571
 */
(function(){function S(){return function(){process.nextTick(l)}}function T(){return function(){H(l)}}function U(){var a=0,b=new I(l),c=document.createTextNode("");b.observe(c,{characterData:!0});return function(){c.data=a=++a%2}}function V(){var a=new MessageChannel;a.port1.onmessage=l;return function(){a.port2.postMessage(0)}}function J(){return function(){setTimeout(l,1)}}function l(){for(var a=0;a<n;a+=2)(0,p[a])(p[a+1]),p[a]=void 0,p[a+1]=void 0;n=0}function W(){try{var a=require("vertx");H=a.runOnLoop||a.runOnContext;return T()}catch(b){return J()}}function q(){}function K(a){try{return a.then}catch(b){return y.error=b,y}}function X(a,b,c,d){try{a.call(b,c,d)}catch(f){return f}}function Y(a,b,c){k(function(a){var f=!1,e=X(c,b,function(c){f||(f=!0,b!==c?u(a,c):m(a,c))},function(b){f||(f=!0,h(a,b))},"Settle: "+(a._label||" unknown promise"));!f&&e&&(f=!0,h(a,e))},a)}function Z(a,b){b._state===v?m(a,b._result):b._state===r?h(a,b._result):B(b,void 0,function(b){u(a,b)},function(b){h(a,b)})}function L(a,b,c){b.constructor===a.constructor&&c===C&&constructor.resolve===D?Z(a,b):c===y?h(a,y.error):void 0===c?m(a,b):"function"===typeof c?Y(a,b,c):m(a,b)}function u(a,b){a===b?h(a,new TypeError("You cannot resolve a promise with itself")):"function"===typeof b||"object"===typeof b&&null!==b?L(a,b,K(b)):m(a,b)}function aa(a){a._onerror&&a._onerror(a._result);E(a)}function m(a,b){a._state===t&&(a._result=b,a._state=v,0!==a._subscribers.length&&k(E,a))}function h(a,b){a._state===t&&(a._state=r,a._result=b,k(aa,a))}function B(a,b,c,d){var f=a._subscribers,e=f.length;a._onerror=null;f[e]=b;f[e+v]=c;f[e+r]=d;0===e&&a._state&&k(E,a)}function E(a){var b=a._subscribers,c=a._state;if(0!==b.length){for(var d,f,e=a._result,g=0;g<b.length;g+=3)d=b[g],f=b[g+c],d?M(c,d,f,e):f(e);a._subscribers.length=0}}function N(){this.error=null}function M(a,b,c,d){var f="function"===typeof c,e,g,k,l;if(f){try{e=c(d)}catch(n){F.error=n,e=F}e===F?(l=!0,g=e.error,e=null):k=!0;if(b===e){h(b,new TypeError("A promises callback cannot return that same promise."));return}}else e=d,k=!0;b._state===t&&(f&&k?u(b,e):l?h(b,g):a===v?m(b,e):a===r&&h(b,e))}function ba(a,b){try{b(function(b){u(a,b)},function(b){h(a,b)})}catch(c){h(a,c)}}function O(a){a[z]=P++;a._state=void 0;a._result=void 0;a._subscribers=[]}function g(a){this[z]=P++;this._result=this._state=void 0;this._subscribers=[];if(q!==a){if("function"!==typeof a)throw new TypeError("You must pass a resolver function as the first argument to the promise constructor");if(this instanceof g)ba(this,a);else throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");}}function w(a,b){this._instanceConstructor=a;this.promise=new a(q);this.promise[z]||O(this.promise);Q(b)?(this._input=b,this._remaining=this.length=b.length,this._result=Array(this.length),0===this.length?m(this.promise,this._result):(this.length=this.length||0,this._enumerate(),0===this._remaining&&m(this.promise,this._result))):h(this.promise,Error("Array Methods must be provided an Array"))}var Q=Array.isArray?Array.isArray:function(a){return"[object Array]"===Object.prototype.toString.call(a)},n=0,H,G,k=function(a,b){p[n]=a;p[n+1]=b;n+=2;2===n&&(G?G(l):R())},x="undefined"!==typeof window?window:void 0,A=x||{},I=A.MutationObserver||A.WebKitMutationObserver,A="undefined"===typeof self&&"undefined"!==typeof process&&"[object process]"==={}.toString.call(process),ca="undefined"!==typeof Uint8ClampedArray&&"undefined"!==typeof importScripts&&"undefined"!==typeof MessageChannel,p=Array(1E3),R;R=A?S():I?U():ca?V():void 0===x&&"function"===typeof require?W():J();var C=function(a,b){var c=this,d=new this.constructor(q);void 0===d[z]&&O(d);var f=c._state;if(f){var e=arguments[f-1];k(function(){M(f,d,e,c._result)})}else B(c,d,a,b);return d},D=function(a){if(a&&"object"===typeof a&&a.constructor===this)return a;var b=new this(q);u(b,a);return b},z=Math.random().toString(36).substring(16),t=void 0,v=1,r=2,y=new N,F=new N,P=0;g.all=function(a){return(new da(this,a)).promise};g.race=function(a){var b=this;return Q(a)?new b(function(c,d){for(var f=a.length,e=0;e<f;e++)b.resolve(a[e]).then(c,d)}):new b(function(a,b){b(new TypeError("You must pass an array to race."))})};g.resolve=D;g.reject=function(a){var b=new this(q);h(b,a);return b};g._setScheduler=function(a){G=a};g._setAsap=function(a){k=a};g._asap=k;g.prototype={constructor:g,then:C,"catch":function(a){return this.then(null,a)}};var da=w;w.prototype._enumerate=function(){for(var a=this.length,b=this._input,c=0;this._state===t&&c<a;c++)this._eachEntry(b[c],c)};w.prototype._eachEntry=function(a,b){var c=this._instanceConstructor,d=c.resolve;d===D?(d=K(a),d===C&&a._state!==t?this._settledAt(a._state,b,a._result):"function"!==typeof d?(this._remaining--,this._result[b]=a):c===g?(c=new c(q),L(c,a,d),this._willSettleAt(c,b)):this._willSettleAt(new c(function(b){b(a)}),b)):this._willSettleAt(d(a),b)};w.prototype._settledAt=function(a,b,c){var d=this.promise;d._state===t&&(this._remaining--,a===r?h(d,c):this._result[b]=c);0===this._remaining&&m(d,this._result)};w.prototype._willSettleAt=function(a,b){var c=this;B(a,void 0,function(a){c._settledAt(v,b,a)},function(a){c._settledAt(r,b,a)})};x=function(){var a;if("undefined"!==typeof global)a=global;else if("undefined"!==typeof self)a=self;else try{a=Function("return this")()}catch(c){throw Error("polyfill failed because global object is unavailable in this environment");}var b=a.Promise;if(!b||"[object Promise]"!==Object.prototype.toString.call(b.resolve())||b.cast)a.Promise=g};g.Promise=g;g.polyfill=x;"function"===typeof define&&define.amd?define(function(){return g}):"undefined"!==typeof module&&module.exports?module.exports=g:"undefined"!==typeof this&&(this.Promise=g);x()}).call(this);
/*!
 * A window.fetch JavaScript polyfill. http://github.github.io/fetch/
 * github.com/github/fetch
 * The global fetch function is an easier way to make web requests
 * and handle responses than using an XMLHttpRequest.
 * This polyfill is written as closely as possible
 * to the standard Fetch specification at https://fetch.spec.whatwg.org.
 */
var $jscomp={scope:{},getGlobal:function(b){return"undefined"!=typeof window&&window===b?b:"undefined"!=typeof global?global:b}};$jscomp.global=$jscomp.getGlobal(this);$jscomp.patches={};$jscomp.patch=function(b,e){($jscomp.patches[b]=$jscomp.patches[b]||[]).push(e);for(var h=$jscomp.global,f=b.split("."),c=0;c<f.length-1&&h;c++)h=h[f[c]];f=f[f.length-1];h&&h[f]instanceof Function&&(h[f]=e(h[f]))};$jscomp.defineProperty="function"==typeof Object.defineProperties?Object.defineProperty:function(b,e,h){if(h.get||h.set)throw new TypeError("ES3 does not support getters and setters.");b!=Array.prototype&&b!=Object.prototype&&(b[e]=h.value)};$jscomp.SYMBOL_PREFIX="jscomp_symbol_";$jscomp.initSymbol=function(){$jscomp.initSymbol=function(){};if(!$jscomp.global.Symbol){$jscomp.global.Symbol=$jscomp.Symbol;var b=[],e=function(e){return function(f){b=[];f=e(f);for(var c=[],l=0,r=f.length;l<r;l++){var m;a:if(m=f[l],m.length<$jscomp.SYMBOL_PREFIX.length)m=!1;else{for(var p=0;p<$jscomp.SYMBOL_PREFIX.length;p++)if(m[p]!=$jscomp.SYMBOL_PREFIX[p]){m=!1;break a}m=!0}m?b.push(f[l]):c.push(f[l])}return c}};$jscomp.patch("Object.keys",e);$jscomp.patch("Object.getOwnPropertyNames",e);$jscomp.patch("Object.getOwnPropertySymbols",function(h){return function(f){e.unused=Object.getOwnPropertyNames(f);b.push.apply(h(f));return b}})}};$jscomp.symbolCounter_=0;$jscomp.Symbol=function(b){return $jscomp.SYMBOL_PREFIX+b+$jscomp.symbolCounter_++};$jscomp.initSymbolIterator=function(){$jscomp.initSymbol();var b=$jscomp.global.Symbol.iterator;b||(b=$jscomp.global.Symbol.iterator=$jscomp.global.Symbol("iterator"));"function"!=typeof Array.prototype[b]&&$jscomp.defineProperty(Array.prototype,b,{configurable:!0,writable:!0,value:function(){return $jscomp.arrayIterator(this)}});$jscomp.initSymbolIterator=function(){}};$jscomp.arrayIterator=function(b){var e=0;return $jscomp.iteratorPrototype(function(){return e<b.length?{done:!1,value:b[e++]}:{done:!0}})};$jscomp.iteratorPrototype=function(b){$jscomp.initSymbolIterator();b={next:b};b[$jscomp.global.Symbol.iterator]=function(){return this};return b};(function(b){function e(a){"string"!==typeof a&&(a=String(a));if(/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(a))throw new TypeError("Invalid character in header field name");return a.toLowerCase()}function h(a){"string"!==typeof a&&(a=String(a));return a}function f(a){var d={next:function(){var d=a.shift();return{done:void 0===d,value:d}}};k.iterable&&($jscomp.initSymbol(),$jscomp.initSymbolIterator(),d[Symbol.iterator]=function(){return d});return d}function c(a){this.map={};a instanceof c?a.forEach(function(a,b){this.append(b,a)},this):a&&Object.getOwnPropertyNames(a).forEach(function(d){this.append(d,a[d])},this)}function l(a){if(a.bodyUsed)return Promise.reject(new TypeError("Already read"));a.bodyUsed=!0}function r(a){return new Promise(function(d,b){a.onload=function(){d(a.result)};a.onerror=function(){b(a.error)}})}function m(a){var d=new FileReader;d.readAsArrayBuffer(a);return r(d)}function p(){this.bodyUsed=!1;this._initBody=function(a){this._bodyInit=a;if("string"===typeof a)this._bodyText=a;else if(k.blob&&Blob.prototype.isPrototypeOf(a))this._bodyBlob=a;else if(k.formData&&FormData.prototype.isPrototypeOf(a))this._bodyFormData=a;else if(k.searchParams&&URLSearchParams.prototype.isPrototypeOf(a))this._bodyText=a.toString();else if(!a)this._bodyText="";else if(!k.arrayBuffer||!ArrayBuffer.prototype.isPrototypeOf(a))throw Error("unsupported BodyInit type");this.headers.get("content-type")||("string"===typeof a?this.headers.set("content-type","text/plain;charset=UTF-8"):this._bodyBlob&&this._bodyBlob.type?this.headers.set("content-type",this._bodyBlob.type):k.searchParams&&URLSearchParams.prototype.isPrototypeOf(a)&&this.headers.set("content-type","application/x-www-form-urlencoded;charset=UTF-8"))};k.blob?(this.blob=function(){var a=l(this);if(a)return a;if(this._bodyBlob)return Promise.resolve(this._bodyBlob);if(this._bodyFormData)throw Error("could not read FormData body as blob");return Promise.resolve(new Blob([this._bodyText]))},this.arrayBuffer=function(){return this.blob().then(m)},this.text=function(){var a=l(this);if(a)return a;if(this._bodyBlob){var a=this._bodyBlob,d=new FileReader;d.readAsText(a);return r(d)}if(this._bodyFormData)throw Error("could not read FormData body as text");return Promise.resolve(this._bodyText)}):this.text=function(){var a=l(this);return a?a:Promise.resolve(this._bodyText)};k.formData&&(this.formData=function(){return this.text().then(u)});this.json=function(){return this.text().then(JSON.parse)};return this}function q(a,d){d=d||{};var b=d.body;if(q.prototype.isPrototypeOf(a)){if(a.bodyUsed)throw new TypeError("Already read");this.url=a.url;this.credentials=a.credentials;d.headers||(this.headers=new c(a.headers));this.method=a.method;this.mode=a.mode;b||(b=a._bodyInit,a.bodyUsed=!0)}else this.url=a;this.credentials=d.credentials||this.credentials||"omit";if(d.headers||!this.headers)this.headers=new c(d.headers);var f=d.method||this.method||"GET",e=f.toUpperCase();this.method=-1<v.indexOf(e)?e:f;this.mode=d.mode||this.mode||null;this.referrer=null;if(("GET"===this.method||"HEAD"===this.method)&&b)throw new TypeError("Body not allowed for GET or HEAD requests");this._initBody(b)}function u(a){var d=new FormData;a.trim().split("&").forEach(function(a){if(a){var b=a.split("=");a=b.shift().replace(/\+/g," ");b=b.join("=").replace(/\+/g," ");d.append(decodeURIComponent(a),decodeURIComponent(b))}});return d}function w(a){var d=new c;(a.getAllResponseHeaders()||"").trim().split("\n").forEach(function(a){var b=a.trim().split(":");a=b.shift().trim();b=b.join(":").trim();d.append(a,b)});return d}function n(a,b){b||(b={});this.type="default";this.status=b.status;this.ok=200<=this.status&&300>this.status;this.statusText=b.statusText;this.headers=b.headers instanceof c?b.headers:new c(b.headers);this.url=b.url||"";this._initBody(a)}if(!b.fetch){$jscomp.initSymbol();var x="Symbol"in b&&"iterator"in Symbol,t;if(t="FileReader"in b&&"Blob"in b)try{new Blob,t=!0}catch(a){t=!1}var k={searchParams:"URLSearchParams"in b,iterable:x,blob:t,formData:"FormData"in b,arrayBuffer:"ArrayBuffer"in b};c.prototype.append=function(a,b){a=e(a);b=h(b);var c=this.map[a];c||(c=[],this.map[a]=c);c.push(b)};c.prototype["delete"]=function(a){delete this.map[e(a)]};c.prototype.get=function(a){return(a=this.map[e(a)])?a[0]:null};c.prototype.getAll=function(a){return this.map[e(a)]||[]};c.prototype.has=function(a){return this.map.hasOwnProperty(e(a))};c.prototype.set=function(a,b){this.map[e(a)]=[h(b)]};c.prototype.forEach=function(a,b){Object.getOwnPropertyNames(this.map).forEach(function(c){this.map[c].forEach(function(f){a.call(b,f,c,this)},this)},this)};c.prototype.keys=function(){var a=[];this.forEach(function(b,c){a.push(c)});return f(a)};c.prototype.values=function(){var a=[];this.forEach(function(b){a.push(b)});return f(a)};c.prototype.entries=function(){var a=[];this.forEach(function(b,c){a.push([c,b])});return f(a)};k.iterable&&($jscomp.initSymbol(),$jscomp.initSymbolIterator(),c.prototype[Symbol.iterator]=c.prototype.entries);var v="DELETE GET HEAD OPTIONS POST PUT".split(" ");q.prototype.clone=function(){return new q(this)};p.call(q.prototype);p.call(n.prototype);n.prototype.clone=function(){return new n(this._bodyInit,{status:this.status,statusText:this.statusText,headers:new c(this.headers),url:this.url})};n.error=function(){var a=new n(null,{status:0,statusText:""});a.type="error";return a};var y=[301,302,303,307,308];n.redirect=function(a,b){if(-1===y.indexOf(b))throw new RangeError("Invalid status code");return new n(null,{status:b,headers:{location:a}})};b.Headers=c;b.Request=q;b.Response=n;b.fetch=function(a,b){return new Promise(function(c,f){var e;e=q.prototype.isPrototypeOf(a)&&!b?a:new q(a,b);var g=new XMLHttpRequest;g.onload=function(){var a=g.status,b=g.statusText,d=w(g),e;e="responseURL"in g?g.responseURL:/^X-Request-URL:/m.test(g.getAllResponseHeaders())?g.getResponseHeader("X-Request-URL"):void 0;c(new n("response"in g?g.response:g.responseText,{status:a,statusText:b,headers:d,url:e}))};g.onerror=function(){f(new TypeError("Network request failed"))};g.ontimeout=function(){f(new TypeError("Network request failed"))};g.open(e.method,e.url,!0);"include"===e.credentials&&(g.withCredentials=!0);"responseType"in g&&k.blob&&(g.responseType="blob");e.headers.forEach(function(a,b){g.setRequestHeader(b,a)});g.send("undefined"===typeof e._bodyInit?null:e._bodyInit)})};b.fetch.polyfill=!0}})("undefined"!==typeof self?self:this);
/*!
 * load CSS async
 * modified order of arguments, added callback option, removed CommonJS stuff
 * github.com/filamentgroup/loadCSS
 * gist.github.com/englishextra/50592e9944bd2edc46fe5a82adec3396
 */
var loadCSS=function(href,callback,media,before){"use strict";var doc=document;var ss=doc.createElement("link");var ref;if(before){ref=before;}else{var refs=(doc.body||doc.getElementsByTagName("head")[0]).childNodes;ref=refs[refs.length-1];}var sheets=doc.styleSheets;ss.rel="stylesheet";ss.href=href;ss.media="only x";if(callback&&"function"===typeof callback){ss.onload=callback;}function ready(cb){if(doc.body){return cb();}setTimeout(function(){ready(cb);});}ready(function(){ref.parentNode.insertBefore(ss,(before?ref:ref.nextSibling));});var onloadcssdefined=function(cb){var resolvedHref=ss.href;var i=sheets.length;while(i--){if(sheets[i].href===resolvedHref){return cb();}}setTimeout(function(){onloadcssdefined(cb);});};function loadCB(){if(ss.addEventListener){ss.removeEventListener("load",loadCB);}ss.media=media||"all";}if(ss.addEventListener){ss.addEventListener("load",loadCB);}ss.onloadcssdefined=onloadcssdefined;onloadcssdefined(loadCB);return ss;};
/*!
 * load JS async
 * modified order of arguments, removed CommonJS stuff
 * github.com/filamentgroup/loadJS
 * gist.github.com/englishextra/397e62184fde65d7755744fdb7a01829
 */
var loadJS=function(src,callback){"use strict";var ref=document.getElementsByTagName("script")[0];var script=document.createElement("script");script.src=src;script.async=true;ref.parentNode.insertBefore(script,ref);if(callback&&"function"===typeof callback){script.onload=callback;}return script;};
/*!
 * How can I check if a CSS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var styleIsLoaded=function(h){var a=document.styleSheets||"";if(a)for(var b=0,d=a.length;b<d;b++)if(a[b].href==h)return!0;return!1};
/*!
 * How can I check if a JS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var scriptIsLoaded=function(s){for(var b=document.getElementsByTagName("script")||"",a=0;a<b.length;a++)if(b[a].getAttribute("src")==s)return!0;return!1};
/*!
 * Load and execute JS via AJAX
 * gist.github.com/englishextra/8dc9fe7b6ff8bdf5f9b483bf772b9e1c
 * IE 5.5+, Firefox, Opera, Chrome, Safari XHR object
 * gist.github.com/Xeoncross/7663273
 * modified callback(x.responseText,x); to callback(eval(x.responseText),x);
 * stackoverflow.com/questions/3728798/running-javascript-downloaded-with-xmlhttprequest
 */
var AJAXLoadAndTriggerJs=function(u,cb,d){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.open(d?"POST":"GET",u,!0);x.setRequestHeader("X-Requested-With","XMLHttpRequest");x.setRequestHeader("Content-type","application/x-www-form-urlencoded");x.onreadystatechange=function(){if(x.readyState>3){if(x.responseText){eval(x.responseText);cb&&"function"===typeof cb&&cb(x.responseText);}}};x.send(d);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * Load .json file, but don't JSON.parse it
 * modified JSON with JS.md
 * gist.github.com/thiagodebastos/08ea551b97892d585f17
 * gist.github.com/englishextra/e2752e27761649479f044fd93a602312
 */
var AJAXloadUnparsedJSON=function(u,cb){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.overrideMimeType("application/json;charset=utf-8");x.open("GET",u,!0);x.onreadystatechange=function(){if(x.readyState==4&&x.status=="200"){cb(x.responseText);}};x.send(null);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * parse JSON with no eval using JSON-js/json_parse.js
 * with fallback to native JSON.parse
 */
var safelyParseJSON=function(d){var w=window;try{return w.json_parse?json_parse(d):JSON.parse(d);}catch(e){console.log(e);}};
/*!
 * return an array of values that match on a certain key
 * techslides.com/how-to-parse-and-search-json-in-javascript
 * gist.github.com/englishextra/872269c30d7cb2d10e3c3babdefc37b4
 * var jpr = JSON.parse(response);
 * for (var i = 0, l = jpr.length; i < l; i += 1) {
 * 	var o = jpr[i],
 * 	t = getJsonKeyValues(o, "label"),
 * 	p = getJsonKeyValues(o, "link");
 * 	crel(select, crel("option", {
 * 		"value" : p,
 * 		"title" : "" + t
 * 	}, truncString("" + t, 33)));
 * }
 */
var getJsonKeyValues=function(b,d){var c=[],a;for(a in b)b.hasOwnProperty(a)&&("object"==typeof b[a]?c=c.concat(getJsonKeyValues(b[a],d)):a==d&&c.push(b[a]));return c};
/*!
 * loop over the Array
 * stackoverflow.com/questions/18238173/javascript-loop-through-json-array
 * gist.github.com/englishextra/b4939b3430da4b55d731201460d3decb
 */
var truncString=function(str,max,add){add=add||"\u2026";return(typeof str==="string"&&str.length>max?str.substring(0,max)+add:str);};
/*!
 * remove all children of parent element
 */
var removeChildElements=function(a){if(a)for(;a.firstChild;)a.removeChild(a.firstChild);};
/*!
 * set style display block of an element
 */
var setStyleDisplayBlock=function(a){a&&(a.style.display="block");};
/*!
 * set style display none of an element
 */
var setStyleDisplayNone=function(a){a&&(a.style.display="none");};
/*!
 * toggle style display of an element
 */
var toggleStyleDisplay=function(a,show,hide){if(a){a.style.display=hide==a.style.display||""==a.style.display?show:hide;}};
/*!
 * set style opacity of an element
 */
var setStyleOpacity=function(a,n){a&&(a.style.opacity=n);};
/*!
 * set style visibility visible of an element
 */
var setStyleVisibilityVisible=function(a){a&&(a.style.visibility="visible");};
/*!
 * set style visibility hidden of an element
 */
var setStyleVisibilityHidden=function(a){a&&(a.style.visibility="hidden");};
/*!
 * Adds Element BEFORE NeighborElement
 * gist.github.com/englishextra/fa19e39ce84982b17fc76485db9d1bea
 * .insertBeforeNeighborElement(element) Prototype
 * stackoverflow.com/questions/4793604/how-to-do-insert-after-in-javascript-without-using-a-library
 * NewElement.insertBeforeNeighborElement(document.getElementById("NeighborElement"));
 */
Node.prototype.insertBeforeNeighborElement=function(a){a.parentNode.insertBefore(this,a)};!1;
/*!
 * Adds Element AFTER NeighborElement
 * gist.github.com/englishextra/c19556b7a61865e3631cc879aaeb314e
 * .appendAfterNeighborElement(element) Prototype
 * stackoverflow.com/questions/4793604/how-to-do-insert-after-in-javascript-without-using-a-library
 * NewElement.appendAfterNeighborElement(document.getElementById("NeighborElement"));
 */
Node.prototype.appendAfterNeighborElement=function(a){a.parentNode.insertBefore(this,a.nextSibling)};!1;
/*!
 * return element height
 * stackoverflow.com/questions/15615552/get-div-height-with-plain-javascript
 */
var getHeight=function(a){return a.clientHeight||a.offsetHeight||0};
/*!
 * return scroll Y position
 * developer.mozilla.org/en-US/docs/Web/API/Window/scrollY
 */
var getScrollYPos=function(){var a="CSS1Compat"===(document.compatMode||"");return(void 0!==window.pageXOffset?window.pageYOffset:a?document.documentElement.scrollTop:document.body.scrollTop)||0;};
/*!
 * change location
 */
var changeDocumentLocation=function(h){h&&(document.location.href=h);};
/*!
 * has http
 */
var hasHTTP=function(url){return/^(http|https):\/\//i.test(url)?!0:!1};
/*!
 * get http or https
 */
var getHTTPProtocol=function(){var a=window.location.protocol||"";return"http:"===a?"http":"https:"===a?"https":""};
/*!
 * show data loading spinner
 */
var showDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	h && h.classList.add(data_loading);
};
/*!
 * hide data loading spinner
 */
var hideDataLoadingSpinner = function () {
	var h = BALA.one("html") || "",
	data_loading = "data-loading";
	if (h) {
		setAndClearTimeout(function () {
			h.classList.remove(data_loading);
		}, 500);
	}
};
/*!
 * Open external links in default browser out of Electron / nwjs
 * gist.github.com/englishextra/b9a8140e1c1b8aa01772375aeacbf49b
 * stackoverflow.com/questions/32402327/how-can-i-force-external-links-from-browser-window-to-open-in-a-default-browser
 * github.com/nwjs/nw.js/wiki/shell
 * electron - file: | nwjs - chrome-extension: | http: Intel XDK
 */
var openDeviceBrowser = function (a) {
	var w = window,
	g = function () {
		var electronShell = require("electron").shell;
		electronShell.openExternal(a);
	},
	k = function () {
		var nwGui = require("nw.gui");
		nwGui.Shell.openExternal(a);
	},
	q = function () {
		var win = w.open(a, "_blank");
		win.focus();
	},
	v = function () {
		w.open(a, "_system", "scrollbars=1,location=no");
	};
	if (!!isElectron) {
		g();
	} else if (!!isNwjs) {
		k();
	} else {
		if (!!getHTTPProtocol()) {
			q();
		} else {
			v();
		}
	}
};
/*!
 * set target blank to external links
 */
var setTargetBlankOnAnchors = function () {
	var w = window,
	a = BALA("a") || "",
	g = function (e) {
		var p = e.getAttribute("href") || "";
		if (p
			&& (/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)|(localhost)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(p) || /http:\/\/localhost/.test(p))
			&& !e.getAttribute("rel")) {
			crel(e, {
				"title" : "\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 " + (p.match(/:\/\/(.[^/]+)/)[1] || "") + " \u043e\u0442\u043a\u0440\u043e\u0435\u0442\u0441\u044f \u0432 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043b\u0438 \u043e\u043a\u043d\u0435"
			});
			if (w.openDeviceBrowser) {
				crel(e, {
					"onclick" : "openDeviceBrowser('" + jsStringEscape(p) + "');return !1;"
				});
			} else {
				crel(e, {
					"target" : "_blank"
				});
			}
		}
	},
	k = function () {
			if (w._) {
				_.each(a, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(a, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = a.length; i < l; i += 1) {
					g(a[i]);
				};
			}
	};
	if (a) {
		k();
	}
};
docReady(function () {
	setTargetBlankOnAnchors();
});
/*!
 * init fastclick
 * github.com/ftlabs/fastclick
 */
var initFastclick = function () {
	var w = window,
	b = BALA.one("body") || "",
	fastclick_js_src = "/cdn/fastclick/1.0.6/js/fastclick.fixed.min.js",
	g = function () {
		AJAXLoadAndTriggerJs(fastclick_js_src, function () {
			w.FastClick && FastClick.attach(b);
		});
	};
	if (!!getHTTPProtocol()) {
		g();
	}
};
"undefined" !== typeof earlyHasTouch && "touch" === earlyHasTouch && evento.add(window, "load", function () {
	initFastclick();
});
/*!
 * hide ui buttons in fullscreen mode
 */
var hideUiButtonsInFullscreen = function () {
	var w = window,
	cd_prev = BALA.one(".cd-prev") || "",
	cd_next = BALA.one(".cd-next") || "",
	btn_nav_menu = BALA.one(".btn-nav-menu") || "",
	btn_menu_more = BALA.one(".btn-menu-more") || "",
	btn_show_vk_like = BALA.one(".btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?105",
	vk_like = BALA.one(".vk-like") || "",
	btn_block_social_buttons = BALA.one(".btn-block-social-buttons") || "",
	ui_totop = BALA.one("#ui-totop") || "",
	holder_search_form = BALA.one(".holder-search-form") || "",
	f = !1;
	if (!1 === f) {
		/*!
		 * Detecting if a browser is in full screen mode
		 * stackoverflow.com/questions/2863351/checking-if-browser-is-in-fullscreen
		 * stackoverflow.com/questions/1047319/detecting-if-a-browser-is-in-full-screen-mode
		 */
		var args = [cd_prev, cd_next, btn_nav_menu, btn_menu_more, btn_block_social_buttons, ui_totop, holder_search_form];
		if ((w.navigator.standalone) || (screen.height === w.outerHeight) || (w.fullScreen) || (w.innerWidth == screen.width && w.innerHeight == screen.height)) {
			for (var i = 0; i < args.length; i++) {
				setStyleDisplayNone(args[i]);
			}
			setStyleDisplayNone(btn_show_vk_like);
			f = !0;
		} else {
			for (var i = 0; i < args.length; i++) {
				setStyleDisplayBlock(args[i]);
			}
			scriptIsLoaded(openapi_js_src) || setStyleDisplayBlock(btn_show_vk_like);
			f = !0;
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "medium" === earlyDeviceSize) || evento.add(window, "resize", function () {
	hideUiButtonsInFullscreen();
});
/*!
 * init all Masonry grids
 */
var initAllMasonryGrids = function () {
	var w = window,
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	masonry_js_src = "../../cdn/masonry/4.0.0/js/masonry.pkgd.fixed.min.js",
	q = function (a) {
		var s = function (e) {
			if (w.Masonry) {
				var msnry = new Masonry(e, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
			}
		};
		if (w._) {
			_.each(a, function (e) {
				s(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				s(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				s(a[i]);
			};
		}
	},
	v = function (a) {
		scriptIsLoaded(masonry_js_src) || loadJS(masonry_js_src, function () {
			q(a);
		});
	};
	if (grid && grid_item) {
		var a = BALA(g);
		if (a) {
			v(a);
		}
	}
};
evento.add(window, "load", function () {
	initAllMasonryGrids();
});
/*!
 * init all Packery grids
 */
var initAllPackeryGrids = function () {
	var w = window,
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	packery_js_src = "../../cdn/packery/2.0.0/js/packery.pkgd.fixed.min.js",
	draggabilly_js_src = "../../cdn/draggabilly/2.1.0/js/draggabilly.pkgd.fixed.min.js",
	q = function (a, c) {
		if (w.Packery) {
			var pckry;
			var s = function (e) {
				pckry = new Packery(e, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
			};
			if (w._) {
				_.each(a, function (e) {
					s(e);
				});
			} else if (w.forEach) {
				forEach(a, function (e) {
					s(e);
				}, !1);
			} else {
				for (var i = 0, l = a.length; i < l; i += 1) {
					s(a[i]);
				};
			}
			if (w.Draggabilly) {
				var draggie;
				var s = function (e) {
					var draggableElem = e;
					draggie = new Draggabilly(draggableElem, {});
					draggies.push(draggie);
				},
				draggies = [];
				if (w._) {
					_.each(c, function (e) {
						s(e);
					});
				} else if (w.forEach) {
					forEach(c, function (e) {
						s(e);
					}, !1);
				} else {
					for (var i = 0, l = c.length; i < l; i += 1) {
						s(c[i]);
					};
				}
				pckry && pckry.bindDraggabillyEvents(draggie);
			}
		}
	},
	v = function (a, c) {
		scriptIsLoaded(packery_js_src) || loadJS(packery_js_src, function () {
			scriptIsLoaded(draggabilly_js_src) || loadJS(draggabilly_js_src, function () {
				q(a, c);
			});
		});
	};
	if (grid && grid_item) {
		var a = BALA(g),
		c = BALA(h) || "";
		if (a, c) {
			v(a, c);
		}
	}
};
evento.add(window, "load", function () {
	initAllPackeryGrids();
});
/*!
 * init photoswipe
 */
var initPhotoswipe = function () {
	var w = window,
	c = ".article-gallery",
	gallery = BALA.one(c) || "",
	photoswipe_css_href = "../../cdn/photoswipe/4.1.0/custom/css/photoswipe.bundle.min.css",
	photoswipe_js_src = "../../cdn/photoswipe/4.1.0/custom/js/photoswipe.bundle.min.js",
	pswp = function(k){var p=function(l){for(var a=l.childNodes,b=a.length,e=[],c,g,d,f=0;f<b;f++)if(l=a[f],1===l.nodeType){c=l.children;g=l.getAttribute("data-size").split("x");d={src:l.getAttribute("href"),w:parseInt(g[0],10),h:parseInt(g[1],10),author:l.getAttribute("data-author")};d.el=l;0<c.length&&(d.msrc=c[0].getAttribute("src"),1<c.length&&(d.title=c[1].innerHTML));if(c=l.getAttribute("data-med"))g=l.getAttribute("data-med-size").split("x"),d.m={src:c,w:parseInt(g[0],10),h:parseInt(g[1], 10)};d.o={src:d.src,w:d.w,h:d.h};e.push(d)}return e},q=function a(b,e){return b&&(e(b)?b:a(b.parentNode,e))},h=function(a){a=a||window.event;a.preventDefault?a.preventDefault():a.returnValue=!1;if(a=q(a.target||a.srcElement,function(a){return"A"===a.tagName})){for(var b=a.parentNode,e=a.parentNode.childNodes,c=e.length,g=0,d,f=0;f<c;f++)if(1===e[f].nodeType){if(e[f]===a){d=g;break}g++}0<=d&&n(d,b);return!1}},n=function(a,b,e){e=document.querySelectorAll(".pswp")[0];var c,g;g=p(b);a={index:a,galleryUID:b.getAttribute("data-pswp-uid"), getThumbBoundsFn:function(a){var b=window.pageYOffset||document.documentElement.scrollTop;a=g[a].el.children[0].getBoundingClientRect();return{x:a.left,y:a.top+b,w:a.width}},addCaptionHTMLFn:function(a,b,c){if(!a.title)return b.children[0].innerText="",!1;b.children[0].innerHTML=a.title+"<br/><small>Photo: "+a.author+"</small>";return!0}};c=new PhotoSwipe(e,PhotoSwipeUI_Default,g,a);var d,f=!1,k=!0,h;c.listen("beforeResize",function(){var a=window.devicePixelRatio?window.devicePixelRatio:1,a=Math.min(a, 2.5);d=c.viewportSize.x*a;1200<=d||!c.likelyTouchDevice&&800<d||1200<screen.width?f||(h=f=!0):f&&(f=!1,h=!0);h&&!k&&c.invalidateCurrItems();k&&(k=!1);h=!1});c.listen("gettingData",function(a,b){f?(b.src=b.o.src,b.w=b.o.w,b.h=b.o.h):(b.src=b.m.src,b.w=b.m.w,b.h=b.m.h)});c.init()};k=document.querySelectorAll(k);for(var m=0,r=k.length;m<r;m++)k[m].setAttribute("data-pswp-uid",m+1),k[m].onclick=h;h=function(){var a=window.location.hash.substring(1),b={};if(5>a.length)return b;for(var a=a.split("&"),e= 0;e<a.length;e++)if(a[e]){var c=a[e].split("=");2>c.length||(b[c[0]]=c[1])}b.gid&&(b.gid=parseInt(b.gid,10));if(!b.hasOwnProperty("pid"))return b;b.pid=parseInt(b.pid,10);return b}();0<h.pid&&0<h.gid&&n(h.pid-1,k[h.gid-1],!0)};
	fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http",
	g = function (e) {
		var m = e.dataset.med || "";
		if (m && !hasHTTP(m) && /^\/\//.test(m)) {
			e.dataset.med = m.replace (/^/, fixed_protocol + ":");
		}
		/*!
		 * dont use href to read href, use getAttribute, because href adds protocol
		 */
		var h = e.getAttribute("href");
		if (h && !hasHTTP(h) && /^\/\//.test(h)) {
			e.href = h.replace (/^/, fixed_protocol + ":");
		}
	},
	k = function (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	},
	q = function (e) {
		setStyleDisplayBlock(e);
		var a = BALA("a", e) || "";
		if (a) {
			k(a);
		}
	},
	v = function () {
		var galleries = BALA(c);
		if (w._) {
			_.each(galleries, function (e) {
				q(e);
			});
		} else if (w.forEach) {
			forEach(galleries, function (e) {
				q(e);
			}, !1);
		} else {
			for (var i = 0, l = galleries.length; i < l; i += 1) {
				q(galleries[i]);
			};
		}
	},
	z = function () {
		styleIsLoaded(photoswipe_css_href) || loadCSS(photoswipe_css_href, function () {
			scriptIsLoaded(photoswipe_js_src) || loadJS(photoswipe_js_src, function () {
				v();
				pswp(c);
			});
		});
	};
	if (gallery) {
		z();
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initPhotoswipe();
});
/*!
 * init photoswipe
 */
var initPhotoswipe = function () {
	var w = window,
	c = ".grid",
	gallery = BALA.one(c) || "",
	item = ".grid-item",
	gallery_item = BALA.one(item) || "",
	photoswipe_css_href = "../cdn/photoswipe/4.1.0/custom/css/photoswipe.bundle.min.css",
	photoswipe_js_src = "../cdn/photoswipe/4.1.0/custom/js/photoswipe.bundle.min.js",
	pswp = function(k){var p=function(l){for(var a=l.childNodes,b=a.length,e=[],c,g,d,f=0;f<b;f++)if(l=a[f],1===l.nodeType){c=l.children;g=l.getAttribute("data-size").split("x");d={src:l.getAttribute("href"),w:parseInt(g[0],10),h:parseInt(g[1],10),author:l.getAttribute("data-author")};d.el=l;0<c.length&&(d.msrc=c[0].getAttribute("src"),1<c.length&&(d.title=c[1].innerHTML));if(c=l.getAttribute("data-med"))g=l.getAttribute("data-med-size").split("x"),d.m={src:c,w:parseInt(g[0],10),h:parseInt(g[1], 10)};d.o={src:d.src,w:d.w,h:d.h};e.push(d)}return e},q=function a(b,e){return b&&(e(b)?b:a(b.parentNode,e))},h=function(a){a=a||window.event;a.preventDefault?a.preventDefault():a.returnValue=!1;if(a=q(a.target||a.srcElement,function(a){return"A"===a.tagName})){for(var b=a.parentNode,e=a.parentNode.childNodes,c=e.length,g=0,d,f=0;f<c;f++)if(1===e[f].nodeType){if(e[f]===a){d=g;break}g++}0<=d&&n(d,b);return!1}},n=function(a,b,e){e=document.querySelectorAll(".pswp")[0];var c,g;g=p(b);a={index:a,galleryUID:b.getAttribute("data-pswp-uid"), getThumbBoundsFn:function(a){var b=window.pageYOffset||document.documentElement.scrollTop;a=g[a].el.children[0].getBoundingClientRect();return{x:a.left,y:a.top+b,w:a.width}},addCaptionHTMLFn:function(a,b,c){if(!a.title)return b.children[0].innerText="",!1;b.children[0].innerHTML=a.title+"<br/><small>Photo: "+a.author+"</small>";return!0}};c=new PhotoSwipe(e,PhotoSwipeUI_Default,g,a);var d,f=!1,k=!0,h;c.listen("beforeResize",function(){var a=window.devicePixelRatio?window.devicePixelRatio:1,a=Math.min(a, 2.5);d=c.viewportSize.x*a;1200<=d||!c.likelyTouchDevice&&800<d||1200<screen.width?f||(h=f=!0):f&&(f=!1,h=!0);h&&!k&&c.invalidateCurrItems();k&&(k=!1);h=!1});c.listen("gettingData",function(a,b){f?(b.src=b.o.src,b.w=b.o.w,b.h=b.o.h):(b.src=b.m.src,b.w=b.m.w,b.h=b.m.h)});c.init()};k=document.querySelectorAll(k);for(var m=0,r=k.length;m<r;m++)k[m].setAttribute("data-pswp-uid",m+1),k[m].onclick=h;h=function(){var a=window.location.hash.substring(1),b={};if(5>a.length)return b;for(var a=a.split("&"),e= 0;e<a.length;e++)if(a[e]){var c=a[e].split("=");2>c.length||(b[c[0]]=c[1])}b.gid&&(b.gid=parseInt(b.gid,10));if(!b.hasOwnProperty("pid"))return b;b.pid=parseInt(b.pid,10);return b}();0<h.pid&&0<h.gid&&n(h.pid-1,k[h.gid-1],!0)},
	fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http",
	g = function (e) {
		var m = e.dataset.med || "";
		if (m && !hasHTTP(m) && /^\/\//.test(m)) {
			e.dataset.med = m.replace (/^/, fixed_protocol + ":");
		}
		/*!
		 * dont use href to read href, use getAttribute, because href adds protocol
		 */
		var h = e.getAttribute("href");
		if (h && !hasHTTP(h) && /^\/\//.test(h)) {
			e.href = h.replace (/^/, fixed_protocol + ":");
		}
	},
	k = function (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	},
	q = function (e) {
		setStyleDisplayBlock(e);
		var a = BALA("a", e) || "";
		if (a) {
			k(a);
		}
	},
	v = function () {
		var galleries = BALA(c);
		if (w._) {
			_.each(galleries, function (e) {
				q(e);
			});
		} else if (w.forEach) {
			forEach(galleries, function (e) {
				q(e);
			}, !1);
		} else {
			for (var i = 0, l = galleries.length; i < l; i += 1) {
				q(galleries[i]);
			};
		}
	},
	z = function () {
		styleIsLoaded(photoswipe_css_href) || loadCSS(photoswipe_css_href, function () {
			scriptIsLoaded(photoswipe_js_src) || loadJS(photoswipe_js_src, function () {
				v();
				pswp(c);
			});
		});
	};
	if (gallery) {
		z();
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initPhotoswipe();
});
/*!
 * init tablesort
 * github.com/tristen/tablesort
 */
var initTablesort = function () {
	var w = window,
	c = "table.sort",
	table = BALA.one(c) || "",
	tablesort_css_href = "../../cdn/tablesort/4.0.1/custom/css/tablesort.min.css",
	tablesort_js_src = "../../cdn/tablesort/4.0.1/js/tablesort.fixed.min.js",
	g = function (e) {
		var t_id = e.id || "";
		if (t_id) {
			var t = BALA.one("#" + t_id),
			t_caption = BALA.one("#" + t_id + " caption") || t.insertBefore(crel("caption"), t.firstChild),
			tablesort = new Tablesort(t);
			if (t_caption) {
				crel(t_caption, "\u0421\u043E\u0440\u0442\u0438\u0440\u0443\u0435\u043C\u0430\u044F \u0442\u0430\u0431\u043B\u0438\u0446\u0430");
			}
		}
	},
	k = function () {
		var a = BALA(c);
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	};
	if (table) {
		scriptIsLoaded(tablesort_js_src) || loadJS(tablesort_js_src, function () {
			k();
		});
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initTablesort();
});
/*!
 * init prettyPrint
 */
var initPrettyPrint = function () {
	var w = window,
	a = BALA.one('[class^="prettyprint"]') || "",
	prettify_css_href = "../../cdn/google-code-prettify/0.1/custom/css/prettify.min.css",
	prettify_js_src = "../../cdn/google-code-prettify/0.1/custom/js/prettify.min.js",
	lang_css_js_src = "../../cdn/google-code-prettify/0.1/js/lang-css.js";
	if (a) {
		styleIsLoaded(prettify_css_href) || loadCSS(prettify_css_href, function () {
			scriptIsLoaded(prettify_js_src) || loadJS(prettify_js_src, function () {
				scriptIsLoaded(lang_css_js_src) || loadJS(lang_css_js_src, function () {
					"undefined" !== typeof w.prettyPrint && prettyPrint();
				});
			});
		});
	}
};
evento.add(window, "load", function () {
	initPrettyPrint();
});
/*!
 * init hint.css
 */
var initHintCss = function () {
	var hint = BALA.one('[class*=" hint-"]') || BALA.one('[class^="hint-"]') || "",
	hint_css_href = "../../cdn/hint.css/2.2.0/custom/css/hint.min.css";
	if (hint) {
		styleIsLoaded(hint_css_href) || loadCSS(hint_css_href);
	}
};
evento.add(window, "load", function () {
	initHintCss();
});
/*!
 * init gh-fork-ribbon
 */
var initGhForkRibbon = function () {
	var ribbon = BALA.one('[class^="github-fork-ribbon-wrapper"]') || "",
	ribbon_css_href = "/cdn/github-fork-ribbon-css/0.1.1/css/gh-fork-ribbon.min.css";
	if (ribbon) {
		if (!!getHTTPProtocol()) {
			styleIsLoaded(ribbon_css_href) || loadCSS(ribbon_css_href);
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initGhForkRibbon();
});
/*!
 * replace img src with data-src
 */
var replaceImgSrcWithDataSrc = function () {
	var w = window,
	a = BALA("img") || "",
	g = function (e) {
		var s = e.dataset.src || "",
		f = e.classList.contains("data-src-img"),
		fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http";
		if (s && f) {
			if (!hasHTTP(s) && /^\/\//.test(s)) {
				e.dataset.src = s.replace(/^/, fixed_protocol + ":");
			}
			w.lzld ? lzld(e) : e.src = e.dataset.src;
			setStyleVisibilityVisible(e.parentNode);
			setStyleOpacity(e.parentNode, 1);
		}
	};
	if (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	}
};
evento.add(window, "load", function () {
	replaceImgSrcWithDataSrc();
});
/*!
 * append media-iframe
 */
var appendMediaIframe = function () {
	var w = window,
	a = BALA("iframe") || "",
	g = function (e) {
		var s = e.dataset.src || "",
		f = e.classList.contains("data-src-iframe"),
		fixed_protocol = getHTTPProtocol() ? getHTTPProtocol() : "http";
		if (s && f) {
			if (!hasHTTP(s) && /^\/\//.test(s)) {
				e.dataset.src = s.replace(/^/, fixed_protocol + ":");
			}
			crel(e, {
				"scrolling" : "no",
				"frameborder" : "no",
				"style" : "border:none;",
				"webkitallowfullscreen" : "true",
				"mozallowfullscreen" : "true",
				"allowfullscreen" : "true"
			});
			w.lzld ? lzld(e) : e.src = e.dataset.src;
			setStyleDisplayBlock(e.parentNode, 1);
		}
	};
	if (a) {
		if (w._) {
			_.each(a, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				g(a[i]);
			};
		}
	}
};
evento.add(window, "load", function () {
	appendMediaIframe();
});
/*!
 * init text
 */
var initSearchText = function () {
	var a = BALA.one("#text") || "",
	g = function (_this) {
		_this.value = _this.value.replace(/\\/g, "").replace(/ +(?= )/g, " ").replace(/\/+(?=\/)/g, "/") || "";
	},
	k = function (e) {
		e.focus();
		evento.add(e, "input", function () {
			g(this);
		});
	};
	if (a) {
		k(a);
	}
};
docReady(function () {
	initSearchText();
});
/*!
 * show hidden-layer
 */
var showHiddenLayer = function () {
	var w = window,
	h = BALA.one("html") || "",
	c = ".btn-expand-hidden-layer",
	is_active = "is-active",
	btn = BALA.one(c) || "",
	g = function (_this) {
		var s = _this.parentNode.nextElementSibling;
		_this.classList.toggle(is_active),
		s.classList.toggle(is_active);
		return !1;
	},
	k = function (e) {
		evento.add(e, "click", function () {
			g(this);
		});
	},
	q = function () {
		var a = BALA(c);
		if (w._) {
			_.each(a, function (e) {
				k(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				k(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				k(a[i]);
			};
		}
	};
	if (btn) {
		q();
	}
};
docReady(function () {
	showHiddenLayer();
});
/*!
 * show source code
 */
var showSourceCode = function () {
	var w = window,
	h = BALA.one("html") || "",
	c = ".sg-btn--source",
	is_active = "is-active",
	btn = BALA.one(c) || "",
	g = function (_this) {
		var s = _this.parentNode.nextElementSibling;
		_this.classList.toggle(is_active),
		s.classList.toggle(is_active);
		return !1;
	},
	k = function (e) {
		evento.add(e, "click", function () {
			g(this);
		});
	},
	q = function () {
		var a = BALA(c);
		if (w._) {
			_.each(a, function (e) {
				k(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				k(e);
			}, !1);
		} else {
			for (var i = 0, l = a.length; i < l; i += 1) {
				k(a[i]);
			};
		}
	};
	if (btn) {
		q();
	}
};
docReady(function () {
	showSourceCode();
});
/*!
 * init qr-code
 * stackoverflow.com/questions/12777622/how-to-use-enquire-js
 */
var showPageQRRef = function () {
	var w = window,
	d = document,
	a = BALA.one("#qr-code") || "",
	p = w.location.href || "",
	g = function () {
		removeChildElements(a);
		var t = jsStringEscape(d.title ? ("\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u00ab" + d.title.replace(/\[[^\]]*?\]/g, "").trim() + "\u00bb") : ""),
		s = getHTTPProtocol() + "://chart.googleapis.com/chart?cht=qr&chld=M|4&choe=UTF-8&chs=300x300&chl=" + encodeURIComponent(p),
		c = "width:10.000em;height:10.000em;background:transparent;background-size:120.000pt 120.000pt;border:0;vertical-align:bottom;padding:0;margin:0;";
		crel(a,
			crel("img", {
				"src" : s,
				"style" : c,
				"title" : t,
				"alt" : t
			}));
	};
	if (a && p) {
		if (!!getHTTPProtocol()) {
			g();
		} else {
			setStyleDisplayNone(a);
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	showPageQRRef();
});
/*!
 * init nav-menu
 */
var initNavMenu = function () {
	var w = window,
	container = BALA.one("#container") || "",
	page = BALA.one("#page") || "",
	btn = BALA.one("#btn-nav-menu") || "",
	panel = BALA.one("#panel-nav-menu") || "",
	items = BALA("a", panel) || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	active = "active",
	p = w.location.href || "",
	g = function () {
		if (panel.classList.contains(active)) {
			page.classList.remove(active);
			panel.classList.remove(active);
			btn.classList.remove(active);
		}
	},
	k = function () {
		setStyleDisplayNone(holder);
		page.classList.toggle(active);
		panel.classList.toggle(active);
		btn.classList.toggle(active);
	},
	q = function () {
		setStyleDisplayNone(holder);
		page.classList.remove(active);
		panel.classList.remove(active);
		btn.classList.remove(active);
		/* scrollToTop(); */
	},
	m = function (e) {
		e.classList.remove(active);
	},
	n = function (e) {
		e.classList.add(active);
	},
	s = function (a) {
		if (w._) {
			_.each(a, function (e) {
				m(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				m(e);
			}, !1);
		} else {
			for (var j = 0, l = a.length; j < l; j += 1) {
				m(a[j]);
			};
		}
	},
	v = function (a, e) {
		evento.add(e, "click", function () {
			if (panel.classList.contains(active)) {
				q();
			}
			s(a);
			n(e);
		});
		if (e.href == p) {
			n(e);
		} else {
			m(e);
		}
	},
	z = function () {
		if (w._) {
			_.each(items, function (e) {
				v(items, e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				v(items, e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				v(items, items[i]);
			};
		}
	};
	if (container && page && btn && panel && items) {
		/*!
		 * open or close nav
		 */
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		}),
		evento.add(container, "click", function () {
			g();
		});
		/*!
		 * close nav, scroll to top, highlight active nav item
		 */
		z();
	}
};
docReady(function () {
	initNavMenu();
});
/*!
 * init menu-more
 */
var initMenuMore = function () {
	var w = window,
	container = BALA.one("#container") || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	btn = BALA.one("#btn-menu-more") || "",
	panel = BALA.one("#panel-menu-more") || "",
	items = BALA("li", panel) || "",
	g = function (e) {
		evento.add(e, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	k = function () {
		evento.add(container, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			toggleStyleDisplay(holder, "inline-block", "none");
		});
	},
	v = function () {
		if (w._) {
			_.each(items, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				g(items[i]);
			};
		}
	};
	if (container && holder && btn && panel && items) {
		/*!
		 * hide menu more on outside click
		 */
		k();
		/*!
		 * show or hide menu more
		 */
		q();
		/*!
		 * hide menu more on item clicked
		 */
		v();
	}
};
docReady(function () {
	initMenuMore();
});
/*!
 * show menu-more on document ready
 */
var showMenuMoreOnLoad = function () {
	var a = BALA.one("#holder-panel-menu-more") || "",
	g = function () {
		setAndClearTimeout(function () {
			setStyleOpacity(a, 0),
			setStyleDisplayBlock(a),
			FX.fadeIn(a, {
				duration : 500
			});
			setAndClearTimeout(function () {
				FX.fadeOut(a, {
					duration : 500
				});
				setAndClearTimeout(function () {
					setStyleOpacity(a, 1),
					setStyleDisplayNone(a);
				}, 1000);
			}, 4000);
		}, 2000);
	};
	if (a) {
		g();
	}
};
docReady(function () {
	showMenuMoreOnLoad();
});
/*!
 * init ui-totop
 */
var initUiTotop = function () {
	var w = window,
	b = BALA.one("body") || "",
	h = BALA.one("html") || "",
	u = "ui-totop",
	v = "ui-totop-hover",
	g = function (cb) {
		crel(b,
			crel("a", {
				"style" : "opacity:0;",
				"href" : "#",
				"title" : "\u041d\u0430\u0432\u0435\u0440\u0445",
				"id" : u,
				"onclick" : "function scrollTop2(c){var b=window.pageYOffset,d=0,e=setInterval(function(b,a){return function(){a-=b*c;window.scrollTo(0,a);d++;(150<d||0>a)&&clearInterval(e)}}(c,b--),50)};window.zenscroll?zenscroll.toY(0):scrollTop2(50);return !1;"
			},
				crel("span", {
					"id" : v
				}), "\u041d\u0430\u0432\u0435\u0440\u0445"));
		!!cb && "function" === typeof cb && cb();
	},
	k = function (_this) {
		var offset = _this.pageYOffset || h.scrollTop || b.scrollTop || "",
		height = _this.innerHeight || h.clientHeight || b.clientHeight || "",
		btn = BALA.one("#" + u) || "";
		if (offset && height && btn) {
			offset > height ? (setStyleVisibilityVisible(btn), setStyleOpacity(btn, 1)) : (setStyleVisibilityHidden(btn), setStyleOpacity(btn, 0));
		}
	},
	q = function () {
		evento.add(w, "scroll", function () {
			k(this);
		});
	};
	if (b) {
		g(function () {
			q();
		});
	}
};
docReady(function () {
	initUiTotop();
});
/*!
 * init pluso-engine or ya-share on click
 */
var showShareOptionsOnClick = function () {
	var pluso = BALA.one(".pluso") || "",
	ya_share2 = BALA.one(".ya-share2") || "",
	btn = BALA.one("#btn-block-social-buttons") || "",
	pluso_like_js_src = getHTTPProtocol() + "://share.pluso.ru/pluso-like.js",
	share_js_src = getHTTPProtocol() + "://yastatic.net/share2/share.js",
	g = function (share_block, btn) {
		setStyleVisibilityVisible(share_block);
		setStyleOpacity(share_block, 1);
		setStyleDisplayNone(btn);
	},
	k = function (js_src, share_block, btn) {
		scriptIsLoaded(js_src) || loadJS(js_src, function () {
			g(share_block, btn);
		});
	},
	q = function () {
		if (pluso) {
			k(pluso_like_js_src, pluso, btn);
		} else {
			if (ya_share2) {
				k(share_js_src, ya_share2, btn);
			}
		}
	},
	v = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			q();
		});
	};
	if ((pluso || ya_share2) && btn) {
		if (!!getHTTPProtocol()) {
			v();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	showShareOptionsOnClick();
});
/*!
 * init Masonry grid and rerender on imagesLoaded progress
 */
var initMasonryRerenderOnImagesLoaded = function () {
	var w = window,
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer";
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	masonry_js_src = "../cdn/masonry/4.0.0/js/masonry.pkgd.fixed.min.js",
	imagesloaded_js_src = "../cdn/imagesloaded/4.1.0/js/imagesloaded.pkgd.fixed.min.js",
	q = function () {
		var s = function () {
			if (w.Masonry && w.imagesLoaded) {
				var msnry = new Masonry(grid, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0,
						percentPosition : true
					});
				var imgLoad = imagesLoaded(g);
				imgLoad.on("progress", function (instance) {
					msnry.layout();
				});
			}
		};
		scriptIsLoaded(masonry_js_src) || loadJS(masonry_js_src, function () {
			scriptIsLoaded(imagesloaded_js_src) || loadJS(imagesloaded_js_src, function () {
				s();
			});
		});
	};
	if (grid && grid_item) {
		q();
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	initMasonryRerenderOnImagesLoaded();
});
/*!
 * init superbox
 */
var initSuperBox = function () {
	var w = window,
	d = document,
	lists = BALA(".superbox-list") || "",
	s_show_div = crel("div", {
			"class" : "superbox-show"
		}, crel("div", {
				"class" : "superbox-current-desc"
			})),
	s_close_div = crel("div", {
			"class" : "superbox-close"
		}),
	renderLists = function (e) {
		/*!
		 * show description
		 */
		evento.add(e, "click", function (evn) {
			evn.preventDefault();
			evn.stopPropagation();
			/*!
			 * save reference to this, while its still this!
			 */
			var _this = this,
			s_desc = BALA.one(".superbox-desc", _this) || "",
			s_desc_html = s_desc.innerHTML;
			s_show_div.appendAfterNeighborElement(_this);
			var s_show = BALA.one(".superbox-show") || "";
			setStyleDisplayBlock(s_show);
			var s_cur_desc = BALA.one(".superbox-current-desc") || "";
			removeChildElements(s_cur_desc),
			/* s_cur_desc.innerHTML = s_desc_html, */
			s_cur_desc.insertAdjacentHTML("beforeend", s_desc_html),
			crel(s_cur_desc, s_close_div),
			setStyleOpacity(s_cur_desc, 0),
			setStyleDisplayBlock(s_cur_desc);
			/*!
			 * If you want coords relative to the parent node, use element.offsetTop.
			 * Add element.scrollTop if you want to take the parent scrolling into account.
			 * (or use jQuery .position() if you are fan of that library)
			 * If you want coords relative to the document use element.getBoundingClientRect().top.
			 * Add window.pageYOffset if you want to take the document scrolling into account.
			 * Subtract element.clientTop if you don't consider the element border as the part of the element
			 * stackoverflow.com/questions/6777506/offsettop-vs-jquery-offset-top
			 * In IE<=11, calling getBoundingClientRect on an element outside of the DOM
			 * throws an unspecified error instead of returning a 0x0 DOMRect. See IE bug #829392.
			 * caniuse.com/#feat=getboundingclientrect
			 */
			/*!
			 * stackoverflow.com/questions/3464876/javascript-get-window-x-y-position-for-scroll
			 */
			var reveal_pos = _this.offsetTop,
			hide_pos = w.pageYOffset || d.documentElement.scrollTop;
			/* crel(s_cur_desc, crel("p", "" + reveal_pos + " / " + hide_pos)); */
			setImmediate(function () {
				w.zenscroll ? zenscroll.toY(reveal_pos, 500) : w.scroll(0, reveal_pos);
			}),
			FX.fadeIn(s_cur_desc, {
				duration : 500
			});
			/*!
			 * track clicks on external links
			 */
			var link = BALA.one("a", s_cur_desc) || "";
			if (link) {
				var links = BALA("a", s_cur_desc),
				s = /localhost/.test(self.location.host) ? "http://localhost/externalcounters/" : "//shimansky.biz/externalcounters/",
				rfrr = encodeURIComponent(d.location.href || ""),
				ttl = encodeURIComponent(d.title || "").replace("\x27", "&#39;"),
				b = BALA.one("body").firstChild || "",
				g = function (_this) {
					h = _this.getAttribute("href") || "",
					dmn = h ? encodeURIComponent(h) : "",
					a = crel("div", {
							"style" : "position:absolute;left:-9999px;width:1px;height:1px;border:0;background:transparent url(" + s + "?dmn=" + dmn + "&rfrr=" + rfrr + "&ttl=" + ttl + "&encoding=utf-8) top left no-repeat;"
						});
					b && a.appendAfterNeighborElement(b);
					/* setImmediate(function () {
						openDeviceBrowser(h);
					}); */
				},
				trackClicks = function (e) {
					evento.add(e, "click", function (evt) {
						evt.preventDefault();
						evt.stopPropagation();
						g(this);
					});
				};
				if (w._) {
					_.each(links, function (e) {
						trackClicks(e);
					});
				} else if (w.forEach) {
					forEach(links, function (e) {
						trackClicks(e);
					}, !1);
				} else {
					for (var j = 0, l = links.length; j < l; j += 1) {
						trackClicks(links[j]);
					};
				}
			}
			/*!
			 * hide description
			 */
			var s_close = BALA.one(".superbox-close", s_cur_desc) || "",
			doOnClose = function () {
				setImmediate(function () {
					w.zenscroll ? zenscroll.toY(hide_pos, 500) : w.scroll(0, hide_pos);
				}),
				FX.fadeOut(s_cur_desc, {
					duration : 500,
					complete : function () {
						setStyleDisplayNone(s_cur_desc),
						setStyleDisplayNone(s_show);
					}
				});
			};
			if (s_close) {
				evento.add(s_close, "click", function (ent) {
					ent.preventDefault();
					ent.stopPropagation();
					doOnClose();
				});
			}
		});
	};
	if (lists) {
		if (w._) {
			_.each(lists, function (e) {
				renderLists(e);
			});
		} else if (w.forEach) {
			forEach(lists, function (e) {
				renderLists(e);
			}, !1);
		} else {
			for (var i = 0, l = lists.length; i < l; i += 1) {
				renderLists(lists[i]);
			};
		}
	}
};
docReady(function () {
	initSuperBox();
});
/*!
 * init disqus_thread and Masonry / Packery
 * add Draggabilly to Packarey
 * gist.github.com/englishextra/5e423ff34f67982f017b
 */
var initMasonryAndDisqusThread = function () {
	var w = window,
	disqus_thread = BALA.one("#disqus_thread") || "",
	is_active = "is-active",
	disqus_shortname = disqus_thread ? (disqus_thread.dataset.shortname || "") : "",
	embed_js_src = getHTTPProtocol() + "://" + disqus_shortname + ".disqus.com/embed.js",
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	masonry_js_src = "../cdn/masonry/4.0.0/js/masonry.pkgd.fixed.min.js",
	packery_js_src = "../cdn/packery/2.0.0/js/packery.pkgd.fixed.min.js",
	draggabilly_js_src = "../cdn/draggabilly/2.1.0/js/draggabilly.pkgd.fixed.min.js",
	q = function (a) {
		var s = function () {
			if (w.Masonry) {
				msnry = new Masonry(a, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
			}
		};
		scriptIsLoaded(masonry_js_src) || loadJS(masonry_js_src, function () {
			s();
		});
	},
	v = function (a, c) {
		var s = function () {
			if (w.Packery) {
				pckry = new Packery(a, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
				if (c) {
					if (w.Draggabilly) {
						var t = function (e) {
							var draggableElem = e;
							draggie = new Draggabilly(draggableElem, {});
							draggies.push(draggie);
						};
						var draggies = [];
						if (w._) {
							_.each(c, function (e) {
								t(e);
							});
						} else if (w.forEach) {
							forEach(c, function (e) {
								t(e);
							}, !1);
						} else {
							for (var i = 0, l = c.length; i < l; i += 1) {
								t(c[i]);
							};
						}
						pckry && pckry.bindDraggabillyEvents(draggie);
					}
				}
			}
		};
		scriptIsLoaded(packery_js_src) || loadJS(packery_js_src, function () {
			scriptIsLoaded(draggabilly_js_src) || loadJS(draggabilly_js_src, function () {
				s();
			});
		});
	},
	z = function () {
		var s = function () {
			var si = new interval(50, function () {
				var disqus_thread_height = disqus_thread.clientHeight || disqus_thread.offsetHeight || "";
				if (108 < disqus_thread_height && 0 !== si) {
					si.stop(),
					si = 0;
					if ("undefined" !== typeof msnry && msnry) {
						msnry.layout();
					} else {
						if ("undefined" !== typeof pckry && pckry) {
							pckry.layout();
						}
					}
				}
			});
			si && si.run();
			disqus_thread.classList.add(is_active);
		};
		scriptIsLoaded(embed_js_src) || loadJS(embed_js_src, function () {
			s();
		});
	};
	if (grid && grid_item) {
		var msnry,
		pckry;
		q(grid);
		/* var c = BALA(h) || "";
		v(grid, c); */
		if (disqus_thread && disqus_shortname) {
			if (!!getHTTPProtocol()) {
				z();
			} else {
				setStyleDisplayNone(disqus_thread.parentNode.parentNode);
			}
		}
	}
};
if ("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) {
	var a = BALA.one("#disqus_thread") || "";
	setStyleDisplayNone(a.parentNode.parentNode);
} else {
	evento.add(window, "load", function () {
		initMasonryAndDisqusThread();
	});
}
/*!
 * Load HTML content
 * b.innerHTML=a.responseText changed to replaceInnerHTML(b,a.responseText)
 * dependency replaceInnerHTML ianopolous.github.io/javascript/innerHTML
 */
var AJAXIncludeHTML = function (id, u, cb) {
	var w = window,
	container = BALA.one("#" + id) || "",
	g = function (d, t, f) {
		var c = d.cloneNode(!1);
		c.innerHTML = t;
		d.parentNode.replaceChild(c, d);
		f && "function" === typeof f && f();
	};
	if (container) {
		if (w.Promise && w.fetch && !isElectron) {
			fetch(u).then(function (r) {
				return r.text();
			}).catch (function (e) {
				console.log("Error fetch-ing file", e);
			}).then(function (t) {
				g(container, t, cb);
			}).catch (function (e) {
				console.log("Error inserting content", e);
			});
		} else if (w.reqwest) {
			reqwest({
				url : u,
				type : "html",
				method : "get",
				error : function (e) {
					console.log("Error reqwest-ing file", e);
				},
				success : function (r) {
					g(container, r, cb);
				}
			});
		} else {
			var x = w.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
			x.onreadystatechange = function () {
				if (x.readyState == 4 && x.status == 200) {
					g(container, x.responseText, cb);
				}
			}
			x.open("GET", u, true);
			x.send();
		}
	}
};
/*!
 * load or refresh disqus_thread on click
 */
var loadRefreshDisqusThread = function () {
	var w = window,
	disqus_thread = BALA.one("#disqus_thread") || "",
	is_active = "is-active",
	btn = BALA.one("#btn-show-disqus-thread") || "",
	disqus_shortname = disqus_thread ? (disqus_thread.dataset.shortname || "") : "",
	p = w.location.href || "",
	embed_js_src = getHTTPProtocol() + "://" + disqus_shortname + ".disqus.com/embed.js",
	g = function () {
		setStyleDisplayNone(btn);
		disqus_thread.classList.add(is_active);
	},
	k = function () {
		try {
			DISQUS.reset({
				reload : !0,
				config : function () {
					this.page.identifier = disqus_shortname;
					this.page.url = p;
				}
			});
			g();
		} catch(e) {
			setStyleDisplayBlock(btn);
		}
	},
	v = function () {
		loadJS(embed_js_src, function () {
			g();
		});
	},
	z = function () {
		var s = crel("p", "\u041A\u043E\u043C\u043C\u0435\u043D\u0442\u0430\u0440\u0438\u0438 \u0434\u043E\u0441\u0442\u0443\u043F\u043D\u044B \u0442\u043E\u043B\u044C\u043A\u043E \u0432 \u0432\u0435\u0431 \u0432\u0435\u0440\u0441\u0438\u0438 \u044D\u0442\u043E\u0439 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u044B.");
		removeChildElements(disqus_thread);
		crel(disqus_thread, s);
		disqus_thread.removeAttribute("id");
		setStyleDisplayNone(btn.parentNode);
	};
	if (disqus_thread && btn && disqus_shortname && p) {
		if (!!getHTTPProtocol()) {
			if (scriptIsLoaded(embed_js_src)) {
				k();
			} else {
				v();
			}
		} else {
			z();
		}
	}
};
/*!
 * init disqus_thread on scroll
 */
var initDisqusThreadOnScroll = function () {
	var h1 = BALA.one("#h1") || "";
	if (h1) {
		var w = window,
		h = BALA.one("html") || "",
		data_loading = "data-loading",
		disqus_thread = BALA.one("#disqus_thread") || "",
		is_active = "is-active",
		btn = BALA.one("#btn-show-disqus-thread") || "",
		p = w.location.href || "",
		disqus_shortname = disqus_thread ? (disqus_thread.dataset.shortname || "") : "",
		embed_js_src = getHTTPProtocol() + "://" + disqus_shortname + ".disqus.com/embed.js",
		g = function () {
			setStyleDisplayNone(btn);
			disqus_thread.classList.add(is_active);
			!!waypoint && waypoint.destroy();
		},
		k = function () {
			scriptIsLoaded(embed_js_src) || loadJS(embed_js_src, function () {
				g();
			});
		},
		q = function () {
			evento.add(btn, "click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				k();
			});
		},
		v = function () {
			var s = crel("p", "\u041A\u043E\u043C\u043C\u0435\u043D\u0442\u0430\u0440\u0438\u0438 \u0434\u043E\u0441\u0442\u0443\u043F\u043D\u044B \u0442\u043E\u043B\u044C\u043A\u043E \u0432 \u0432\u0435\u0431 \u0432\u0435\u0440\u0441\u0438\u0438 \u044D\u0442\u043E\u0439 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u044B.");
			removeChildElements(disqus_thread);
			crel(disqus_thread, s);
			disqus_thread.removeAttribute("id");
			setStyleDisplayNone(btn.parentNode);
		};
		if (disqus_thread && btn && disqus_shortname && p) {
			if (!!getHTTPProtocol()) {
				if ("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) {
					q();
				} else {
					if (w.Waypoint) {
						try {
							var waypoint = new Waypoint({
									element : h1,
									handler : function (direction) {
										k();
									}
								});
						} catch (e) {
							console.log(e);
						}
						q();
					} else {
						q();
					}
				}
			} else {
				v();
			}
		}
	}
};
evento.add(window, "load", function () {
	initDisqusThreadOnScroll();
});
/*!
 * init ads
 */
var showDownloadAppBtn = function () {
	var b = BALA.one("body") || "",
	n = navigator.userAgent || "",
	g = function () {
		var a = crel("a", {
				"class" : "btn-download-app",
				"target" : "_blank"
			}),
		c = " left top no-repeat;background-size:9.000rem 3.250rem;position:fixed;top:3.125em;right:1.063em;-webkit-box-shadow: 0.188em 0.188em 0.188em 0 rgba(50,50,50,0.5);-moz-box-shadow: 0.188em 0.188em 0.188em 0 rgba(50,50,50,0.5);box-shadow: 0.188em 0.188em 0.188em 0 rgba(50,50,50,0.3);opacity:0;width:9.000rem;height:3.250rem;";
		(/Windows/i.test(n) && /(WOW64|Win64)/i.test(n)) ? a = crel(a, {
				"style" : "background:transparent url(../../libs/products/img/download_windows_app_144x52.png)" + c,
				"href" : "https://englishextraapp.codeplex.com/downloads/get/1539373"
			}) :
			(/(x86_64|x86-64|x64;|amd64|AMD64|x64_64)/i.test(n) && /(Linux|X11)/i.test(n)) ? a = crel(a, {
				"style" : "background:transparent url(../../libs/products/img/download_linux_app_144x52.png)" + c,
				"href" : "https://englishextraapp.codeplex.com/downloads/get/1540156"
			}) :
			(/IEMobile/i.test(n)) ? a = crel(a, {
				"style" : "background:transparent url(../../libs/products/img/download_wp_app_144x52.png)" + c,
				"href" : "https://englishextraapp.codeplex.com/downloads/get/1536102"
			}) :
			(/Android/i.test(n)) ? a = crel(a, {
				"style" : "background:transparent url(../../libs/products/img/download_android_app_144x52.png)" + c,
				"href" : "https://englishextraapp.codeplex.com/downloads/get/1528911"
			}) :
			setStyleDisplayNone(a);
		crel(b, a);
		FX.fadeIn(a, {
			duration : 500
		});
		setAndClearTimeout(function () {
				FX.fadeOut(a, {
					duration : 500
				});
				setAndClearTimeout(function () {
						setStyleDisplayNone(a);
					}, 500);
			}, 8000);
	};
	if (!!getHTTPProtocol() && b && !!n) {
		setAndClearTimeout(function () {
			g();
		}, 3000);
	}
};
evento.add(window, "load", function () {
	showDownloadAppBtn();
});
/*!
 * init static select
 */
var initStaticSelect = function () {
	var w = window,
	a = BALA.one("#select") || "",
	g = function (_this) {
		var h = _this.options[_this.selectedIndex].value || "",
		zh = h ? (/^#/.test(h) ? BALA.one(h) : "") : "";
		if (h) {
			w.zenscroll ? (zh ? zenscroll.to(zh) : changeDocumentLocation(h)) : changeDocumentLocation(h);
		}
	},
	k = function () {
		evento.add(a, "change", function () {
			g(this);
		});
	};
	if (a) {
		k();
	}
};
docReady(function () {
	initStaticSelect();
});
/*!
 * init AJAX JSON select
 */
var initContentsSelect = function () {
	var w = window,
	a = BALA.one("#select") || "",
	contents_json_src = "../libs/contents/json/contents.json",
	g = function (e) {
		var t = getJsonKeyValues(e, "label") || "",
		p = getJsonKeyValues(e, "link") || "";
		if (t && p) {
			crel(a, crel("option", {
					"value" : p,
					"title" : "" + t
				}, truncString("" + t, 33)));
		}
	},
	k = function (_this) {
		var h = _this.options[_this.selectedIndex].value || "",
		zh = h ? (/^#/.test(h) ? BALA.one(h) : "") : "";
		if (h) {
			w.zenscroll ? (zh ? zenscroll.to(zh) : changeDocumentLocation(h)) : changeDocumentLocation(h);
		}
	},
	q = function (r) {
		var jpr = safelyParseJSON(r);
		if (jpr) {
			if (w._) {
				_.each(jpr, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(jpr, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = jpr.length; i < l; i += 1) {
					g(jpr[i]);
				};
			}
			evento.add(a, "change", function () {
				k(this);
			});
		}
	};
	if (a) {
		if (w.Promise && w.fetch && !isElectron) {
			fetch(contents_json_src).then(function (r) {
				return r.text();
			}).catch (function (e) {
				console.log("Error fetch-ing file", e);
			}).then(function (t) {
				q(t);
			}).catch (function (e) {
				console.log("Error parsing file", e);
			});
		} else {
			AJAXloadUnparsedJSON(contents_json_src, function (r) {
				q(r);
			});
		}
	}
};
evento.add(window, "load", function () {
	initContentsSelect();
});
/*!
 * init Contents Kamil autocomplete
 * github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion
 */
var initContentsKamilAutocomplete = function () {
	var w = window,
	d = document,
	b = BALA.one("body") || "",
	search_form = BALA.one("#search_form") || "",
	text_id = "text",
	text = BALA.one("#" + text_id) || "",
	contents_json_src = "../libs/contents/json/contents.json",
	q = function (r) {
		var jpr = safelyParseJSON(r);
		if (jpr) {
			var ac = new Kamil("#" + text_id, {
					source : jpr,
					minChars : 2
				});
			/*!
			 * show suggestions
			 */
			ac.renderMenu = function (ul, items) {
				var _this = this;
				/*!
				 * limit output
				 */
				if (w._) {
					_.each(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					});
				} else if (w.forEach) {
					forEach(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					}, !1);
				} else {
					for (var i = 0, l = items.length; i < l; i += 1) {
						if (i < 10) {
							_this._renderItemData(ul, items[i], i);
						}
					};
				};
				/*!
				 * truncate text
				 */
				var g = function (e) {
					var t = e.firstChild.textContent || "",
					n = document.createTextNode(truncString(t, 24));
					e.replaceChild(n, e.firstChild);
					crel(e, {
						title : "" + t
					});
				},
				lis = BALA("li", ul);
				if (w._) {
					_.each(lis, function (e) {
						g(e);
					});
				} else if (w.forEach) {
					forEach(lis, function (e) {
						g(e);
					}, !1);
				} else {
					for (var i = 0, l = lis.length; i < l; i += 1) {
						g(lis[i]);
					};
				}
			};
			/*!
			 * use kamil built-in word label as search key in JSON file
			 * [{"link":"/","label":"some text to match"},
			 * {"link":"/pages/contents.html","label":"some text to match"}]
			 */
			ac.on("kamilselect", function (e) {
				var p = e.item.link || "";
				if (p) {
					setImmediate(function () {
						e.inputElement.value = "";
						changeDocumentLocation(p);
					});
				}
			});
		}
	};
	if (search_form && text) {
		if (w.Promise && w.fetch && !isElectron) {
			fetch(contents_json_src).then(function (r) {
				return r.text();
			}).catch (function (e) {
				console.log("Error fetch-ing file", e);
			}).then(function (t) {
				q(t);
			}).catch (function (e) {
				console.log("Error parsing file", e);
			});
		} else {
			AJAXloadUnparsedJSON(contents_json_src, function (r) {
				q(r);
			});
		}
	}
};
evento.add(window, "load", function () {
	initContentsKamilAutocomplete();
});
/*!
 * init Contents Kamil autocomplete
 * github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion
 */
var initContentsKamilAutocomplete = function () {
	var w = window,
	d = document,
	b = BALA.one("body") || "",
	search_form = BALA.one("#search_form") || "",
	text_id = "text",
	text = BALA.one("#" + text_id) || "",
	_ul_id = "kamil-typo-autocomplete",
	_ul_class = "kamil-autocomplete",
	kamil_js_src = "../cdn/kamil/0.1.1/js/kamil.min.js",
	contents_json_src = "../libs/contents/json/contents.json";
	scriptIsLoaded(kamil_js_src) || loadJS(kamil_js_src, function () {
		var q = function (r) {
			var jpr = safelyParseJSON(r);
			var ac = new Kamil("#" + text_id, {
					source : jpr,
					minChars : 2
				});
			/*!
			 * create typo suggestion list
			 */
			var _ul = crel("ul", {
					"class" : _ul_class,
					"id" : _ul_id,
					"style" : "display:none;"
				}),
			_li = crel("li", {
					"style" : "display:none;"
				});
			crel(_ul, crel(_li));
			_ul.appendAfterNeighborElement(text);
			/*!
			 * show suggestions
			 */
			ac.renderMenu = function (ul, items) {
				var l = items.length,
				_this = this;
				/*!
				 * limit output
				 */
				if (w._) {
					_.each(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					});
				} else if (w.forEach) {
					forEach(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					}, !1);
				} else {
					for (var i = 0; i < l; i += 1) {
						if (i < 10) {
							_this._renderItemData(ul, items[i], i);
						}
					};
				}
				/*!
				 * fix typo - non latin characters found
				 */
				while (l < 1) {
					var v = text.value;
					if (/[^\u0000-\u007f]/.test(v)) {
						v = fixEnRuTypo(v, "ru", "en");
					} else {
						v = fixEnRuTypo(v, "en", "ru");
					}
					crel(_ul, {
						"style" : "display:block;"
					});
					crel(_li, {
						"style" : "display:block;"
					});
					removeChildElements(_li);
					crel(_li, {
						"onclick" : '(function(a,b,c){c&&(c.value="' + v + '")&&c.focus();b&&b.setAttribute("style","display:none;"),a&&a.setAttribute("style","display:none;");}(this,document.getElementById("' + _ul_id + '")||"",document.getElementById("' + text_id + '")||""));'
					}, "" + v);
					if (v.match(/^\s*$/)) {
						crel(_ul, {
							"style" : "display:none;"
						});
						crel(_li, {
							"style" : "display:none;"
						});
					}
					evento.add(text, "input", function () {
						if (text.value.length < 3 || text.value.match(/^\s*$/)) {
							crel(_ul, {
								"style" : "display:none;"
							});
							crel(_li, {
								"style" : "display:none;"
							});
						}
					});
					l++;
				};
				/*!
				 * truncate text
				 */
				var g = function (e) {
					var t = e.firstChild.textContent || "",
					n = document.createTextNode(truncString(t, 24));
					e.replaceChild(n, e.firstChild);
					crel(e, {
						title : "" + t
					});
				},
				lis = BALA("li", ul);
				if (w._) {
					_.each(lis, function (e) {
						g(e);
					});
				} else if (w.forEach) {
					forEach(lis, function (e) {
						g(e);
					}, !1);
				} else {
					for (var i = 0, l = lis.length; i < l; i += 1) {
						g(lis[i]);
					};
				}
			};
			/*!
			 * use kamil built-in word label as search key in JSON file
			 * [{"link":"/","label":"some text to match"},
			 * {"link":"/pages/contents.html","label":"some text to match"}]
			 */
			ac.on("kamilselect", function (e) {
				var p = e.item.link || "";
				if (p) {
					setImmediate(function () {
						e.inputElement.value = "";
						changeDocumentLocation(p);
					});
				}
			});
		};
		if (search_form && text) {
			if (w.Promise && w.fetch && !isElectron) {
				fetch(contents_json_src).then(function (r) {
					return r.text();
				}).catch (function (e) {
					console.log("Error fetch-ing file", e);
				}).then(function (t) {
					q(t);
				}).catch (function (e) {
					console.log("Error parsing file", e);
				});
			} else {
				AJAXloadUnparsedJSON(contents_json_src, function (r) {
					q(r);
				});
			}
		}
	});
};
evento.add(window, "load", function () {
	initContentsKamilAutocomplete();
});
/*!
 * init Pages Kamil autocomplete
 * github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion
 */
var initPagesKamilAutocomplete = function () {
	var w = window,
	d = document,
	b = BALA.one("body") || "",
	search_form = BALA.one("#search_form") || "",
	text_id = "text",
	text = BALA.one("#" + text_id) || "",
	pages_json_src = "../../libs/englishextra-ui/json/pages.json",
	q = function (r) {
		var jpr = safelyParseJSON(r);
		if (jpr) {
			var ac = new Kamil("#" + text_id, {
					source : jpr,
					minChars : 2
				});
			/*!
			 * show suggestions
			 */
			ac.renderMenu = function (ul, items) {
				var _this = this;
				/*!
				 * limit output
				 */
				if (w._) {
					_.each(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					});
				} else if (w.forEach) {
					forEach(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					}, !1);
				} else {
					for (var i = 0, l = items.length; i < l; i += 1) {
						if (i < 10) {
							_this._renderItemData(ul, items[i], i);
						}
					};
				};
				/*!
				 * truncate text
				 */
				var g = function (e) {
					var t = e.firstChild.textContent || "",
					n = document.createTextNode(truncString(t, 24));
					e.replaceChild(n, e.firstChild);
					crel(e, {
						title : "" + t
					});
				},
				lis = BALA("li", ul);
				if (w._) {
					_.each(lis, function (e) {
						g(e);
					});
				} else if (w.forEach) {
					forEach(lis, function (e) {
						g(e);
					}, !1);
				} else {
					for (var i = 0, l = lis.length; i < l; i += 1) {
						g(lis[i]);
					};
				}
			};
			/*!
			 * use kamil built-in word label as search key in JSON file
			 * [{"link":"/","label":"some text to match"},
			 * {"link":"/pages/contents.html","label":"some text to match"}]
			 */
			ac.on("kamilselect", function (e) {
				var p = e.item.link || "";
				if (p) {
					setImmediate(function () {
						e.inputElement.value = "";
						changeDocumentLocation(p);
					});
				}
			});
		}
	};
	if (search_form && text) {
		if (w.Promise && w.fetch && !isElectron) {
			fetch(pages_json_src).then(function (r) {
				return r.text();
			}).catch (function (e) {
				console.log("Error fetch-ing file", e);
			}).then(function (t) {
				q(t);
			}).catch (function (e) {
				console.log("Error parsing file", e);
			});
		} else {
			AJAXloadUnparsedJSON(pages_json_src, function (r) {
				q(r);
			});
		}
	}
};
evento.add(window, "load", function () {
	initPagesKamilAutocomplete();
});
/*!
 * init Pages Kamil autocomplete
 * github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion
 */
var initPagesKamilAutocomplete = function () {
	var w = window,
	d = document,
	b = BALA.one("body") || "",
	search_form = BALA.one("#search_form") || "",
	text_id = "text",
	text = BALA.one("#" + text_id) || "",
	_ul_id = "kamil-typo-autocomplete",
	_ul_class = "kamil-autocomplete",
	kamil_js_src = "../../cdn/kamil/0.1.1/js/kamil.min.js",
	pages_json_src = "../../libs/englishextra-ui/json/pages.json";
	scriptIsLoaded(kamil_js_src) || loadJS(kamil_js_src, function () {
		var q = function (r) {
			var jpr = safelyParseJSON(r);
			var ac = new Kamil("#" + text_id, {
					source : jpr,
					minChars : 2
				});
			/*!
			 * create typo suggestion list
			 */
			var _ul = crel("ul", {
					"class" : _ul_class,
					"id" : _ul_id,
					"style" : "display:none;"
				}),
			_li = crel("li", {
					"style" : "display:none;"
				});
			crel(_ul, crel(_li));
			_ul.appendAfterNeighborElement(text);
			/*!
			 * show suggestions
			 */
			ac.renderMenu = function (ul, items) {
				var l = items.length,
				_this = this;
				/*!
				 * limit output
				 */
				if (w._) {
					_.each(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					});
				} else if (w.forEach) {
					forEach(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					}, !1);
				} else {
					for (var i = 0; i < l; i += 1) {
						if (i < 10) {
							_this._renderItemData(ul, items[i], i);
						}
					};
				}
				/*!
				 * fix typo - non latin characters found
				 */
				while (l < 1) {
					var v = text.value;
					if (/[^\u0000-\u007f]/.test(v)) {
						v = fixEnRuTypo(v, "ru", "en");
					} else {
						v = fixEnRuTypo(v, "en", "ru");
					}
					crel(_ul, {
						"style" : "display:block;"
					});
					crel(_li, {
						"style" : "display:block;"
					});
					removeChildElements(_li);
					crel(_li, {
						"onclick" : '(function(a,b,c){c&&(c.value="' + v + '")&&c.focus();b&&b.setAttribute("style","display:none;"),a&&a.setAttribute("style","display:none;");}(this,document.getElementById("' + _ul_id + '")||"",document.getElementById("' + text_id + '")||""));'
					}, "" + v);
					if (v.match(/^\s*$/)) {
						crel(_ul, {
							"style" : "display:none;"
						});
						crel(_li, {
							"style" : "display:none;"
						});
					}
					evento.add(text, "input", function () {
						if (text.value.length < 3 || text.value.match(/^\s*$/)) {
							crel(_ul, {
								"style" : "display:none;"
							});
							crel(_li, {
								"style" : "display:none;"
							});
						}
					});
					l++;
				};
				/*!
				 * truncate text
				 */
				var g = function (e) {
					var t = e.firstChild.textContent || "",
					n = document.createTextNode(truncString(t, 24));
					e.replaceChild(n, e.firstChild);
					crel(e, {
						title : "" + t
					});
				},
				lis = BALA("li", ul);
				if (w._) {
					_.each(lis, function (e) {
						g(e);
					});
				} else if (w.forEach) {
					forEach(lis, function (e) {
						g(e);
					}, !1);
				} else {
					for (var i = 0, l = lis.length; i < l; i += 1) {
						g(lis[i]);
					};
				}
			};
			/*!
			 * use kamil built-in word label as search key in JSON file
			 * [{"link":"/","label":"some text to match"},
			 * {"link":"/pages/contents.html","label":"some text to match"}]
			 */
			ac.on("kamilselect", function (e) {
				var p = e.item.link || "";
				if (p) {
					setImmediate(function () {
						e.inputElement.value = "";
						changeDocumentLocation(p);
					});
				}
			});
		};
		if (search_form && text) {
			if (w.Promise && w.fetch && !isElectron) {
				fetch(pages_json_src).then(function (r) {
					return r.text();
				}).catch (function (e) {
					console.log("Error fetch-ing file", e);
				}).then(function (t) {
					q(t);
				}).catch (function (e) {
					console.log("Error parsing file", e);
				});
			} else {
				AJAXloadUnparsedJSON(pages_json_src, function (r) {
					q(r);
				});
			}
		}
	});
};
evento.add(window, "load", function () {
	initPagesKamilAutocomplete();
});
/*!
 * init vk-like on click
 */
var initVKOnClick = function () {
	var w = window,
	vk_like = BALA.one("#vk-like") || "",
	btn = BALA.one("#btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?122",
	g = function () {
		try {
			w.VK && (VK.init({
					apiId : (vk_like.dataset.apiid || ""),
					nameTransportPath : "/xd_receiver.htm",
					onlyWidgets : !0
				}), VK.Widgets.Like("vk-like", {
					type : "button",
					height : 24
				}));
			setStyleVisibilityVisible(vk_like);
			setStyleOpacity(vk_like, 1);
			setStyleDisplayNone(btn);
		} catch(e) {
			setStyleVisibilityHidden(vk_like);
			setStyleOpacity(vk_like, 0);
			setStyleDisplayBlock(btn);
		}
	},
	k = function () {
		scriptIsLoaded(openapi_js_src) || loadJS(openapi_js_src, function () {
			g();
		});
	}
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		});
	};
	if (vk_like && btn) {
		if (!!getHTTPProtocol()) {
				q();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	initVKOnClick();
});
/*!
 * init manUP.js
 */
var initManupJs = function () {
	var manup_js_src = "/cdn/ManUp.js/0.7/js/manup.fixed.min.js";
	if (!!getHTTPProtocol()) {
		AJAXLoadAndTriggerJs(manup_js_src);
	}
};
evento.add(window, "load", function () {
	initManupJs();
});
/*!
 * init routie
 */
var ci = "container-includes",
printNotFoundMessage = function (id) {
	var c = BALA.one("#" + ci) || "";
	removeChildElements(c);
	var e = crel("div", {
			"class" : "content-wrapper"
		}, crel("div", {
				"class" : "grid grid-pad"
			}, crel("div", {
					"class" : "col col-1-1"
				}, crel("div", {
						"class" : "content"
					}, crel("h1", {
							"id" : "h1"
						}, "\u041D\u0435\u0442 \u0442\u0430\u043A\u043E\u0439 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u044B. ", crel("a", {
								"href" : "./index.html"
							}, "\u0418\u0441\u043F\u0440\u0430\u0432\u0438\u0442\u044C?"))))));
	crel(c, e);
};
/*!
 * Hashbang (#!)
 * The URL patterns RouteMap uses are based on a file-system path analogy,
 * so all patterns must begin with a '/' character.
 * In order to support the hashbang convention,
 * even though all URL patterns must begin with '/',
 * a prefix can be specified.
 * The default prefix value is '#' but if you want your site to be indexed,
 * you can switch the prefix to be '#!'
 * github.com/OpenGamma/RouteMap
 * Instead of the _escaped_fragment_ URLs,
 * we'll generally crawl, render, and index the #! URLs.
 * webmasters.googleblog.com/2015/10/deprecating-our-ajax-crawling-scheme.html
 * yandex.ru/blog/webmaster/21369
 */
/*!
 * "#" => ""
 * "#/" => "/"
 * "#/home" => "/home"
 */
routie({
	/*!
	 * Search engines may not index ajax loaded content for homepage
	 */
	"/" : function () {
		AJAXIncludeHTML(ci, "./includes/home.html", function () {
			scrollToTop();
			if (!!getHTTPProtocol()) {
				try {
					("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || loadYandexMapOnClick("ymap");
				} catch (e) {
					console.log(e);
				}
			}
			document.title = jsStringEscape(initialDocumentTitle + userBrowsingDetails);
		});
	},
	"/feedback" : function () {
		AJAXIncludeHTML(ci, "./includes/feedback.html", function () {
			scrollToTop();
			if (!!getHTTPProtocol()) {
				("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || loadRefreshDisqusThread();
			}
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u041D\u0430\u043F\u0438\u0448\u0438\u0442\u0435 \u043C\u043D\u0435" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/schedule" : function () {
		if (!!getHTTPProtocol()) {
			AJAXIncludeHTML(ci, "./includes/schedule.html", function () {
				scrollToTop();
				document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0420\u0430\u0441\u043F\u0438\u0441\u0430\u043D\u0438\u0435" + jsStringEscape(userBrowsingDetails);
			});
		}
	},
	"/map" : function () {
		if (!!getHTTPProtocol()) {
			AJAXIncludeHTML(ci, "./includes/map.html", function () {
				scrollToTop();
				document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0421\u043C\u043E\u0442\u0440\u0435\u0442\u044C \u043D\u0430 \u043A\u0430\u0440\u0442\u0435" + jsStringEscape(userBrowsingDetails);
			});
		}
	},
	"/level_test" : function () {
		AJAXIncludeHTML(ci, "./includes/level_test.html", function () {
			scrollToTop();
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0423\u0440\u043E\u0432\u043D\u0435\u0432\u044B\u0439 \u0442\u0435\u0441\u0442" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/common_mistakes" : function () {
		AJAXIncludeHTML(ci, "./includes/common_mistakes.html", function () {
			scrollToTop();
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0420\u0430\u0441\u043F\u0440\u043E\u0441\u0442\u0440\u0430\u043D\u0435\u043D\u043D\u044B\u0435 \u043E\u0448\u0438\u0431\u043A\u0438" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/demo_ege" : function () {
		AJAXIncludeHTML(ci, "./includes/demo_ege.html", function () {
			scrollToTop();
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0414\u0435\u043C\u043E-\u0432\u0430\u0440\u0438\u0430\u043D\u0442 \u0415\u0413\u042D-11 \u0410\u042F (\u041F\u0427)" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/demo_ege_speaking" : function () {
		AJAXIncludeHTML(ci, "./includes/demo_ege_speaking.html", function () {
			scrollToTop();
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0414\u0435\u043C\u043E-\u0432\u0430\u0440\u0438\u0430\u043D\u0442 \u0415\u0413\u042D-11 \u0410\u042F (\u0423\u0427)" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/previous_ege_analysis" : function () {
		AJAXIncludeHTML(ci, "./includes/previous_ege_analysis.html", function () {
			scrollToTop();
			document.title = jsStringEscape(initialDocumentTitle) + " - " + "\u0415\u0413\u042D 2015: \u0440\u0430\u0437\u0431\u043E\u0440 \u043E\u0448\u0438\u0431\u043E\u043A" + jsStringEscape(userBrowsingDetails);
		});
	},
	"/*" : function () {
		scrollToTop();
		printNotFoundMessage(ci);
		document.title = jsStringEscape(initialDocumentTitle + userBrowsingDetails);
	}
});
/*!
 * observe mutations
 */
var observeMutations = function () {
	var c = BALA.one("#container") || "";
	if (c) {
		var observer = new MutationObserver(function (mutations) {
				var f = !1;
				mutations.forEach(function (m) {
					console.log("mutations observer: " + m.type);
					console.log(m.type, "added: " + m.addedNodes.length + " nodes");
					console.log(m.type, "removed: " + m.removedNodes.length + " nodes");
					if ("childList" === m.type) {
						f = !0;
					}
				});
				if (!0 === f) {
					setTargetBlankOnAnchors();
					initAJAXIncludeHTMLAnchors();
					initStaticSelect();
				}
			});
		observer.observe(c, {
			childList : !0,
			subtree : !0,
			attributes : !1,
			characterData : !1
		});
	}
};
evento.add(window, "hashchange", function () {
	/* respond changes of static DOM */
	highlightCurrentNavMenuItem();
	("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || showPageQRRef();
	/* respond changes of dynamic DOM */
	observeMutations();
});
/*!
 * show page, finish ToProgress
 */
evento.add(window, "load", function () {
	var w = window,
	container = BALA.one("#container") || "",
	g = function () {
		setStyleOpacity(container, 1),
		setImmediate(function () {
			progressBarAvailable && (progressBar.finish(), progressBar.hide());
		});
	},
	k = function () {
			var si = new interval(50, function () {
					if (hiddenPreloadImage && 0 !== si) {
						si.stop(),
						si = 0;
						g();
					}
				});
			si && si.run();
	};
	if (container) {
		if ("undefined" !== typeof hiddenPreloadImage) {
			k();
		} else {
			g();
		}
	}
});
