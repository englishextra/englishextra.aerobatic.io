/*!
 * safe way to handle console.log():
 * sitepoint.com/safe-console-log/
 */
"undefined"===typeof console&&(console={log:function(){}});
/*!
 * detect Electron and NW.js
 */
var isElectron="undefined"!==typeof window&&window.process&&"renderer"===window.process.type||"",
isNwjs="";try{"undefined"!==typeof require("nw.gui")&&(isNwjs=!0)}catch(a){isNwjs=!1};
var isOldOpera=!!window.opera||!1;
/*!
 * modified for babel ToProgress v0.1.1
 * http://github.com/djyde/ToProgress
 * arguments.callee changed to ToProgress
 * wrapped in curly brackets:
 * else{document.body.appendChild(this.progressBar);}
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 */
var ToProgress=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}function t(){var t,s=document.createElement("fakeelement"),i={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(t in i)if(void 0!==s.style[t])return i[t]}function s(t,s){if(this.progress=0,this.options={id:"top-progress-bar",color:"#F44336",height:"2px",duration:.2},t&&"object"==typeof t)for(var i in t)this.options[i]=t[i];if(this.options.opacityDuration=3*this.options.duration,this.progressBar=document.createElement("div"),this.progressBar.id=this.options.id,this.progressBar.setCSS=function(t){for(var s in t)this.style[s]=t[s]},this.progressBar.setCSS({position:s?"relative":"fixed",top:"0",left:"0",right:"0","background-color":this.options.color,height:this.options.height,width:"0%",transition:"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-moz-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s","-webkit-transition":"width "+this.options.duration+"s, opacity "+this.options.opacityDuration+"s"}),s){var o=document.querySelector(s);o&&(o.hasChildNodes()?o.insertBefore(this.progressBar,o.firstChild):o.appendChild(this.progressBar))}else document.body.appendChild(this.progressBar)}var i=t();return s.prototype.transit=function(){this.progressBar.style.width=this.progress+"%"},s.prototype.getProgress=function(){return this.progress},s.prototype.setProgress=function(t,s){this.show(),this.progress=t>100?100:0>t?0:t,this.transit(),s&&s()},s.prototype.increase=function(t,s){this.show(),this.setProgress(this.progress+t,s)},s.prototype.decrease=function(t,s){this.show(),this.setProgress(this.progress-t,s)},s.prototype.finish=function(t){var s=this;this.setProgress(100,t),this.hide(),i&&this.progressBar.addEventListener(i,function(t){s.reset(),s.progressBar.removeEventListener(t.type,ToProgress)})},s.prototype.reset=function(t){this.progress=0,this.transit(),t&&t()},s.prototype.hide=function(){this.progressBar.style.opacity="0"},s.prototype.show=function(){this.progressBar.style.opacity="1"},s;}());
/*!
 * init ToProgress
 */
var toprogress_options = {
	id : "top-progress-bar",
	color : "#FC6054",
	height : "3px",
	duration : .2
}, progressBar = new ToProgress(toprogress_options),
progressBarAvailable = "undefined" !== typeof window && window.progressBar ? !0 : !1,
startProgressBar = function (v) {
	v = v || 50;
	progressBarAvailable && progressBar.increase(v);
},
finishProgressBar = function () {
	progressBarAvailable && (progressBar.finish(), progressBar.hide());
};
progressBarAvailable && progressBar.increase(20);
/*!
 * modified MediaHack - (c) 2013 Pomke Nohkan MIT LICENCED.
 * gist.github.com/englishextra/ff8c9dde94abe32a9d7c4a65e0f2ccac
 * removed className fallback and additionally
 * returns earlyDeviceOrientation,earlyDeviceSize
 * Add media query classes to DOM nodes
 * github.com/pomke/mediahack/blob/master/mediahack.js
 */
var earlyDeviceOrientation="",earlyDeviceSize="";(function(d){function n(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.add(i);}}}function l(i){var n=i.split(" ");if(d){for(var i,e=0;e<n.length;e++){(i=n[e])&&d.remove(i);}}}var i={landscape:"all and (orientation:landscape)",portrait:"all and (orientation:portrait)"};var j={small:"all and (max-width:768px)",medium:"all and (min-width:768px) and (max-width:991px)",large:"all and (min-width:992px)"};for(var e in i){var o=window.matchMedia(i[e]);!function(i,e){var o=function(i){i.matches?(n(e),(earlyDeviceOrientation=e)):l(e);};o(i),i.addListener(o);}(o,e);}for(var e in j){var s=window.matchMedia(j[e]);!function(j,e){var s=function(j){j.matches?(n(e),(earlyDeviceSize=e)):l(e);};s(j),j.addListener(s);}(s,e);}}(document.documentElement.classList||""));
/*!
 * add mobile or desktop class
 * using Detect Mobile Browsers | Open source mobile phone detection
 * Regex updated: 1 August 2014
 * detectmobilebrowsers.com
 */
var earlyDeviceType;(function(a,b,c,n){var c=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(n)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0,4))?b:c;a&&c&&(a.className+=" "+c),(earlyDeviceType=c)}(document.getElementsByTagName("html")[0]||"","mobile","desktop",navigator.userAgent||navigator.vendor||window.opera));
/*!
 * add svg support class
 */
var earlySvgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/2000/svg","1.1")?b:"no-"+b;(earlySvgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svg"));
/*!
 * add svgasimg support class
 */
var earlySvgasimgSupport;(function(a,b){var c=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")?b:"no-"+b;(earlySvgasimgSupport=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","svgasimg"));
/*!
 * add touch support class
 */
var earlyHasTouch;(function(a,b){var c="ontouchstart" in document.documentElement?b:"no-"+b;(earlyHasTouch=c);a&&c&&(a.className+=" "+c)}(document.getElementsByTagName("html")[0]||"","touch"));
/*!
 * return date in YYYY-MM-DD format
 */
var earlyFnGetYyyymmdd=function(){"use strict";var a=new Date,b=a.getDate(),c=a.getMonth(),c=c+1,d=a.getFullYear();10>b&&(b="0"+b);10>c&&(c="0"+c);return d+"-"+c+"-"+b;};
/*!
 * Escape strings for use as JavaScript string literals
 * gist.github.com/englishextra/3053a4dc18c2de3c80ce7d26207681e0
 * modified github.com/joliss/js-string-escape
 */
var jsStringEscape=function(s){return(""+s).replace(/["'\\\n\r\u2028\u2029]/g,function(a){switch(a){case '"':case "'":case "\\":return"\\"+a;case "\n":return"\\n";case "\r":return"\\r";case "\u2028":return"\\u2028";case "\u2029":return"\\u2029"}})};
/*!
 * append details to title
 */
var initialDocumentTitle=document.title||"",
userBrowsingDetails=" ["+(earlyFnGetYyyymmdd()?earlyFnGetYyyymmdd():"")+(earlyDeviceType?" "+earlyDeviceType:"")+(earlyDeviceSize?" "+earlyDeviceSize:"")+(earlyDeviceOrientation?" "+earlyDeviceOrientation:"")+(earlySvgSupport?" "+earlySvgSupport:"")+(earlySvgasimgSupport?" "+earlySvgasimgSupport:"")+(earlyHasTouch?" "+earlyHasTouch:"")+"]";
document.title&&(document.title=jsStringEscape(document.title+userBrowsingDetails));
/*!
 * modified JavaScript Sync/Async forEach - v0.1.2 - 1/10/2012
 * github.com/cowboy/javascript-sync-async-foreach
 * Copyright (c) 2012 "Cowboy" Ben Alman; Licensed MIT
 * removed Node.js / browser support wrapper function
 * forEach(a,function(e){console.log("eachCallback: "+e);},!1});
 * forEach(a,function(e){console.log("eachCallback: "+e);},function(){console.log("doneCallback");});
 */
var forEach=function(a,b,c){var d=-1,e=a.length>>>0;(function f(g){var h,j=g===!1;do++d;while(!(d in a)&&d!==e);if(j||d===e){c&&c(!j,a);return}g=b.call({async:function(){return h=!0,f}},a[d],d,a),h||f(g)})()};
/*!
 * A function for elements selection
 * 0.1.9
 * github.com/finom/bala
 * global $ renamed to BALA, prepended var
 * a = BALA.one("#someid");
 * a = BALA.one(".someclass");
 * a = BALA(".someclass");
 */
var BALA=function(e,f,g){function c(a,b,d){d=Object.create(c.fn);a&&d.push.apply(d,a[f]?[a]:""+a===a?/</.test(a)?((b=e.createElement(b||f)).innerHTML=a,b.children):b?(b=c(b)[0])?b[g](a):d:e[g](a):"function"==typeof a?e.readyState[7]?a():e[f]("DOMContentLoaded",a):a);return d}c.fn=[];c.one=function(a,b){return c(a,b)[0]||null};return c}(document,"addEventListener","querySelectorAll");
/*!
 * Accurate Javascript setInterval replacement
 * gist.github.com/manast/1185904
 * gist.github.com/englishextra/f721a0c4d12aa30f74c2e089370e09eb
 * minified with closure-compiler.appspot.com/home
 * var si = new interval(50, function(){ if(1===1){si.stop(), si = 0;}}); si.run();
 * The handle will be a number that isn't equal to 0; therefore, 0 makes a handy flag value for "no timer set".
 * stackoverflow.com/questions/5978519/setinterval-and-how-to-use-clearinterval
 */
function interval(d,f){this.baseline=void 0;this.run=function(){void 0===this.baseline&&(this.baseline=(new Date).getTime());f();var c=(new Date).getTime();this.baseline+=d;var b=d-(c-this.baseline);0>b&&(b=0);(function(d){d.timer=setTimeout(function(){d.run(c)},b)}(this))};this.stop=function(){clearTimeout(this.timer)}};
/*!
 * modified for babel crel - a small, simple, and fast DOM creation utility
 * github.com/KoryNunn/crel
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * crel(tagName/dom element [, attributes, child1, child2, childN...])
 * var element=crel('div',crel('h1','Crello World!'),crel('p','This is crel'),crel('input',{type:'number'}));
 */
var crel=(function(){function e(){var o,a=arguments,p=a[0],m=a[1],x=2,v=a.length,b=e[f];if(p=e[c](p)?p:d.createElement(p),1===v)return p;if((!l(m,t)||e[u](m)||s(m))&&(--x,m=null),v-x===1&&l(a[x],"string")&&void 0!==p[r])p[r]=a[x];else for(;v>x;++x)if(o=a[x],null!=o)if(s(o))for(var g=0;g<o.length;++g)y(p,o[g]);else y(p,o);for(var h in m)if(b[h]){var N=b[h];typeof N===n?N(p,m[h]):p[i](N,m[h])}else p[i](h,m[h]);return p}var n="function",t="object",o="nodeType",r="textContent",i="setAttribute",f="attrMap",u="isNode",c="isElement",d=typeof document===t?document:{},l=function(e,n){return typeof e===n},a=typeof Node===n?function(e){return e instanceof Node}:function(e){return e&&l(e,t)&&o in e&&l(e.ownerDocument,t)},p=function(n){return e[u](n)&&1===n[o]},s=function(e){return e instanceof Array},y=function(n,t){e[u](t)||(t=d.createTextNode(t)),n.appendChild(t)};return e[f]={},e[c]=p,e[u]=a,"undefined"!=typeof Proxy&&(e.proxy=new Proxy(e,{get:function(n,t){return!(t in e)&&(e[t]=e.bind(null,t)),e[t]}})),e}());
/*!
 * implementing fadeIn and fadeOut without jQuery
 * gist.github.com/englishextra/baaa687f6ae9c7733d560d3ec74815cd
 * modified jsfiddle.net/LzX4s/
 * changed options.complete(); to:
 * function"==typeof options.complete && options.complete();
 * usage:
 * FX.fadeIn(document.getElementById('test'), {
 * 	duration: 2000,
 * 	complete: function() {
 * 		alert('Complete');
 * 	}
 * });
 */
(function(){var FX={easing:{linear:function(progress){return progress;},quadratic:function(progress){return Math.pow(progress,2);},swing:function(progress){return 0.5-Math.cos(progress*Math.PI)/2;},circ:function(progress){return 1-Math.sin(Math.acos(progress));},back:function(progress,x){return Math.pow(progress,2)*((x+1)*progress-x);},bounce:function(progress){for(var a=0,b=1,result;1;a+=b,b/=2){if(progress>=(7-4*a)/11){return-Math.pow((11-6*a-11*progress)/4,2)+Math.pow(b,2);}}},elastic:function(progress,x){return Math.pow(2,10*(progress-1))*Math.cos(20*Math.PI*x/3*progress);}},animate:function(options){var start=new Date;var id=setInterval(function(){var timePassed=new Date-start;var progress=timePassed/options.duration;if(progress>1){progress=1;};options.progress=progress;var delta=options.delta(progress);options.step(delta);if(progress==1){clearInterval(id);"function"==typeof options.complete&&options.complete();}},options.delay||10);},fadeOut:function(element,options){var to=1;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to-delta;}});},fadeIn:function(element,options){var to=0;this.animate({duration:options.duration,delta:function(progress){progress=this.progress;return FX.easing.swing(progress);},complete:options.complete,step:function(delta){element.style.opacity=to+delta;}});}};window.FX=FX;}());
/*!
 * modified for babel Zenscroll 3.2.2
 * github.com/zengabor/zenscroll
 * removed AMD, CommonJS support
 * changed this or window to self as argument
 * Copyright 2015–2016 Gabor Lenard
 * minified with closure-compiler.appspot.com/home
 * github.com/zengabor/zenscroll/blob/dist/zenscroll.js
 */
var zenscroll=(function(){"use strict";if("undefined"==typeof window||!("document"in window)){return{};}var t=function(t,e,n){e=e||999,n||0===n||(n=9);var o,i=document.documentElement,r=function(){return"getComputedStyle"in window&&"smooth"===window.getComputedStyle(t?t:document.body)["scroll-behavior"]},c=function(){return t?t.scrollTop:window.scrollY||i.scrollTop},u=function(){return t?Math.min(t.offsetHeight,window.innerHeight):window.innerHeight||i.clientHeight},f=function(e){return t?e.offsetTop:e.getBoundingClientRect().top+c()-i.offsetTop},l=function(){clearTimeout(o),o=0},a=function(n,f,a){if(l(),r())(t||window).scrollTo(0,n),a&&a();else{var d=c(),s=Math.max(n,0)-d;f=f||Math.min(Math.abs(s),e);var m=(new Date).getTime();!function e(){o=setTimeout(function(){var n=Math.min(((new Date).getTime()-m)/f,1),o=Math.max(Math.floor(d+s*(n<.5?2*n*n:n*(4-2*n)-1)),0);t?t.scrollTop=o:window.scrollTo(0,o),n<1&&u()+o<(t||i).scrollHeight?e():(setTimeout(l,99),a&&a())},9)}()}},d=function(t,e,o){a(f(t)-n,e,o)},s=function(t,e,o){var i=t.getBoundingClientRect().height,r=f(t),l=r+i,s=u(),m=c(),h=m+s;r-n<m||i+n>s?d(t,e,o):l+n>h?a(l-s+n,e,o):o&&o()},m=function(t,e,n,o){a(Math.max(f(t)-u()/2+(n||t.getBoundingClientRect().height/2),0),e,o)},h=function(t,o){t&&(e=t),(0===o||o)&&(n=o)};return{setup:h,to:d,toY:a,intoView:s,center:m,stop:l,moving:function(){return!!o}}},e=t();if("addEventListener"in window&&"smooth"!==document.body.style.scrollBehavior&&!window.noZensmooth){var n=function(t){try{history.replaceState({},"",window.location.href.split("#")[0]+t)}catch(t){}};window.addEventListener("click",function(t){for(var o=t.target;o&&"A"!==o.tagName;)o=o.parentNode;if(!(!o||1!==t.which||t.shiftKey||t.metaKey||t.ctrlKey||t.altKey)){var i=o.getAttribute("href")||"";if(0===i.indexOf("#"))if("#"===i)t.preventDefault(),e.toY(0),n("");else{var r=o.hash.substring(1),c=document.getElementById(r);c&&(t.preventDefault(),e.to(c),n("#"+r))}}},!1)}return{createScroller:t,setup:e.setup,to:e.to,toY:e.toY,intoView:e.intoView,center:e.center,stop:e.stop,moving:e.moving};}());
/*!
 * Scroll to top with Zenscroll and fallback
 */
var scrollToTop=function(){var w=window,g=function(){w.zenscroll?zenscroll.toY(0):w.scrollTo(0,0);};w.setImmediate?setImmediate(function(){g()}):setTimeout(function(){g();});};
/*!
 * Plain javascript replacement for jQuery's .ready()
 * so code can be scheduled to run when the document is ready
 * github.com/jfriend00/docReady
 * docReady(function(){});
 * simple substitute by Christoph at stackoverflow.com/questions/8100576/how-to-check-if-dom-is-ready-without-a-framework
 * (function(){var a=document.readyState;"interactive"===a||"complete"===a?(function(){}()):setTimeout(arguments.callee,100)})();
 */
(function(funcName,baseObj){"use strict";funcName=funcName||"docReady";baseObj=baseObj||window;var readyList=[];var readyFired=false;var readyEventHandlersInstalled=false;function ready(){if(!readyFired){readyFired=true;for(var i=0;i<readyList.length;i++){readyList[i].fn.call(window,readyList[i].ctx);}readyList=[];}}function readyStateChange(){if(document.readyState==="complete"){ready();}}baseObj[funcName]=function(callback,context){if(readyFired){setTimeout(function(){callback(context);},1);return;}else{readyList.push({fn:callback,ctx:context});}if(document.readyState==="complete"||(!document.attachEvent&&document.readyState==="interactive")){setTimeout(ready,1);}else if(!readyEventHandlersInstalled){if(document.addEventListener){document.addEventListener("DOMContentLoaded",ready,false);window.addEventListener("load",ready,false);}else{document.attachEvent("onreadystatechange",readyStateChange);window.attachEvent("onload",ready);}readyEventHandlersInstalled=true;}}})("docReady",window);
/*!
 * modified for babel Evento - v1.0.0
 * by Erik Royall <erikroyalL@hotmail.com> (http://erikroyall.github.io)
 * changed this or window to self as argument
 * added if("undefined"==typeof window||!("document"in window))
 * {return console.log("window is undefined or document is not in window"),!1;}
 * Dual licensed under MIT and GPL
 * Array.prototype.indexOf shim
 * developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
 * gist.github.com/erikroyall/6618740
 * gist.github.com/englishextra/3a959e4da0fcc268b140
 * evento.add(window,"load",function(){});
 */
if(!Array.prototype.indexOf){Array.prototype.indexOf=function(searchElement){'use strict';if(this==null){throw new TypeError();}var n,k,t=Object(this),len=t.length>>>0;if(len===0){return-1;}n=0;if(arguments.length>1){n=Number(arguments[1]);if(n!=n){n=0;}else if(n!=0&&n!=Infinity&&n!=-Infinity){n=(n>0||-1)*Math.floor(Math.abs(n));}}if(n>=len){return-1;}for(k=n>=0?n:Math.max(len-Math.abs(n),0);k<len;k++){if(k in t&&t[k]===searchElement){return k;}}return-1;};}var evento=(function(){if("undefined"==typeof window||!("document"in window)){return console.log("window is undefined or document is not in window"),!1;}var win=window,doc=win.document,_handlers={},addEvent,removeEvent,triggerEvent;addEvent=(function(){if(typeof doc.addEventListener==="function"){return function(el,evt,fn){el.addEventListener(evt,fn,false);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else if(typeof doc.attachEvent==="function"){return function(el,evt,fn){el.attachEvent(evt,fn);_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}else{return function(el,evt,fn){el["on"+evt]=fn;_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt].push(fn);};}}());removeEvent=(function(){if(typeof doc.removeEventListener==="function"){return function(el,evt,fn){el.removeEventListener(evt,fn,false);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else if(typeof doc.detachEvent==="function"){return function(el,evt,fn){el.detachEvent(evt,fn);Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}else{return function(el,evt,fn){el["on"+evt]=undefined;Helio.each(_handlers[el][evt],function(fun){if(fun===fn){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];_handlers[el][evt][_handlers[el][evt].indexOf(fun)]=undefined;}});};}}());triggerEvent=function(el,evt){_handlers[el]=_handlers[el]||{};_handlers[el][evt]=_handlers[el][evt]||[];for(var _i=0,_l=_handlers[el][evt].length;_i<_l;_i+=1){_handlers[el][evt][_i]();}};return{add:addEvent,remove:removeEvent,trigger:triggerEvent,_handlers:_handlers};}());
/*!
 * How can I check if a JS file has been included already?
 * stackoverflow.com/questions/18155347/how-can-i-check-if-a-js-file-has-been-included-already
 */
var scriptIsLoaded=function(s){for(var b=document.getElementsByTagName("script")||"",a=0;a<b.length;a++)if(b[a].getAttribute("src")==s)return!0;return!1};
/*!
 * Load and execute JS via AJAX
 * gist.github.com/englishextra/8dc9fe7b6ff8bdf5f9b483bf772b9e1c
 * IE 5.5+, Firefox, Opera, Chrome, Safari XHR object
 * gist.github.com/Xeoncross/7663273
 * modified callback(x.responseText,x); to callback(eval(x.responseText),x);
 * stackoverflow.com/questions/3728798/running-javascript-downloaded-with-xmlhttprequest
 */
var AJAXLoadAndTriggerJs=function(u,cb,d){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.open(d?"POST":"GET",u,!0);x.setRequestHeader("X-Requested-With","XMLHttpRequest");x.setRequestHeader("Content-type","application/x-www-form-urlencoded");x.onreadystatechange=function(){if(x.readyState>3){if(x.responseText){eval(x.responseText);cb&&"function"===typeof cb&&cb(x.responseText);}}};x.send(d);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * Load .json file, but don't JSON.parse it
 * modified JSON with JS.md
 * gist.github.com/thiagodebastos/08ea551b97892d585f17
 * gist.github.com/englishextra/e2752e27761649479f044fd93a602312
 */
var AJAXloadUnparsedJSON=function(u,cb){var w=window;try{var x=w.XMLHttpRequest?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");x.overrideMimeType("application/json;charset=utf-8");x.open("GET",u,!0);x.onreadystatechange=function(){if(x.readyState==4&&x.status=="200"){cb(x.responseText);}};x.send(null);}catch(e){console.log("Error XMLHttpRequest-ing file",e);}};
/*!
 * parse JSON with no eval using JSON-js/json_parse.js
 * with fallback to native JSON.parse
 */
var safelyParseJSON=function(d){var w=window;try{return w.json_parse?json_parse(d):JSON.parse(d);}catch(e){console.log(e);}};
/*!
 * return an array of values that match on a certain key
 * techslides.com/how-to-parse-and-search-json-in-javascript
 * gist.github.com/englishextra/872269c30d7cb2d10e3c3babdefc37b4
 * var jpr = JSON.parse(response);
 * for (var i = 0, l = jpr.length; i < l; i += 1) {
 * 	var o = jpr[i],
 * 	t = getJsonKeyValues(o, "label"),
 * 	p = getJsonKeyValues(o, "link");
 * 	crel(select, crel("option", {
 * 		"value" : p,
 * 		"title" : "" + t
 * 	}, truncString("" + t, 33)));
 * }
 */
var getJsonKeyValues=function(b,d){var c=[],a;for(a in b)b.hasOwnProperty(a)&&("object"==typeof b[a]?c=c.concat(getJsonKeyValues(b[a],d)):a==d&&c.push(b[a]));return c};
/*!
 * loop over the Array
 * stackoverflow.com/questions/18238173/javascript-loop-through-json-array
 * gist.github.com/englishextra/b4939b3430da4b55d731201460d3decb
 */
var truncString=function(str,max,add){add=add||"\u2026";return(typeof str==="string"&&str.length>max?str.substring(0,max)+add:str);};
/*!
 * fix en ru / ru en typo
 * modified sovtime.ru/soft/convert.html
 * gist.github.com/englishextra/8f398bb7a3e438b692352a3c114a13ae
 * jsfiddle.net/englishextra/6p150wu1/
 * jsbin.com/runoju/edit?js,output
 */
var fixEnRuTypo=function(e,a,b){var c="";"ru"==a&&"en"==b?(a='\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044c\u044b\u044d\u044e\u044f\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042c\u042b\u042d\u042e\u042f"\u2116;:?/.,',b="f,dult`;pbqrkvyjghcnea[wxio]ms'.zF<DULT~:PBQRKVYJGHCNEA{WXIO}MS'>Z@#$^&|/?"):(a="f,dult`;pbqrkvyjghcnea[wxio]ms'.zF<DULT~:PBQRKVYJGHCNEA{WXIO}MS'>Z@#$^&|/?",b='\u0430\u0431\u0432\u0433\u0434\u0435\u0451\u0436\u0437\u0438\u0439\u043a\u043b\u043c\u043d\u043e\u043f\u0440\u0441\u0442\u0443\u0444\u0445\u0446\u0447\u0448\u0449\u044a\u044c\u044b\u044d\u044e\u044f\u0410\u0411\u0412\u0413\u0414\u0415\u0401\u0416\u0417\u0418\u0419\u041a\u041b\u041c\u041d\u041e\u041f\u0420\u0421\u0422\u0423\u0424\u0425\u0426\u0427\u0428\u0429\u042a\u042c\u042b\u042d\u042e\u042f"\u2116;:?/.,');for(var d=0;d<e.length;d++)var f=a.indexOf(e.charAt(d)),c=0>f?c+e.charAt(d):c+b.charAt(f);return c};
/*!
 * remove all children of parent element
 */
var removeChildElements=function(a){if(a)for(;a.firstChild;)a.removeChild(a.firstChild);};
/*!
 * set style display block of an element
 */
var setStyleDisplayBlock=function(a){a&&(a.style.display="block");};
/*!
 * set style display none of an element
 */
var setStyleDisplayNone=function(a){a&&(a.style.display="none");};
/*!
 * toggle style display of an element
 */
var toggleStyleDisplay=function(a,show,hide){if(a){a.style.display=hide==a.style.display||""==a.style.display?show:hide;}};
/*!
 * set style opacity of an element
 */
var setStyleOpacity=function(a,n){a&&(a.style.opacity=n);};
/*!
 * set style visibility visible of an element
 */
var setStyleVisibilityVisible=function(a){a&&(a.style.visibility="visible");};
/*!
 * set style visibility hidden of an element
 */
var setStyleVisibilityHidden=function(a){a&&(a.style.visibility="hidden");};
/*!
 * Adds Element AFTER NeighborElement
 * gist.github.com/englishextra/c19556b7a61865e3631cc879aaeb314e
 * .appendAfterNeighborElement(element) Prototype
 * stackoverflow.com/questions/4793604/how-to-do-insert-after-in-javascript-without-using-a-library
 * NewElement.appendAfterNeighborElement(document.getElementById("NeighborElement"));
 */
Node.prototype.appendAfterNeighborElement=function(a){a.parentNode.insertBefore(this,a.nextSibling)};!1;
/*!
 * change location
 */
var changeDocumentLocation=function(h){h&&(document.location.href=h);};
/*!
 * get http or https
 */
var getHTTPProtocol=function(){var a=window.location.protocol||"";return"http:"===a?"http":"https:"===a?"https":""};
/*!
 * Open external links in default browser out of Electron / nwjs
 * gist.github.com/englishextra/b9a8140e1c1b8aa01772375aeacbf49b
 * stackoverflow.com/questions/32402327/how-can-i-force-external-links-from-browser-window-to-open-in-a-default-browser
 * github.com/nwjs/nw.js/wiki/shell
 * electron - file: | nwjs - chrome-extension: | http: Intel XDK
 */
var openDeviceBrowser = function (a) {
	var w = window,
	g = function () {
		var electronShell = require("electron").shell;
		electronShell.openExternal(a);
	},
	k = function () {
		var nwGui = require("nw.gui");
		nwGui.Shell.openExternal(a);
	},
	q = function () {
		var win = w.open(a, "_blank");
		win.focus();
	},
	v = function () {
		w.open(a, "_system", "scrollbars=1,location=no");
	};
	if (!!isElectron) {
		g();
	} else if (!!isNwjs) {
		k();
	} else {
		if (!!getHTTPProtocol()) {
			q();
		} else {
			v();
		}
	}
};
/*!
 * set target blank to external links
 */
var setTargetBlankOnAnchors = function () {
	var w = window,
	a = BALA("a") || "",
	g = function (e) {
		var p = e.getAttribute("href") || "";
		if (p
			&& (/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)|(localhost)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/.test(p) || /http:\/\/localhost/.test(p))
			&& !e.getAttribute("rel")) {
			crel(e, {
				"title" : "\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 " + (p.match(/:\/\/(.[^/]+)/)[1] || "") + " \u043e\u0442\u043a\u0440\u043e\u0435\u0442\u0441\u044f \u0432 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043b\u0438 \u043e\u043a\u043d\u0435"
			});
			if (w.openDeviceBrowser) {
				crel(e, {
					"onclick" : "openDeviceBrowser('" + jsStringEscape(p) + "');return !1;"
				});
			} else {
				crel(e, {
					"target" : "_blank"
				});
			}
		}
	},
	k = function () {
			if (w._) {
				_.each(a, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(a, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = a.length; i < l; i += 1) {
					g(a[i]);
				};
			}
	};
	if (a) {
		k();
	}
};
docReady(function () {
	setTargetBlankOnAnchors();
});
/*!
 * init fastclick
 * github.com/ftlabs/fastclick
 */
var initFastclick = function () {
	var w = window,
	b = BALA.one("body") || "",
	fastclick_js_src = "/cdn/fastclick/1.0.6/js/fastclick.fixed.min.js",
	g = function () {
		AJAXLoadAndTriggerJs(fastclick_js_src, function () {
			w.FastClick && FastClick.attach(b);
		});
	};
	if (!!getHTTPProtocol()) {
		g();
	}
};
"undefined" !== typeof earlyHasTouch && "touch" === earlyHasTouch && evento.add(window, "load", function () {
	initFastclick();
});
/*!
 * init disqus_thread and Masonry / Packery
 * add Draggabilly to Packarey
 * gist.github.com/englishextra/5e423ff34f67982f017b
 */
var initMasonryAndDisqusThread = function () {
	var w = window,
	disqus_thread = BALA.one("#disqus_thread") || "",
	is_active = "is-active",
	disqus_shortname = disqus_thread ? (disqus_thread.dataset.shortname || "") : "",
	embed_js_src = getHTTPProtocol() + "://" + disqus_shortname + ".disqus.com/embed.js",
	g = ".grid",
	h = ".grid-item",
	k = ".grid-sizer",
	grid = BALA.one(g) || "",
	grid_item = BALA.one(h) || "",
	masonry_js_src = "../cdn/masonry/4.0.0/js/masonry.pkgd.fixed.min.js",
	packery_js_src = "../cdn/packery/2.0.0/js/packery.pkgd.fixed.min.js",
	draggabilly_js_src = "../cdn/draggabilly/2.1.0/js/draggabilly.pkgd.fixed.min.js",
	q = function (a) {
		var s = function () {
			if (w.Masonry) {
				msnry = new Masonry(a, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
			}
		};
		scriptIsLoaded(masonry_js_src) || loadJS(masonry_js_src, function () {
			s();
		});
	},
	v = function (a, c) {
		var s = function () {
			if (w.Packery) {
				pckry = new Packery(a, {
						itemSelector : h,
						columnWidth : k,
						gutter : 0
					});
				if (c) {
					if (w.Draggabilly) {
						var t = function (e) {
							var draggableElem = e;
							draggie = new Draggabilly(draggableElem, {});
							draggies.push(draggie);
						};
						var draggies = [];
						if (w._) {
							_.each(c, function (e) {
								t(e);
							});
						} else if (w.forEach) {
							forEach(c, function (e) {
								t(e);
							}, !1);
						} else {
							for (var i = 0, l = c.length; i < l; i += 1) {
								t(c[i]);
							};
						}
						pckry && pckry.bindDraggabillyEvents(draggie);
					}
				}
			}
		};
		scriptIsLoaded(packery_js_src) || loadJS(packery_js_src, function () {
			scriptIsLoaded(draggabilly_js_src) || loadJS(draggabilly_js_src, function () {
				s();
			});
		});
	},
	z = function () {
		var s = function () {
			var si = new interval(50, function () {
				var disqus_thread_height = disqus_thread.clientHeight || disqus_thread.offsetHeight || "";
				if (108 < disqus_thread_height && 0 !== si) {
					si.stop(),
					si = 0;
					if ("undefined" !== typeof msnry && msnry) {
						msnry.layout();
					} else {
						if ("undefined" !== typeof pckry && pckry) {
							pckry.layout();
						}
					}
				}
			});
			si && si.run();
			disqus_thread.classList.add(is_active);
		};
		scriptIsLoaded(embed_js_src) || loadJS(embed_js_src, function () {
			s();
		});
	};
	if (grid && grid_item) {
		var msnry,
		pckry;
		q(grid);
		/* var c = BALA(h) || "";
		v(grid, c); */
		if (disqus_thread && disqus_shortname) {
			if (!!getHTTPProtocol()) {
				z();
			} else {
				setStyleDisplayNone(disqus_thread.parentNode.parentNode);
			}
		}
	}
};
if ("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) {
	var a = BALA.one("#disqus_thread") || "";
	setStyleDisplayNone(a.parentNode.parentNode);
} else {
	evento.add(window, "load", function () {
		initMasonryAndDisqusThread();
	});
}
/*!
 * init AJAX JSON select
 */
var initContentsSelect = function () {
	var w = window,
	a = BALA.one("#select") || "",
	contents_json_src = "../libs/contents/json/contents.json",
	g = function (e) {
		var t = getJsonKeyValues(e, "label") || "",
		p = getJsonKeyValues(e, "link") || "";
		if (t && p) {
			crel(a, crel("option", {
					"value" : p,
					"title" : "" + t
				}, truncString("" + t, 33)));
		}
	},
	k = function (_this) {
		var h = _this.options[_this.selectedIndex].value || "",
		zh = h ? (/^#/.test(h) ? BALA.one(h) : "") : "";
		if (h) {
			w.zenscroll ? (zh ? zenscroll.to(zh) : changeDocumentLocation(h)) : changeDocumentLocation(h);
		}
	},
	q = function (r) {
		var jpr = safelyParseJSON(r);
		if (jpr) {
			if (w._) {
				_.each(jpr, function (e) {
					g(e);
				});
			} else if (w.forEach) {
				forEach(jpr, function (e) {
					g(e);
				}, !1);
			} else {
				for (var i = 0, l = jpr.length; i < l; i += 1) {
					g(jpr[i]);
				};
			}
			evento.add(a, "change", function () {
				k(this);
			});
		}
	};
	if (a) {
		if (w.Promise && w.fetch && !isElectron) {
			fetch(contents_json_src).then(function (r) {
				return r.text();
			}).catch (function (e) {
				console.log("Error fetch-ing file", e);
			}).then(function (t) {
				q(t);
			}).catch (function (e) {
				console.log("Error parsing file", e);
			});
		} else {
			AJAXloadUnparsedJSON(contents_json_src, function (r) {
				q(r);
			});
		}
	}
};
evento.add(window, "load", function () {
	initContentsSelect();
});
/*!
 * init text
 */
var initSearchText = function () {
	var a = BALA.one("#text") || "",
	g = function (_this) {
		_this.value = _this.value.replace(/\\/g, "").replace(/ +(?= )/g, " ").replace(/\/+(?=\/)/g, "/") || "";
	},
	k = function (e) {
		e.focus();
		evento.add(e, "input", function () {
			g(this);
		});
	};
	if (a) {
		k(a);
	}
};
docReady(function () {
	initSearchText();
});
/*!
 * init qr-code
 * stackoverflow.com/questions/12777622/how-to-use-enquire-js
 */
var showPageQRRef = function () {
	var w = window,
	d = document,
	a = BALA.one("#qr-code") || "",
	p = w.location.href || "",
	g = function () {
		removeChildElements(a);
		var t = jsStringEscape(d.title ? ("\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u00ab" + d.title.replace(/\[[^\]]*?\]/g, "").trim() + "\u00bb") : ""),
		s = getHTTPProtocol() + "://chart.googleapis.com/chart?cht=qr&chld=M|4&choe=UTF-8&chs=300x300&chl=" + encodeURIComponent(p),
		c = "width:10.000em;height:10.000em;background:transparent;background-size:120.000pt 120.000pt;border:0;vertical-align:bottom;padding:0;margin:0;";
		crel(a,
			crel("img", {
				"src" : s,
				"style" : c,
				"title" : t,
				"alt" : t
			}));
	};
	if (a && p) {
		if (!!getHTTPProtocol()) {
			g();
		} else {
			setStyleDisplayNone(a);
		}
	}
};
("undefined" !== typeof earlyDeviceSize && "small" === earlyDeviceSize) || evento.add(window, "load", function () {
	showPageQRRef();
});
/*!
 * init nav-menu
 */
var initNavMenu = function () {
	var w = window,
	container = BALA.one("#container") || "",
	page = BALA.one("#page") || "",
	btn = BALA.one("#btn-nav-menu") || "",
	panel = BALA.one("#panel-nav-menu") || "",
	items = BALA("a", panel) || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	active = "active",
	p = w.location.href || "",
	g = function () {
		if (panel.classList.contains(active)) {
			page.classList.remove(active);
			panel.classList.remove(active);
			btn.classList.remove(active);
		}
	},
	k = function () {
		setStyleDisplayNone(holder);
		page.classList.toggle(active);
		panel.classList.toggle(active);
		btn.classList.toggle(active);
	},
	q = function () {
		setStyleDisplayNone(holder);
		page.classList.remove(active);
		panel.classList.remove(active);
		btn.classList.remove(active);
		/* scrollToTop(); */
	},
	m = function (e) {
		e.classList.remove(active);
	},
	n = function (e) {
		e.classList.add(active);
	},
	s = function (a) {
		if (w._) {
			_.each(a, function (e) {
				m(e);
			});
		} else if (w.forEach) {
			forEach(a, function (e) {
				m(e);
			}, !1);
		} else {
			for (var j = 0, l = a.length; j < l; j += 1) {
				m(a[j]);
			};
		}
	},
	v = function (a, e) {
		evento.add(e, "click", function () {
			if (panel.classList.contains(active)) {
				q();
			}
			s(a);
			n(e);
		});
		if (e.href == p) {
			n(e);
		} else {
			m(e);
		}
	},
	z = function () {
		if (w._) {
			_.each(items, function (e) {
				v(items, e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				v(items, e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				v(items, items[i]);
			};
		}
	};
	if (container && page && btn && panel && items) {
		/*!
		 * open or close nav
		 */
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		}),
		evento.add(container, "click", function () {
			g();
		});
		/*!
		 * close nav, scroll to top, highlight active nav item
		 */
		z();
	}
};
docReady(function () {
	initNavMenu();
});
/*!
 * add updates link to menu more
 * place that above init menu more
 */
var addAppUpdatesLink = function () {
	var w = window,
	panel = BALA.one("#panel-menu-more") || "",
	items = BALA("li", panel) || "",
	n = navigator.userAgent || "",
	p = "";
	(/Windows/i.test(n) && /(WOW64|Win64)/i.test(n)) ? (p = "https://englishextraapp.codeplex.com/downloads/get/1539373") :
	(/(x86_64|x86-64|x64;|amd64|AMD64|x64_64)/i.test(n) && /(Linux|X11)/i.test(n)) ? (p = "https://englishextraapp.codeplex.com/downloads/get/1540156") :
	(/IEMobile/i.test(n)) ? (p = "https://englishextraapp.codeplex.com/downloads/get/1536102") :
	(/Android/i.test(n)) ? (p = "https://englishextraapp.codeplex.com/downloads/get/1528911") :
	(p = "");
	var g = function () {
		var s = crel("li", crel("a", {
					"href" : p,
					"title" : "\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0432\u043d\u0435\u0448\u043d\u0438\u0439 \u0440\u0435\u0441\u0443\u0440\u0441 " + (p.match(/:\/\/(.[^/]+)/)[1] || "") + " \u043e\u0442\u043a\u0440\u043e\u0435\u0442\u0441\u044f \u0432 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435 \u0438\u043b\u0438 \u043e\u043a\u043d\u0435"
				}, "" + "\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u043F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u0438\u0435 \u0441\u0430\u0439\u0442\u0430"));
		if (w.openDeviceBrowser) {
			crel(s, {
				"onclick" : "openDeviceBrowser('" + jsStringEscape(p) + "');return !1;"
			});
		} else {
			crel(s, {
				"target" : "_blank"
			});
		}
		!!panel.firstChild && panel.insertBefore(s, panel.firstChild);
	};
	if (panel && items && p && !!n) {
		g();
	}
};
docReady(function () {
	addAppUpdatesLink();
});
/*!
 * init menu-more
 */
var initMenuMore = function () {
	var w = window,
	container = BALA.one("#container") || "",
	holder = BALA.one("#holder-panel-menu-more") || "",
	btn = BALA.one("#btn-menu-more") || "",
	panel = BALA.one("#panel-menu-more") || "",
	items = BALA("li", panel) || "",
	g = function (e) {
		evento.add(e, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	k = function () {
		evento.add(container, "click", function () {
			setStyleDisplayNone(holder);
		});
	},
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			toggleStyleDisplay(holder, "inline-block", "none");
		});
	},
	v = function () {
		if (w._) {
			_.each(items, function (e) {
				g(e);
			});
		} else if (w.forEach) {
			forEach(items, function (e) {
				g(e);
			}, !1);
		} else {
			for (var i = 0, l = items.length; i < l; i += 1) {
				g(items[i]);
			};
		}
	};
	if (container && holder && btn && panel && items) {
		/*!
		 * hide menu more on outside click
		 */
		k();
		/*!
		 * show or hide menu more
		 */
		q();
		/*!
		 * hide menu more on item clicked
		 */
		v();
	}
};
docReady(function () {
	initMenuMore();
});
/*!
 * init ui-totop
 */
var initUiTotop = function () {
	var w = window,
	b = BALA.one("body") || "",
	h = BALA.one("html") || "",
	u = "ui-totop",
	v = "ui-totop-hover",
	g = function (cb) {
		crel(b,
			crel("a", {
				"style" : "opacity:0;",
				"href" : "#",
				"title" : "\u041d\u0430\u0432\u0435\u0440\u0445",
				"id" : u,
				"onclick" : "function scrollTop2(c){var b=window.pageYOffset,d=0,e=setInterval(function(b,a){return function(){a-=b*c;window.scrollTo(0,a);d++;(150<d||0>a)&&clearInterval(e)}}(c,b--),50)};window.zenscroll?zenscroll.toY(0):scrollTop2(50);return !1;"
			},
				crel("span", {
					"id" : v
				}), "\u041d\u0430\u0432\u0435\u0440\u0445"));
		!!cb && "function" === typeof cb && cb();
	},
	k = function (_this) {
		var offset = _this.pageYOffset || h.scrollTop || b.scrollTop || "",
		height = _this.innerHeight || h.clientHeight || b.clientHeight || "",
		btn = BALA.one("#" + u) || "";
		if (offset && height && btn) {
			offset > height ? (setStyleVisibilityVisible(btn), setStyleOpacity(btn, 1)) : (setStyleVisibilityHidden(btn), setStyleOpacity(btn, 0));
		}
	},
	q = function () {
		evento.add(w, "scroll", function () {
			k(this);
		});
	};
	if (b) {
		g(function () {
			q();
		});
	}
};
docReady(function () {
	initUiTotop();
});
/*!
 * init pluso-engine or ya-share on click
 */
var showShareOptionsOnClick = function () {
	var pluso = BALA.one(".pluso") || "",
	ya_share2 = BALA.one(".ya-share2") || "",
	btn = BALA.one("#btn-block-social-buttons") || "",
	pluso_like_js_src = getHTTPProtocol() + "://share.pluso.ru/pluso-like.js",
	share_js_src = getHTTPProtocol() + "://yastatic.net/share2/share.js",
	g = function (share_block, btn) {
		setStyleVisibilityVisible(share_block);
		setStyleOpacity(share_block, 1);
		setStyleDisplayNone(btn);
	},
	k = function (js_src, share_block, btn) {
		scriptIsLoaded(js_src) || loadJS(js_src, function () {
			g(share_block, btn);
		});
	},
	q = function () {
		if (pluso) {
			k(pluso_like_js_src, pluso, btn);
		} else {
			if (ya_share2) {
				k(share_js_src, ya_share2, btn);
			}
		}
	},
	v = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			q();
		});
	};
	if ((pluso || ya_share2) && btn) {
		if (!!getHTTPProtocol()) {
			v();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	showShareOptionsOnClick();
});
/*!
 * init vk-like on click
 */
var initVKOnClick = function () {
	var w = window,
	vk_like = BALA.one("#vk-like") || "",
	btn = BALA.one("#btn-show-vk-like") || "",
	openapi_js_src = getHTTPProtocol() + "://vk.com/js/api/openapi.js?122",
	g = function () {
		try {
			w.VK && (VK.init({
					apiId : (vk_like.dataset.apiid || ""),
					nameTransportPath : "/xd_receiver.htm",
					onlyWidgets : !0
				}), VK.Widgets.Like("vk-like", {
					type : "button",
					height : 24
				}));
			setStyleVisibilityVisible(vk_like);
			setStyleOpacity(vk_like, 1);
			setStyleDisplayNone(btn);
		} catch(e) {
			setStyleVisibilityHidden(vk_like);
			setStyleOpacity(vk_like, 0);
			setStyleDisplayBlock(btn);
		}
	},
	k = function () {
		scriptIsLoaded(openapi_js_src) || loadJS(openapi_js_src, function () {
			g();
		});
	}
	q = function () {
		evento.add(btn, "click", function (e) {
			e.preventDefault();
			e.stopPropagation();
			k();
		});
	};
	if (vk_like && btn) {
		if (!!getHTTPProtocol()) {
				q();
		} else {
			setStyleDisplayNone(btn);
		}
	}
};
evento.add(window, "load", function () {
	initVKOnClick();
});
/*!
 * init Contents Kamil autocomplete
 * github.com/oss6/kamil/wiki/Example-with-label:link-json-and-typo-correct-suggestion
 */
var initContentsKamilAutocomplete = function () {
	var w = window,
	d = document,
	b = BALA.one("body") || "",
	search_form = BALA.one("#search_form") || "",
	text_id = "text",
	text = BALA.one("#" + text_id) || "",
	_ul_id = "kamil-typo-autocomplete",
	_ul_class = "kamil-autocomplete",
	kamil_js_src = "../cdn/kamil/0.1.1/js/kamil.min.js",
	contents_json_src = "../libs/contents/json/contents.json";
	scriptIsLoaded(kamil_js_src) || loadJS(kamil_js_src, function () {
		var q = function (r) {
			var jpr = safelyParseJSON(r);
			var ac = new Kamil("#" + text_id, {
					source : jpr,
					minChars : 2
				});
			/*!
			 * create typo suggestion list
			 */
			var _ul = crel("ul", {
					"class" : _ul_class,
					"id" : _ul_id,
					"style" : "display:none;"
				}),
			_li = crel("li", {
					"style" : "display:none;"
				});
			crel(_ul, crel(_li));
			_ul.appendAfterNeighborElement(text);
			/*!
			 * show suggestions
			 */
			ac.renderMenu = function (ul, items) {
				var l = items.length,
				_this = this;
				/*!
				 * limit output
				 */
				if (w._) {
					_.each(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					});
				} else if (w.forEach) {
					forEach(items, function (e, i) {
						if (i < 10) {
							_this._renderItemData(ul, e, i);
						}
					}, !1);
				} else {
					for (var i = 0; i < l; i += 1) {
						if (i < 10) {
							_this._renderItemData(ul, items[i], i);
						}
					};
				}
				/*!
				 * fix typo - non latin characters found
				 */
				while (l < 1) {
					var v = text.value;
					if (/[^\u0000-\u007f]/.test(v)) {
						v = fixEnRuTypo(v, "ru", "en");
					} else {
						v = fixEnRuTypo(v, "en", "ru");
					}
					crel(_ul, {
						"style" : "display:block;"
					});
					crel(_li, {
						"style" : "display:block;"
					});
					removeChildElements(_li);
					crel(_li, {
						"onclick" : '(function(a,b,c){c&&(c.value="' + v + '")&&c.focus();b&&b.setAttribute("style","display:none;"),a&&a.setAttribute("style","display:none;");}(this,document.getElementById("' + _ul_id + '")||"",document.getElementById("' + text_id + '")||""));'
					}, "" + v);
					if (v.match(/^\s*$/)) {
						crel(_ul, {
							"style" : "display:none;"
						});
						crel(_li, {
							"style" : "display:none;"
						});
					}
					evento.add(text, "input", function () {
						if (text.value.length < 3 || text.value.match(/^\s*$/)) {
							crel(_ul, {
								"style" : "display:none;"
							});
							crel(_li, {
								"style" : "display:none;"
							});
						}
					});
					l++;
				};
				/*!
				 * truncate text
				 */
				var g = function (e) {
					var t = e.firstChild.textContent || "",
					n = document.createTextNode(truncString(t, 24));
					e.replaceChild(n, e.firstChild);
					crel(e, {
						title : "" + t
					});
				},
				lis = BALA("li", ul);
				if (w._) {
					_.each(lis, function (e) {
						g(e);
					});
				} else if (w.forEach) {
					forEach(lis, function (e) {
						g(e);
					}, !1);
				} else {
					for (var i = 0, l = lis.length; i < l; i += 1) {
						g(lis[i]);
					};
				}
			};
			/*!
			 * use kamil built-in word label as search key in JSON file
			 * [{"link":"/","label":"some text to match"},
			 * {"link":"/pages/contents.html","label":"some text to match"}]
			 */
			ac.on("kamilselect", function (e) {
				var p = e.item.link || "";
				if (p) {
					setImmediate(function () {
						e.inputElement.value = "";
						changeDocumentLocation(p);
					});
				}
			});
		};
		if (search_form && text) {
			if (w.Promise && w.fetch && !isElectron) {
				fetch(contents_json_src).then(function (r) {
					return r.text();
				}).catch (function (e) {
					console.log("Error fetch-ing file", e);
				}).then(function (t) {
					q(t);
				}).catch (function (e) {
					console.log("Error parsing file", e);
				});
			} else {
				AJAXloadUnparsedJSON(contents_json_src, function (r) {
					q(r);
				});
			}
		}
	});
};
evento.add(window, "load", function () {
	initContentsKamilAutocomplete();
});
/*!
 * init search form and ya-site-form
 */
var initSearchForm = function () {
	var w = window,
	h = BALA.one("html") || "",
	search_form = BALA.one("#search_form") || "",
	ya_site_form = BALA.one(".ya-site-form.ya-site-form_inited_no") || "",
	all_js_src = getHTTPProtocol() + "://site.yandex.net/v2.0/js/all.js",
	g = function () {
		crel(search_form, {
			"action" : getHTTPProtocol() + "://yandex.ru/sitesearch",
			"target" : "_blank"
		});
	},
	k = function () {
		if (h && !h.classList.contains("ya-page_js_yes")) {
			h.classList.add("ya-page_js_yes");
		}
		crel(ya_site_form, {
			"onclick" : "return {'action':'https://yandex.com/search/site/','arrow':false,'bg':'transparent','fontsize':16,'fg':'#000000','language':'auto','logo':'rb','publicname':'Поиск по сайту englishextra.github.io','suggest':true,'target':'_blank','tld':'com','type':3,'usebigdictionary':true,'searchid':2192588,'input_fg':'#363636','input_bg':'#E9E9E9','input_fontStyle':'normal','input_fontWeight':'normal','input_placeholder':'\u041F\u043E\u0438\u0441\u043A','input_placeholderColor':'#686868','input_borderColor':'#E9E9E9'}"
		});
		scriptIsLoaded(all_js_src) || loadJS(all_js_src, function () {
			w.Ya && Ya.Site.Form.init();
			/*!
			 * yandex will load its own css making form visible
			 */
		});
	},
	q = function () {
		crel(search_form, {
			"action" : "#",
			"target" : "_self",
			"onsubmit" : "return !1;"
		});
		setStyleDisplayNone(ya_site_form);
	};
	if (!!getHTTPProtocol()) {
		if (search_form) {
			g();
		}
		if (ya_site_form) {
			k();
		}
	} else {
		q();
	}
};
evento.add(window, "load", function () {
	initSearchForm();
});
/*!
 * init manUP.js
 */
var initManupJs = function () {
	var manup_js_src = "/cdn/ManUp.js/0.7/js/manup.fixed.min.js";
	if (!!getHTTPProtocol()) {
		AJAXLoadAndTriggerJs(manup_js_src);
	}
};
evento.add(window, "load", function () {
	initManupJs();
});
/*!
 * show page, finish ToProgress
 */
evento.add(window, "load", function () {
	var a = BALA.one("#container") || "",
	g = function () {
		setStyleOpacity(a, 1),
		setImmediate(function () {
			progressBarAvailable && (progressBar.finish(), progressBar.hide());
		});
	},
	k = function () {
		var si = new interval(50, function () {
				if (hiddenPreloadImage && 0 !== si) {
					si.stop(),
					si = 0;
					g();
				}
			});
		si && si.run();
	};
	if (a) {
		if ("undefined" !== typeof hiddenPreloadImage) {
			k();
		} else {
			g();
		}
	}
});
